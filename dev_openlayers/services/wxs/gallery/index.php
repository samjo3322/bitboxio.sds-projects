<?php
/* ---------------------------------------------------------------------
 * FILE: index.php				DATE:04/20/2010
 * ---------------------------------------------------------------------
 * This script accepts a http request and returns a response as data
 * in the follow formats:
 * 		1. XML
 * 		2. JSON
 * 		3. KRUMO (Debugging)
 * 
 * PARAMETERS: 
 * 		Required: $output, $svcname
 * 		Optional: seecode, param1...*
 * 
 * EXAMPLE: ?svcname=0&output=krumo
 * ---------------------------------------------------------------------*/

require_once('../classes/krumo/class.krumo.php');
require_once('../includes/config.php');
require_once('../includes/functions.php');

switch ($svcname) {
    case 0: // get only public maps 
		$sql = "select * from maps where [maps].[IsPublic] = 'True'";
		break;
	case 1: // get only subscription maps
		$sql = "select * from maps where [maps].[IsPublic] = 'False'";
		break;
	case 2: // get both public and subscription maps
		if($param0 != ''){
			$sql = "select [maps].[MapId], [maps].[MapName], CAST([maps].[MapDefinitions] as TEXT) as [MapDefinitions], [maps].[MapStack], [maps].[Snapshot], CAST([maps].[MapDescription] as TEXT) as [MapDescription], CAST([maps].[MapDisclaimer] as TEXT) as [MapDisclaimer], [maps].[MapToolTip], [countymaplookup].[MapId], [countymaplookup].[CountyId] from [maps], [countymaplookup] where [countymaplookup].[MapId] = [maps].[MapId] and [countymaplookup].[CountyId] = ".$param0;
			$sql .= "\nselect [CountyId], [Header] from [counties] where [CountyId] = ".$param0;
		}
		break;
	case 3: // search for maps based on name, and description
		if($param0 != ''){
			$sql = "select [MapId], [MapName], [MapDescription], [IsPublic] from [maps] where [MapName] like '%".$param0."%' or [MapDescription] like '%".$param0."%'";
		}
		break;
	case 4: // get a list of locals for gallery
		$sql = "select [counties].[CountyId], [counties].[CountyName], [counties].[Boilerplate], [counties].[Logo], [counties].[Header], [counties].[StateId], [states].[StateId], [states].[StateName], [states].[StateCode], [countymaplookup].[CountyId], [countymaplookup].[MapId] from [counties], [states], [countymaplookup] where [counties].[StateId] = [states].[StateId] and [counties].[CountyId] = [countymaplookup].[CountyId]";
		break;
	case 5: // get a list of locals for gallery
		if($param0 != ''){
			$sql = "select [MapId], [MapName], CAST([MapDefinitions] as TEXT) as [MapDefinitions], [MapStack], [Snapshot], CAST([maps].[MapDescription] as TEXT) as [MapDescription], CAST([MapDisclaimer] as TEXT) as [MapDisclaimer], [MapToolTip] from [maps] where [MapId] = ".$param0;
		}
		break;
}	

if($sql != ""){
	putenv('TDSVER=70');	
	$outputArr = array();
	$dbConn = mssql_connect($dbHost, $dbUser, $dbPass) or die("Could not connect to MSSQL: ".mssql_get_last_message());
	$selected = mssql_select_db($dbDatabase, $dbConn) or die("Couldn't open database $dbDatabase ".mssql_get_last_message());
	$dbResult = queryDB($sql,$dbConn);
	//enable return of multiple datasets
	if($response){
		$i = 0;
		do {	
			while($line = mssql_fetch_assoc($dbResult)){
				$item = array("item$i"=>$line);
				array_push($outputArr,$item);
			}
			$i++;
		}while (mssql_next_result($dbResult));
		mssql_free_result($dbResult);
		mssql_close($dbConn);
		echo output_result($output, $outputArr);
	}else{
		$blank = array('Result'=>'No return expected');
		echo output_result($output, $blank);
	}
}
?>




