<?php
/* ---------------------------------------------------------------------
 * FILE: webservice.php				DATE:06/10/2010
 * ---------------------------------------------------------------------
 * This script accepts a http request and returns a response as data
 * in the follow formats:
 * 		1. XML
 * 		2. JSON
 * 		3. KRUMO (Debugging)
 * 
 * PARAMETERS: 
 * 		Required: $output, $svcname
 * 		Optional: seecode, param1...*
 * 
 * EXAMPLE: ?svcname=0&output=krumo
 * ---------------------------------------------------------------------
 */

require_once('../classes/krumo/class.krumo.php');
require_once('../includes/config.php');
require_once('../includes/functions.php');  

switch ($svcname) {
    case 0: // MEASURE: takes 2 points as lonlat and returns distance in meteres (0)spheriod_dist, (1)sphere_dist
		if($param0 != "" || $param1 != ""){
			$sql = "SELECT ST_Distance(gg1, gg2) As spheroid_dist, ST_Distance(gg1, gg2, false) As sphere_dist FROM (SELECT ST_GeographyFromText('SRID=4326;POINT(".$param0.")') As gg1, ST_GeographyFromText('SRID=4326;POINT(".$param1.")') As gg2) As foo;";
		}
		break;
	case 1: // MEASURE: takes 3+ points as lonlat and returns area in sq ft(0) & sq meters(1)
		if($param0 != ""){
			$sql = "SELECT ST_Area(geog)/POWER(0.3048,2) As sqft, ST_Area(geog) As sqm FROM geography(ST_GeomFromText('POLYGON((".$param0."))',4326)) As foo(geog);";
		}
		break;
	case 2: // SELECT: takes a tablename, point and distance parmeter; returns parcellist and pointer to kml file.
		if($param0 != "" && $param1 != "" && $param2){
			$sql = "SELECT * FROM \"".$param0."\" WHERE ST_DWithin(geography(the_geom), geography(ST_GeomFromText('POINT(".$param1.")')), ".$param2.");";
			$cmd = $ogrpath." -f KML ".$ogrcache."test.kml PG:\"".$ogrpostgis."\" -sql \"".$sql."\"";
			echo $cmd;
		}
		break;
}

if($sql != ""){
	$outputArr = array();
	$sdbConn = pg_connect("host=".$sdbHost." port=".$sdbPort." dbname=".$sdbDatabase." user=".$sdbUser." password=".$sdbPass);
	$sdbResult = pg_query($sdbConn, $sql);

	$i = 0;
	while ($line = pg_fetch_assoc($sdbResult)) {
	  $item = array("item$i"=>$line);
	  array_push($outputArr,$item);
	  $i++;
	}
	pg_free_result($sdbResult);
	pg_close($sdbConn);
	echo output_result($output, $outputArr);
	//krumo($row);
}
?>
