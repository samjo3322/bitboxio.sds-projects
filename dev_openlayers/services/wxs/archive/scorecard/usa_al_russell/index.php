<?php
/* ---------------------------------------------------------------------
 * FILE: admin_webservice.php				DATE:03/11/2010
 * ---------------------------------------------------------------------
 * This script accepts a http request and returns a response as data
 * in the follow formats:
 * 		1. XML
 * 		2. JSON
 * 		3. KRUMO (Debugging)
 * 
 * PARAMETERS: 
 * 		Required: $output, $svcname
 * 		Optional: seecode, param1...*
 * 
 * EXAMPLE: ?svcname=0&output=krumo
 * ---------------------------------------------------------------------
 */

require_once('../../classes/krumo/class.krumo.php');
require_once('../../includes/config.php');
require_once('../../includes/functions.php');

switch ($svcname) {
    case 0: //
		if($param0 != ""){
			$sql = "select P_HOUSE, P_STREET, P_CITY from PARCEL where PARCEL = '".$param0."'";
		}
		break;
	case 1: // 
		if($param0 != ""){
			$sql = "select * from BUILDING where PARCEL = '".$param0."'";
		}
		break;
	case 2: // 
		if($param0 != ""){
			$sql = "select TAX_YEAR, SALE_PRICE from SALESold where PARCELNO = '".$param0."'";
		}
		break;
}

if($sql != ""){
	putenv('TDSVER=70');	
	$outputArr = array();
	$dbConn = mssql_connect($cdbHost_usRussell, $cdbUser_usRussell, $cdbPass_usRussell) or die("Could not connect to MSSQL: ".mssql_get_last_message());
	$selected = mssql_select_db($cdbDatabase_usRussell, $dbConn) or die("Couldn't open database $cdbDatabase_usRussell ".mssql_get_last_message());
	$dbResult = queryDB($sql,$dbConn);
	//enable return of multiple datasets
	if($response){
		$i = 0;
		do {	
			while($line = mssql_fetch_assoc($dbResult)){
				$item = array("item$i"=>$line);
				array_push($outputArr,$item);
			}
			$i++;
		}while (mssql_next_result($dbResult));
		mssql_free_result($dbResult);
		mssql_close($dbConn);
		echo output_result($output, $outputArr);
	}else{
		$blank = array('Result'=>'No return expected');
		echo output_result($output, $blank);
	}
}
?>
