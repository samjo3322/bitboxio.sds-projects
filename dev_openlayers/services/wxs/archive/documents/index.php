<?php
/* ---------------------------------------------------------------------
 * FILE: index.php				DATE:04/20/2010
 * ---------------------------------------------------------------------
 * This script accepts a http request and returns a response as data
 * in the follow formats:
 * 		1. XML
 * 		2. JSON
 * 		3. KRUMO (Debugging)
 * 
 * PARAMETERS: 
 * 		Required: $output, $svcname
 * 		Optional: seecode, param1...*
 * 
 * EXAMPLE: ?svcname=0&output=krumo
 * ---------------------------------------------------------------------*/

require_once('../../classes/krumo/class.krumo.php');
require_once('../../includes/config.php');
require_once('../../includes/functions.php');

// override default db config for this service
$dbHost = $docHost;				
$dbUser = $docUser;
$dbPass = $docPass;
$dbDatabase = $docDatabase;

switch ($svcname) {
	case 0: // Check for file data and insert into doc table
		if($param0 != '' && $param1 != ''){
			$sql = "INSERT INTO DOC(DOC_FILE, DOC_PATH) values('".$param0."','".$param0."'); SELECT @@IDENTITY"
		} 
		break;
	case 1: // add document attribute
		if($param0 > 0 && $param1 != '' && $param2 != ''){
			$sql = "INSERT INTO DOC_ATTR(DOC_ATTRIBUTE, DOC_ATTR_VALUE) VALUES('".$param1."','".$param2."'); SELECT @@IDENTITY"
		} 
		break;
	case 2: // update document attribute
		if($param0 != '' && $param1 != '' && $param2 != ''){
			$sql = "UPDATE DOC_ATTR SET DOC_ATTRIBUTE = '".$param0."', DOC_ATTR_VALUE = '".$param1."' WHERE DOC_ATTR_ID = ".$param2
		} 
		break;
	case 3: // update document
		if($param0 != '' && $param1 != '' || $param2 != ''){
			
			if($param1 != ''){
				$sql = "UPDATE DOC SET DOC_FILE = '".$param1."' WHERE DOC_ID = ".$param0;
			}
			if($param2 != ''){
				$sql = $sql." UPDATE DOC SET DOC_PATH = '".$param2."' WHERE DOC_ID = ".$param0;
			}
		} 
		break;	
	case 4: // update document info
		if($param0 != '' && $param1 != '' || $param2 != '' || $param3 != ''){
			if($param1 != ''){
				$sql = "UPDATE DOC_INFO SET DOC_NAME = '".$param1."' WHERE DOC_INFO_ID = ".$param0;
			}
			if($param2 != ''){
				$sql = $sql." UPDATE DOC_INFO SET DOC_SIZE = ".$param2." WHERE DOC_INFO_ID = ".$param0;
			}
			if($param3 != ''){
				$sql = $sql." UPDATE DOC_INFO SET DOC_TYPE = '".$param3."' WHERE DOC_INFO_ID = ".$param0;
			}
		} 
		break;
		
	case 5: // read document
		if($param0 != ''){
			$sql = "SELECT DOC_FILE FROM DOC WHERE DOC_ID = ".$param0
		} 
		break;
	case 6: // delete document
		if($param0 != ''){
			$sql = "UPDATE DOC_INFO SET DOC_ISACTIVE = 0 WHERE DOC_ID = ".$param0
		} 
		break;
	case 6: // purge document
		if($param0 != ''){
			$sql = "DELETE FROM DOC WHERE DOC_ID = ".$param0
		} 
		break;
}

if($sql != ""){
	putenv('TDSVER=70');	
	$outputArr = array();
	$dbConn = mssql_connect($dbHost, $dbUser, $dbPass) or die("Could not connect to MSSQL: ".mssql_get_last_message());
	$selected = mssql_select_db($dbDatabase, $dbConn) or die("Couldn't open database $dbDatabase ".mssql_get_last_message());
	$dbResult = queryDB($sql,$dbConn);
	//enable return of multiple datasets
	if($response){
		$i = 0;
		do {	
			while($line = mssql_fetch_assoc($dbResult)){
				$item = array("item$i"=>$line);
				array_push($outputArr,$item);
			}
			$i++;
		}while (mssql_next_result($dbResult));
		mssql_free_result($dbResult);
		mssql_close($dbConn);
		echo output_result($output, $outputArr);
	}else{
		$blank = array('Result'=>'No return expected');
		echo output_result($output, $blank);
	}
}
?>




