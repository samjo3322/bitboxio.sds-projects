<?php
/* ---------------------------------------------------------------------
 * FILE: admin_webservice.php				DATE:03/11/2010
 * ---------------------------------------------------------------------
 * This script accepts a http request and returns a response as data
 * in the follow formats:
 * 		1. XML
 * 		2. JSON
 * 		3. KRUMO (Debugging)
 * 
 * PARAMETERS: 
 * 		Required: $output, $svcname
 * 		Optional: seecode, param1...*
 * 
 * EXAMPLE: ?svcname=0&output=krumo
 * ---------------------------------------------------------------------
 */

require_once('../../../classes/krumo/class.krumo.php');
require_once('../../../includes/config.php');
require_once('../../../includes/functions.php');

switch ($svcname) {
	// param0: longitude latitude
	// param1: parcel table
	// param2: parcel column
    case 0: // looks up parcel id from inputs
		if($param0 != "" && $param1 != "" &&  $param2 != ""){
			$sql = "SELECT \"".$param2."\" AS PARCELNO FROM \"".$param1."\" WHERE ST_DWithin(geog, geography(ST_GeomFromText('POINT(".$param0.")')),1);";
		}
		break;
}

if($sql != ""){
	putenv('TDSVER=70');	
	$outputArr = array();
	$sdbConn = pg_connect("host=".$sdbHost." port=".$sdbPort." dbname=".$sdbDatabase." user=".$sdbUser." password=".$sdbPass);
	$sdbResult = pg_query($sdbConn, $sql);
	
	while($line = pg_fetch_assoc($sdbResult)){
		$item = array("item0"=>$line);
		array_push($outputArr,$item);
	}
	pg_free_result($sdbResult);
	pg_close($sdbConn);
	echo output_result($output, $outputArr);
}
?>
