<?php
/* ---------------------------------------------------------------------
 * FILE: webservice.php				DATE:06/10/2010
 * ---------------------------------------------------------------------
 * This script accepts a http request and returns a response as data
 * in the follow formats:
 * 		1. XML
 * 		2. JSON
 * 		3. KRUMO (Debugging)
 * 
 * PARAMETERS: 
 * 		Required: $output, $svcname
 * 		Optional: seecode, param1...*
 * 
 * EXAMPLE: ?svcname=0&output=krumo
 * ---------------------------------------------------------------------
 */

require_once('../../classes/krumo/class.krumo.php');
require_once('../../includes/config.php');
require_once('../../includes/functions.php');  

switch ($svcname) {
	// SELECT: performs a spatial query (dwithin) and returns parcel no(s).
	// $param0 - parcelno
	// $param1 - parcelcolumn
	// $param2 - parceltable
	// $param3 - map extent
    case 0: // SELECT: performs a spatial query (dwithin) and returns parcel no(s).
		if($param0 != "" && $param1 != "" && $param2 != "" && $param3 != ""){
			$sql = "SELECT gid, \"".$param1."\" as PARCELID, ST_AsGML(geog) FROM \"".$param2."\" WHERE \"".$param1."\" = '".$param0."';";
		}
		break;
}

if($sql != ""){
	$outputArr = array();
	$sdbConn = pg_connect("host=".$sdbHost." port=".$sdbPort." dbname=".$sdbDatabase." user=".$sdbUser." password=".$sdbPass);
	$sdbResult = pg_query($sdbConn, $sql);
	
	$wfs .= '<?xml version="1.0" encoding="ISO-8859-1"?>';
	$wfs .= "\n<wfs:FeatureCollection \n\txmlns:ms=\"http://mapserver.gis.umn.edu/mapserver\" \n\txmlns:wfs=\"http://www.opengis.net/wfs\" \n\txmlns:gml=\"http://www.opengis.net/gml\" \n\txmlns:ogc=\"http://www.opengis.net/ogc\" \n\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \n\txsi:schemaLocation=\"http://www.opengis.net/wfs http://schemas.opengis.net/wfs/1.0.0/WFS-basic.xsd \n\t\t\t\thttp://mapserver.gis.umn.edu/mapserver https://127.0.0.1/cgi-bin/mapserv?map=C:\ms4w\sds\maps\wfs_us_al_russell.map&amp;SERVICE=WFS&amp;VERSION=1.0.0&amp;REQUEST=DescribeFeatureType&amp;TYPENAME=".$param1."&amp;OUTPUTFORMAT=XMLSCHEMA\">";
	$wfs .= "\n\t<gml:boundedBy><gml:Box srsName=\"EPSG:4326\"><gml:coordinates>".$param3."</gml:coordinates></gml:Box></gml:boundedBy>";
	

	$i = 0;
	while ($line = pg_fetch_assoc($sdbResult)) {

		// check for parcelno
		if($line['parcelno'] != ""){
			
			$wfs .=	"\n\t<gml:featureMember>\n\t<ms:".$param1.">\n\t\t<gml:boundedBy>\n\t\t\t<gml:Box srsName=\"EPSG:4326\">\n\t\t\t\t<gml:coordinates>".$param3."</gml:coordinates>\n\t\t\t</gml:Box>\n\t\t</gml:boundedBy>\n";

			foreach($line as $key=>$value){
				
				if($key == 'st_asgml'){
					$wfs .= "\t\t<ms:msGeometry>\n";
					$wfs .= "\t\t\t".$value;
					$wfs .= "\n\t\t</ms:msGeometry>\n";
				}else{
					$wfs .= "\t\t<ms:".strtoupper($key).">".$value."</ms:".strtoupper($key).">\n";
				}	
			}
		  
			$wfs .= "\t\t</ms:".$param1.">\n\t</gml:featureMember>";
			
		}

		$i++;
	}
	
	pg_free_result($sdbResult);
	pg_close($sdbConn);
	$wfs .= "\n</wfs:FeatureCollection>";
	
	header("Content-Type: text/xml");
	echo $wfs;
}

?>
