<?php
/* ---------------------------------------------------------------------
 * FILE: admin_webservice.php				DATE:04/20/2010
 * ---------------------------------------------------------------------
 * This script accepts a http request and returns a response as data
 * in the follow formats:
 * 		1. XML
 * 		2. JSON
 * 		3. KRUMO (Debugging)
 * 
 * PARAMETERS: 
 * 		Required: $output, $svcname
 * 		Optional: seecode, param1...*
 * 
 * EXAMPLE: ?svcname=0&output=krumo
 * ---------------------------------------------------------------------*/

require_once('../classes/krumo/class.krumo.php');
require_once('../includes/config.php');
require_once('../includes/functions.php');

switch ($svcname) {
    case 0: // used to get a list of users
		if($param0 != "" && $param1 != ""){
			$sql = "set rowcount ".$param1."\nselect [UserId], [UserName], [UserEmail], [UserFirstName], [UserLastName] from [users] where [UserId] > ".$param0."\nselect count([UserId]) from [users]";
		}
		break;
	case 1: // used to delete a user account
		if($param0 != ""){
			$sql = "delete from [users] where [UserId] = '".$param0."'";
		}
		break;
	case 2: // used to insert new user account
		if($param0 != "" && $param1 != "" && $param2 != ""){
			$sql = "insert  [users] ([UserName], [UserPassword], [UserEmail], [UserFirstName], [UserLastName] )values ('".$param0."', '".$param1."', '".$param2."', '".$param3."', '".$param4."')";
			$response = false;
		}
		break;
	case 3: //used to edit user account
		if($param0 != "" && $param1 != "" && $param2 != "" && $param3 != ""){
			$sql = "update  [users] set [UserName] = '".$param1."', [UserPassword] = '".$param2."', [UserEmail] = '".$param3."', [UserFirstName] = '".$param4."', [UserLastName] = '".$param5."' where [UserId] = ".$param0;
			$response = false;
		}
		break;
	case 4: //used to search user accounts
		if($param0 != "" && $param1 != ""){
			$sql = "select [UserId], [UserName], [UserEmail], [UserFirstName], [UserLastName] from [users] where [".$param0."] LIKE '%".$param1."%'";
		}
		break;
}

if($sql != ""){
	putenv('TDSVER=70');	
	$outputArr = array();
	$dbConn = mssql_connect($dbHost, $dbUser, $dbPass) or die("Could not connect to MSSQL: ".mssql_get_last_message());
	$selected = mssql_select_db($dbDatabase, $dbConn) or die("Couldn't open database $dbDatabase ".mssql_get_last_message());
	$dbResult = queryDB($sql,$dbConn);
	//enable return of multiple datasets
	if($response){
		$i = 0;
		do {	
			while($line = mssql_fetch_assoc($dbResult)){
				$item = array("item$i"=>$line);
				array_push($outputArr,$item);
			}
			$i++;
		}while (mssql_next_result($dbResult));
		mssql_free_result($dbResult);
		mssql_close($dbConn);
		echo output_result($output, $outputArr);
	}else{
		$blank = array('Result'=>'No return expected');
		echo output_result($output, $blank);
	}
}
?>
