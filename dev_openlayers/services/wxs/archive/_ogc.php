<?php
//?service=WMS&version=1.1.1&Request=GetCapabilities
//?service=WMS&version=1.1.1&Request=GetMap&LAYERS=bathymetry&srs=EPSG 

if($_REQUEST['mapfile'] == ""){
	Echo "No params found. Please see examples below...<br></br>";
	Echo "<a href=\"?service=WMS&version=1.1.1&Request=GetCapabilities&mapfile=C:\ms4w\sds\maps\usa_al_russell.map\">WMS GetCapibilities</a><br/>";
	Echo "<a href=\"?SERVICE=wms&VERSION=1.1.1&LAYERS=russelco_400&REQUEST=GetMap&SRS=EPSG:4326&BBOX=724079.186149,564355.250000,1008962.813851,739694.750000&TRANSPARENT=TRUE&FORMAT=image/png&WIDTH=600&HEIGHT=500&mapfile=C:\ms4w\sds\maps\usa_al_russell.map\">WMS GetMap</a><br/>";
	
	Echo "<br/>";
	Echo "<a href=\"?SERVICE=wfs&VERSION=1.0.0&REQUEST=GetCapabilities&mapfile=C:\ms4w\sds\maps\usa_al_russell.map\">WFS GetCapibilities</a><br/>";
	Echo "<a href=\"?SERVICE=wfs&version=1.0.0&TYPENAME=p_parcel&REQUEST=GetFeature&mapfile=C:\ms4w\sds\maps\usa_al_russell.map\">WFS GetFeatures</a><br/>";
}else{

	define( "MODULE", "php_mapscript.dll" );
	if (!extension_loaded("MapScript"))dl(MODULE);

	$request = ms_newowsrequestobj();
	$request->loadparams();

	/*exampple on how to modify the parameters :
	forcing the version from 1.1.1 to 1.1.0 */
	$request->setParameter("version","1.1.0");
	
	ms_ioinstallstdouttobuffer();
	
		$oMap = ms_newMapobj($_REQUEST['mapfile']);
		$oMap->owsdispatch($request);
		$contenttype = ms_iostripstdoutbuffercontenttype();
		$buffer = ms_iogetstdoutbufferstring();
		
		if ($contenttype == 'image/png'){
			header('Content-type: image/png');
			ms_iogetStdoutBufferBytes();
		}else{
			header('Content-type: application/xml');
			echo $buffer;
		}
		
	ms_ioresethandlers();
}

?>
