<?php
/* ---------------------------------------------------------------------
 * FILE: admin_webservice.php				DATE:03/11/2010
 * ---------------------------------------------------------------------
 * This script accepts a http request and returns a response as data
 * in the follow formats:
 * 		1. XML
 * 		2. JSON
 * 		3. KRUMO (Debugging)
 * 
 * PARAMETERS: 
 * 		Required: $output, $svcname
 * 		Optional: seecode, param1...*
 * 
 * EXAMPLE: ?svcname=0&output=krumo
 * ---------------------------------------------------------------------
 */

require_once('../../classes/krumo/class.krumo.php');
require_once('../../includes/config.php');
require_once('../../includes/functions.php');

//param0 = name
//param1 = parcel column
//param2 = parcel table
//param3 = view
//param4 = extent
switch ($svcname) {
    case 0: // search cama by owner parameter: returns standard results (parcel id, owner, addr)
		if($param0 != "" && $param1 != "" && $param2 != "" && $param3 != "" && $param4 != ""){
			$sql = "select * FROM ".$param3." where UPPER(name) LIKE UPPER('%".$param0."%')";
		}
		break;
}

if($sql != ""){
	putenv('TDSVER=70');	
	$outputArr = array();
	$dbConn = mssql_connect($dbHost, $dbUser, $dbPass) or die("Could not connect to MSSQL: ".mssql_get_last_message());
	$selected = mssql_select_db($dbDatabase, $dbConn) or die("Couldn't open database $dbDatabase ".mssql_get_last_message());
	$dbResult = queryDB($sql,$dbConn);
	while($line = mssql_fetch_assoc($dbResult)){
		array_push($outputArr,$line['parcel']);
	}
	mssql_free_result($dbResult);
	mssql_close($dbConn);
	
	if(count($outputArr) >= 1){
		$squery1 = "SELECT ST_Extent(geometry(geog)) as gbox FROM \"".$param2."\"";
		$squery2 = "SELECT gid, \"".$param1."\" AS PARCELNO, ST_AsGML(geog) as GML, ST_Extent(geometry(geog)) as bbox FROM \"".$param2."\"";
		$x = 0;
		foreach($outputArr as $parcel){
			if($x == 0){
				$squery1 .= "WHERE \"PARCELID\" = '".$parcel."'";
				$squery2 .= "WHERE \"PARCELID\" = '".$parcel."'";
			}else{
				$squery1 .= " OR \"PARCELID\" = '".$parcel."'";
				$squery2 .= " OR \"PARCELID\" = '".$parcel."'";
			}
			$x++;
		}
		$squery2 .= " GROUP BY gid, PARCELNO, GML;";
		
		$sdbConn = pg_connect("host=".$sdbHost." port=".$sdbPort." dbname=".$sdbDatabase." user=".$sdbUser." password=".$sdbPass);
		$sdbResult = pg_query($sdbConn, $squery1);
		$extent = pg_fetch_row($sdbResult);
		$extent = str_replace("BOX(", "", $extent[0]);
		$extent = str_replace(")", "", $extent);
		$extent = str_replace(",", " ", $extent);	
		$wfs .= '<?xml version="1.0" encoding="ISO-8859-1"?>';
		$wfs .= "\n<wfs:FeatureCollection \n\txmlns:ms=\"http://mapserver.gis.umn.edu/mapserver\" \n\txmlns:wfs=\"http://www.opengis.net/wfs\" \n\txmlns:gml=\"http://www.opengis.net/gml\" \n\txmlns:ogc=\"http://www.opengis.net/ogc\" \n\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \n\txsi:schemaLocation=\"http://www.opengis.net/wfs http://schemas.opengis.net/wfs/1.0.0/WFS-basic.xsd \n\t\t\t\thttp://mapserver.gis.umn.edu/mapserver https://127.0.0.1/cgi-bin/mapserv?map=C:\ms4w\sds\maps\wfs_us_al_russell.map&amp;SERVICE=WFS&amp;VERSION=1.0.0&amp;REQUEST=DescribeFeatureType&amp;TYPENAME=".$param1."&amp;OUTPUTFORMAT=XMLSCHEMA\">";
		$wfs .= "\n\t<gml:boundedBy><gml:Box srsName=\"EPSG:4326\"><gml:coordinates>".$param4."</gml:coordinates></gml:Box></gml:boundedBy>";
		pg_free_result($sdbResult);
		pg_close($sdbConn);
		
		$sdbConn = pg_connect("host=".$sdbHost." port=".$sdbPort." dbname=".$sdbDatabase." user=".$sdbUser." password=".$sdbPass);
		$sdbResult = pg_query($sdbConn, $squery2);

		$i = 0;
		while ($line = pg_fetch_assoc($sdbResult)) {
			
			
			// check for parcelno
			if($line['parcelno'] != ""){
				
				$wfs .=	"\n\t<gml:featureMember>\n\t<ms:".$param2.">\n\t\t<gml:boundedBy>\n\t\t\t<gml:Box srsName=\"EPSG:4326\">\n\t\t\t\t<gml:coordinates>".$extent."</gml:coordinates>\n\t\t\t</gml:Box>\n\t\t</gml:boundedBy>\n";

				foreach($line as $key=>$value){
					
					if($key == 'gml'){
						$wfs .= "\t\t<ms:msGeometry>\n";
						$wfs .= "\t\t\t".$value;
						$wfs .= "\n\t\t</ms:msGeometry>\n";
					}else{
						$wfs .= "\t\t<ms:".strtoupper($key).">".$value."</ms:".strtoupper($key).">\n";
					}	
				}
				$wfs .= "\t\t<ms:GROUPBOX>".$extent."</ms:GROUPBOX>\n";
				$wfs .= "\t\t</ms:".$param2.">\n\t</gml:featureMember>";
				
			}

			$i++;
		}
		
		pg_free_result($sdbResult);
		pg_close($sdbConn);
		$wfs .= "\n</wfs:FeatureCollection>";
		
		header("Content-Type: text/xml");
		echo $wfs;
		
	}

}
?>
