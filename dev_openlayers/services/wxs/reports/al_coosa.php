<?php
/* ---------------------------------------------------------------------
 * FILE: index.php				DATE:06/28/2010
 * ---------------------------------------------------------------------
 * This script accepts a http request and build a PDF report package
 * ---------------------------------------------------------------------*/

require_once('../classes/krumo/class.krumo.php');
require_once('../includes/config.php');
require_once('../includes/functions.php');
require_once('../classes/class.ezpdf.php');

$dbHost = 'EMAPSWEB02\SQLEXPRESS';				
$dbUser = 'emapsSvcAcct';
$dbPass = 'P@$$W0rd';
$dbDatabase = 'ALCoosa_eMaps';

// change parcel list into an array and loop to build sql
$parcelArr = explode(",",$param0);

// ================================================================
// Begin PDF Generation
// ================================================================

$pdf = new Cezpdf('C','portrait');

// PUBLICATION INFO
$pdf->addInfo('Title', 'Coosa County');
$pdf->addInfo('Author', 'Coosa County, Alabama');
$pdf->addInfo('Subject', 'GIS Internet Report');
$pdf->addInfo('CreationDate', date("F j, Y, g:i a"));

// PAGE SETUP
//$pdf->openHere('Fit');
$pdf->ezSetMargins(30,50,20,20);
$pdf->selectFont('../fonts/Helvetica.afm');
$tmp = array('b'=>'../fonts/Helvetica-Bold.afm','bi'=>'../fonts/Helvetica-BoldOblique.afm','i'=>'../fonts/Helvetica-Oblique.afm');
$pdf->setFontFamily('../fonts/Helvetica.afm', $tmp);

// Loops through parcel list and build report(s)
$i = 0;
foreach($parcelArr as $parcel){
	
	
		// add graphic banner if available
		$pdf->ezImage('al_coosa_co.jpg',0,500,none);
		
		// CAMA: query cama db
		// -------------------------------------------------------------
		putenv('TDSVER=70');	
		$outputArr = array();
		$reportArr = array();
		$pdffields = array();
		
		//########################## BEGIN DATA PREP ##############################
		
		// debug
		//$parcel = "1002100000016002";
		
		$dbConn = mssql_connect($dbHost, $dbUser, $dbPass) or die("Could not connect to MSSQL: ".mssql_get_last_message());
		
		$sql = "SELECT 	DISTINCT *
				FROM				ALCoosa_eMaps.dbo.MASTER a
				LEFT OUTER JOIN		ALCoosa_eMaps.dbo.PARCEL b	
				ON					b.parcel_num = a.DORFOLIO
				WHERE 				a.POLYFOLIO = '".$parcel."'";
		
		$selected = mssql_select_db($dbDatabase, $dbConn) or die("Couldn't open database $dbDatabase ".mssql_get_last_message());
		$dbResult = queryDB($sql,$dbConn);
		while($line = mssql_fetch_assoc($dbResult)){
			array_push($outputArr,$line);
			$gascode = $line['p_gas'];
			$sewercode = $line['p_sewer'];
			$watercode = $line['p_water'];
			$districtcode = $line['tax_dist'];
			$accountno = $line['account_no'];
			$parcelnum = $line['parcel_num'];
		}
		array_push($reportArr, array('parceltable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult);
		
		
		$sql1 = "SELECT DISTINCT * FROM ALCoosa_eMaps.dbo.SW_GAS WHERE CODE = '".$gascode."'";
		$dbResult1 = queryDB($sql1,$dbConn);
		while($line = mssql_fetch_assoc($dbResult1)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('gastable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult1);
		
		
		$sql2 = "SELECT DISTINCT * FROM ALCoosa_eMaps.dbo.SW_SEWER WHERE CODE = '".$sewercode."'";
		$dbResult2 = queryDB($sql2,$dbConn);
		while($line = mssql_fetch_assoc($dbResult2)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('sewertable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult2);
		
		
		$sql3 = "SELECT DISTINCT * FROM ALCoosa_eMaps.dbo.SW_WATER WHERE CODE = '".$watercode."'";
		$dbResult3 = queryDB($sql3,$dbConn);
		while($line = mssql_fetch_assoc($dbResult3)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('watertable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult3);
		
		
		$sql4 = "SELECT DISTINCT * FROM ALCoosa_eMaps.dbo.SW_DISTRICT_CODE WHERE CODE = '".$districtcode."'";
		$dbResult4 = queryDB($sql4,$dbConn);
		while($line = mssql_fetch_assoc($dbResult4)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('districttable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult4);
		
		
		$sql5 = "SELECT DISTINCT *
				FROM 	ALCoosa_eMaps.dbo.ADDRESS 
				WHERE 	account_no = '".$accountno."'";
		$dbResult5 = queryDB($sql5,$dbConn);
		while($line = mssql_fetch_assoc($dbResult5)){
			array_push($outputArr,$line);
			$zipcode = $line['zip'];
		}
		array_push($reportArr, array('addresstable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult5);
		
		
		$sql6 = "SELECT DISTINCT * FROM ALCoosa_eMaps.dbo.LOT WHERE parcel_num = '".$parcelnum."'";
		$dbResult6 = queryDB($sql6,$dbConn);
		while($line = mssql_fetch_assoc($dbResult6)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('lottable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult6);
		
		
		$sql7 = "SELECT DISTINCT * FROM ALCoosa_eMaps.dbo.ACREAGE WHERE parcel_num = '".$parcelnum."'";
		$dbResult7 = queryDB($sql7,$dbConn);
		while($line = mssql_fetch_assoc($dbResult7)){
			array_push($outputArr,$line);
			$landcode = $line['land_type'];
		}
		array_push($reportArr, array('acreagetable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult7);
		
		
		$sql8 = "SELECT DISTINCT * FROM ALCoosa_eMaps.dbo.SW_ZIPCODES WHERE ZIP = '".$zipcode."'";
		$dbResult8 = queryDB($sql8,$dbConn);
		while($line = mssql_fetch_assoc($dbResult8)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('zipcodetable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult8);
		
		
		$sql9 = "SELECT DISTINCT * FROM ALCoosa_eMaps.dbo.SW_LAND_TYPE WHERE CODE = '".$landcode."'";
		$dbResult9 = queryDB($sql9,$dbConn);
		while($line = mssql_fetch_assoc($dbResult9)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('landtypetable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult9);
		
		
		$sql10 = "SELECT DISTINCT * FROM ALCoosa_eMaps.dbo.BUILDING WHERE parcel_num = '".$parcelnum."'";
		$dbResult10 = queryDB($sql10,$dbConn);
		while($line = mssql_fetch_assoc($dbResult10)){
			array_push($outputArr,$line);
			$fmcode1 = $line['fm_code1'];
			$fmcode2 = $line['fm_code2'];
			$fmper1 = $line['fm_per1'];
			$fmper2 = $line['fm_per2'];
			$ftcode1 = $line['ft_code1'];
			$ftcode2 = $line['ft_code2'];
			$ftper1 = $line['ft_per1'];
			$ftper2 = $line['ft_per2'];
			$ewcode1 = $line['ew_code1'];
			$ewcode2 = $line['ew_code2'];
			$ewcode3 = $line['ew_code3'];
			$ewcode4 = $line['ew_code4'];
			$ewper1 = $line['ew_per1'];
			$ewper2 = $line['ew_per2'];
			$ewper3 = $line['ew_per3'];
			$ewper4 = $line['ew_per4'];
			$ifcode1 = $line['if_code1'];
			$ifcode2 = $line['if_code2'];
			$ifcode3 = $line['if_code3'];
			$ifcode4 = $line['if_code4'];
			$ifper1 = $line['if_per1'];
			$ifper2 = $line['if_per2'];
			$ifper3 = $line['if_per3'];
			$ifper4 = $line['if_per4'];
			$flcode1 = $line['fl_code1'];
			$flcode2 = $line['fl_code2'];
			$flcode3 = $line['fl_code3'];
			$flcode4 = $line['fl_code4'];
			$flper1 = $line['fl_per1'];
			$flper2 = $line['fl_per2'];
			$flper3 = $line['fl_per3'];
			$flper4 = $line['fl_per4'];
			$rtcode1 = $line['rt_code1'];
			$rtcode2 = $line['rt_code2'];
			$rtcode3 = $line['rt_code3'];
			$rtcode4 = $line['rt_code4'];
			$rtper1 = $line['rt_per1'];
			$rtper2 = $line['rt_per2'];
			$rtper3 = $line['rt_per3'];
			$rtper4 = $line['rt_per4'];
			$rmcode1 = $line['rm_code1'];
			$rmcode2 = $line['rm_code2'];
			$rmcode3 = $line['rm_code3'];
			$rmcode4 = $line['rm_code4'];
			$rmper1 = $line['rm_per1'];
			$rmper2 = $line['rm_per2'];
			$rmper3 = $line['rm_per3'];
			$rmper4 = $line['rm_per4'];
			$plsfcode1 = $line['plsf_code1'];
			$plsfcode2 = $line['plsf_code2'];
			$plsfcode3 = $line['plsf_code3'];
			$plsfcode4 = $line['plsf_code4'];
			$plsfcode5 = $line['plsf_code5'];
			$plsfcode6 = $line['plsf_code6'];
			$heatcode1 = $line['heat_code1'];
			$heatcode2 = $line['heat_code2'];
			$heatcode3 = $line['heat_code3'];
			$fireplacecode1 = $line['fp_code1'];
			$fireplacecode2 = $line['fp_code2'];
			
		}
		array_push($reportArr, array('buildingtable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult10);
		
		
		$sql11 = "SELECT DISTINCT * FROM ALCoosa_eMaps.dbo.GROSS WHERE parcel_num = '".$parcelnum."'";
		$dbResult11 = queryDB($sql11,$dbConn);
		while($line = mssql_fetch_assoc($dbResult11)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('grosstable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult11);
		
		
		$sql12 = "SELECT DISTINCT * FROM ALCoosa_eMaps.dbo.MHOME WHERE parcel_num = '".$parcelnum."'";
		$dbResult12 = queryDB($sql12,$dbConn);
		while($line = mssql_fetch_assoc($dbResult12)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('mhometable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult12);
		
		
		$sql13 = "SELECT DISTINCT * FROM ALCoosa_eMaps.dbo.OBY WHERE parcel_num = '".$parcelnum."'";
		$dbResult13 = queryDB($sql13,$dbConn);
		while($line = mssql_fetch_assoc($dbResult13)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('obytable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult13);
		
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		$sql14 = "SELECT DISTINCT a.code as foundationtypecode, a.description as foundationtypedescription, b.code as percentagecode, b.description as percentagedescription FROM ALCoosa_eMaps.dbo.SW_FOUNDATION a, SW_PERCENTAGES b WHERE (a.Code = '".$ftcode1."' OR a.Code = '".$ftcode2."') AND (b.CODE = '".$fmper1."' OR b.CODE = '".$fmper2."')";
		$dbResult14 = queryDB($sql14,$dbConn);
		while($line = mssql_fetch_assoc($dbResult14)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('foundationtypetable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult14);
		
		
		$sql15 = "SELECT DISTINCT a.code as foundationmaterialcode, a.description as foundationmaterialdescription, b.code as percentagecode, b.description as percentagedescription FROM ALCoosa_eMaps.dbo.SW_FOUNDATION a, SW_PERCENTAGES b WHERE (a.Code = '".$fmcode1."' OR a.Code = '".$fmcode2."') AND (b.CODE = '".$fmper1."' OR b.CODE = '".$fmper2."')";
		$dbResult15 = queryDB($sql15,$dbConn);
		while($line = mssql_fetch_assoc($dbResult15)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('foundationmaterialtable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult15);
		
		
		$sql16 = "SELECT DISTINCT a.CODE as ewcode, a.DESCR as ewdescription, a.UNITS as ewunits, b.Code as percentagecode, b.Description as percentagedescription FROM ALCoosa_eMaps.dbo.SW_EXTERIOR_WALLS a, SW_PERCENTAGES b WHERE (a.CODE = '".$ewcode1."' OR a.CODE = '".$ewcode2."' OR a.CODE = '".$ewcode3."' OR a.CODE = '".$ewcode4."') AND (b.CODE = '".$ewper1."' OR b.CODE = '".$ewper2."' OR b.CODE = '".$ewper3."' OR b.CODE = '".$ewper4."')";
		$dbResult16 = queryDB($sql16,$dbConn);
		while($line = mssql_fetch_assoc($dbResult16)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('exteriorwallstable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult16);
		
		
		$sql17 = "SELECT DISTINCT a.CODE as ifcode, a.DESCR as ifdescription, a.UNITS as ifunits, b.Code as percentagecode, b.Description as percentagedescription FROM ALCoosa_eMaps.dbo.SW_INTERIOR_FINISH a, SW_PERCENTAGES b WHERE (a.CODE = '".$ifcode1."' OR a.CODE = '".$ifcode2."' OR a.CODE = '".$ifcode3."' OR a.CODE = '".$ifcode4."') AND (b.CODE = '".$ifper1."' OR b.CODE = '".$ifper2."' OR b.CODE = '".$ifper3."' OR b.CODE = '".$ifper4."')";
		$dbResult17 = queryDB($sql17,$dbConn);
		while($line = mssql_fetch_assoc($dbResult17)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('interiorfinishtable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult17);
		
		
		$sql18 = "SELECT DISTINCT * FROM ALCoosa_eMaps.dbo.SW_FLOOR a, SW_PERCENTAGES b WHERE (a.CODE = '".$flcode1."' OR a.CODE = '".$flcode2."' OR a.CODE = '".$flcode3."' OR a.CODE = '".$flcode4."') AND (b.CODE = '".$flper1."' OR b.CODE = '".$flper2."' OR b.CODE = '".$flper3."' OR b.CODE = '".$flper4."')";
		$dbResult18 = queryDB($sql18,$dbConn);
		while($line = mssql_fetch_assoc($dbResult18)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('floortable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult18);
		
		
		$sql19 = "SELECT DISTINCT * FROM ALCoosa_eMaps.dbo.SW_ROOF_TYPE a, SW_PERCENTAGES b WHERE (a.CODE = '".$rtcode1."' OR a.CODE = '".$rtcode2."' OR a.CODE = '".$rtcode3."' OR a.CODE = '".$rtcode4."') AND (b.CODE = '".$rtper1."' OR b.CODE = '".$rtper2."' OR b.CODE = '".$rtper3."' OR b.CODE = '".$rtper4."')";
		$dbResult19 = queryDB($sql19,$dbConn);
		while($line = mssql_fetch_assoc($dbResult19)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('rooftypetable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult19);
		
		
		$sql20 = "SELECT DISTINCT * FROM ALCoosa_eMaps.dbo.SW_ROOF_MATERIAL a, SW_PERCENTAGES b WHERE (a.CODE = '".$rmcode1."' OR a.CODE = '".$rmcode2."' OR a.CODE = '".$rmcode3."' OR a.CODE = '".$rmcode4."') AND (b.CODE = '".$rmper1."' OR b.CODE = '".$rmper2."' OR b.CODE = '".$rmper3."' OR b.CODE = '".$rmper4."')";
		$dbResult20 = queryDB($sql20,$dbConn);
		while($line = mssql_fetch_assoc($dbResult20)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('roofmaterialtable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult20);
		
		$sql21 = "SELECT DISTINCT a.plsf_no1, a.plsf_no2, a.plsf_no3, a.plsf_no4, a.plsf_no5, a.plsf_no6, b.DESCR FROM ALCoosa_eMaps.dbo.BUILDING a, ALCoosa_eMaps.dbo.SW_PLUMBING b WHERE a.parcel_num = '".$parcelnum."' AND (CODE = '".$plsfcode1."' OR CODE = '".$plsfcode2."' OR CODE = '".$plsfcode3."' OR CODE = '".$plsfcode4."' OR CODE = '".$plsfcode5."' OR CODE = '".$plsfcode6."')";
		$dbResult21 = queryDB($sql21,$dbConn);
		while($line = mssql_fetch_assoc($dbResult21)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('plumbingtable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult21);


		$sql22 = "SELECT DISTINCT a.plsf_no1, a.plsf_no2, a.plsf_no3, a.plsf_no4, a.plsf_no5, a.plsf_no6, b.Description FROM ALCoosa_eMaps.dbo.BUILDING a, ALCoosa_eMaps.dbo.SW_SPECIAL_ADJUSTMENT b WHERE a.parcel_num = '".$parcelnum."' AND (b.CODE = '".$plsfcode1."' OR b.CODE = '".$plsfcode2."' OR b.CODE = '".$plsfcode3."' OR b.CODE = '".$plsfcode4."' OR b.CODE = '".$plsfcode5."' OR b.CODE = '".$plsfcode6."')";
		$dbResult22 = queryDB($sql22,$dbConn);
		while($line = mssql_fetch_assoc($dbResult22)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('specialadjustmenttable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult22);
		
		
		$sql23 = "SELECT DISTINCT a.heat_area1, a.heat_area2, a.heat_area3, b.DESCR FROM ALCoosa_eMaps.dbo.BUILDING a, ALCoosa_eMaps.dbo.SW_HEAT_AIR b WHERE a.parcel_num = '".$parcelnum."' AND (b.CODE = '".$heatcode1."' OR b.CODE = '".$heatcode2."' OR b.CODE = '".$heatcode3."')";
		$dbResult23 = queryDB($sql23,$dbConn);
		while($line = mssql_fetch_assoc($dbResult23)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('heatairtable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult23);
		
		
		$sql24 = "SELECT DISTINCT a.fp_no1, a.fp_no2, b.DESCR FROM ALCoosa_eMaps.dbo.BUILDING a, ALCoosa_eMaps.dbo.SW_FIREPLACES b WHERE a.parcel_num = '".$parcelnum."' AND (b.CODE = '".$fireplacecode1."' OR b.CODE = '".$fireplacecode2."')";
		$dbResult24 = queryDB($sql24,$dbConn);
		while($line = mssql_fetch_assoc($dbResult24)){
			array_push($outputArr,$line);
		}
		array_push($reportArr, array('fireplacestable' => $outputArr));
		$outputArr = array();
		mssql_free_result($dbResult24);
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////

		
		mssql_close($dbConn);
		
		
		// debug: data collection
		//krumo($reportArr);

		// organize the data by layout
		array_push($pdffields, array('ParcelNumber' => $reportArr[0]['parceltable'][0]['parcel_num']));
		array_push($pdffields, array('AccountNumber' => $reportArr[0]['parceltable'][0]['account_no']));
		array_push($pdffields, array('YearLastAppraised' => $reportArr[0]['parceltable'][0]['yr_apprsd']));

		// calc total land appraised value
		$totalapp = $reportArr[0]['buildingtable'][10]['cu_appr'] + $reportArr[11]['grosstable'][0]['cu_appr'] + $reportArr[6]['lottable'][0]['cu_appr'] + $reportArr[7]['acreagetable'][0]['cu_appr'] + $reportArr[12]['mhometable'][0]['cu_appr'] + $reportArr[13]['obytable'][0]['cu_appr'];
		$landappr = $reportArr[0]['parceltable'][0]['land_appr'];
		if($totalapp >= 1){
			$totalappval = $totalapp;
		}else{
			$totalappval = $landappr;
		}
		array_push($pdffields, array('TotalAppraisedValue' => $totalappval));

		array_push($pdffields, array('Owner1' => $reportArr[5]['addresstable'][0]['name']));
		array_push($pdffields, array('Owner2' => $reportArr[5]['addresstable'][0]['name2']));

		// concat property address from parcel table
		$propaddr = $reportArr[0]['parceltable'][0]['p_street']." ".$reportArr[0]['parceltable'][0]['p_address'];
		array_push($pdffields, array('PropertyAddress' => $propaddr));

		// concat mail address from address and zipcode tables
		if($reportArr[5]['addresstable'][0]['address2'] == ""){
			$mailaddr = $reportArr[5]['addresstable'][0]['address']."\n".$reportArr[8]['zipcodetable'][0]['CITY'].", ".$reportArr[8]['zipcodetable'][0]['STATE']." ".$reportArr[8]['zipcodetable'][0]['ZIP'];
		}else{
			$mailaddr = $reportArr[5]['addresstable'][0]['address']."\n".$reportArr[5]['addresstable'][0]['address2']."\n".$reportArr[8]['zipcodetable'][0]['CITY'].", ".$reportArr[8]['zipcodetable'][0]['STATE']." ".$reportArr[8]['zipcodetable'][0]['ZIP'];
		}
		
		array_push($pdffields, array('MailingAddress' => $mailaddr));

		array_push($pdffields, array('TaxDistrict' => $reportArr[0]['parceltable'][0]['tax_dist']));
		array_push($pdffields, array('TotalTaxes' => $reportArr[5]['addresstable'][0]['total_tax_due']));
		array_push($pdffields, array('AmountPaid' => $reportArr[5]['addresstable'][0]['amt_paid']));
		array_push($pdffields, array('DeededAcre' => $reportArr[0]['parceltable'][0]['d_acre']));
		array_push($pdffields, array('DeedBook' => $reportArr[0]['parceltable'][0]['d_book']));
		array_push($pdffields, array('TotalAcre' => $reportArr[0]['parceltable'][0]['total_acre']));
		array_push($pdffields, array('DeedPage' => $reportArr[0]['parceltable'][0]['d_bpage']));
		array_push($pdffields, array('TimberAcre' => $reportArr[0]['parceltable'][0]['timber_acre']));
		array_push($pdffields, array('DeedDate' => $reportArr[0]['parceltable'][0]['d_date']));
		array_push($pdffields, array('DeededLotFrontage' => $reportArr[0]['parceltable'][0]['d_lotd']));
		array_push($pdffields, array('Section' => $reportArr[0]['parceltable'][0]['section']));
		array_push($pdffields, array('LotFrontage' => $reportArr[0]['parceltable'][0]['d_lotf']));
		array_push($pdffields, array('Township' => $reportArr[0]['parceltable'][0]['township']));
		array_push($pdffields, array('LandType' => $reportArr[9]['landtypetable'][0]['DESCR']));
		array_push($pdffields, array('Range' => $reportArr[0]['parceltable'][0]['range']));
		array_push($pdffields, array('Subdivision' => $reportArr[0]['parceltable'][0]['subdivision']));
		array_push($pdffields, array('Neighborhood' => $reportArr[0]['parceltable'][0]['nbhd']));
		array_push($pdffields, array('YearBuilt' => $reportArr[10]['buildingtable'][0]['yr_built']));
		array_push($pdffields, array('EffectiveYearBuilt' => $reportArr[10]['buildingtable'][0]['eff_yr_blt']));
		array_push($pdffields, array('YearRemodeled' => $reportArr[10]['buildingtable'][0]['yr_remodel']));
		array_push($pdffields, array('Apratments' => $reportArr[10]['buildingtable'][0]['no_rms_apt'] + $reportArr[10]['buildingtable'][0]['no_rms_apt1'] + $reportArr[10]['buildingtable'][0]['no_rms_apt2']));
		array_push($pdffields, array('BuildingRooms' => $reportArr[10]['buildingtable'][0]['no_rooms']));
		array_push($pdffields, array('Stories' => $reportArr[10]['buildingtable'][0]['story_type']));
		array_push($pdffields, array('FirstFloorArea' => $reportArr[10]['buildingtable'][0]['base_area']));
		array_push($pdffields, array('UpperFloorArea' => $reportArr[10]['buildingtable'][0]['upper_area']));
		array_push($pdffields, array('AppendageAdjustedArea' => $reportArr[10]['buildingtable'][0]['app_adj_area']));
		array_push($pdffields, array('UpperFloorAdjusted' => $reportArr[10]['buildingtable'][0]['upper_adj']));
		array_push($pdffields, array('TotalAdjustedArea' => $reportArr[10]['buildingtable'][0]['total_adj']));
		array_push($pdffields, array('PercentGood' => $reportArr[10]['buildingtable'][0]['condition']));
		array_push($pdffields, array('FunctionalObsolescence' => $reportArr[10]['buildingtable'][0]['f_obsolete']));
		array_push($pdffields, array('EconomicObsolescence' => $reportArr[10]['buildingtable'][0]['e_obsolete']));

		/*
		// These sub table are possible one to many and will need to be looped on the pdf layout
		// Sub-table: Foundation Type
		// Sub-table: Foundation Material
		// Sub-table: Exterior Walls
		// Sub-table: Interior Finish
		// Sub-table: Flooring
		// Sub-table: Roof Type
		// Sub-table: Roof Material
		// Sub-table: Plumbing & Special features
		// Sub-table: Misc - Heat & Air/Fireplaces
		// Sub-table: Special Adjustments
		*/


		// debug: data formating
		//krumo($pdffields);

		//############################# END DATA PREP ##################################################
		
		
		$pdf->addText(45,640,12,"<b>Parcel Number:</b>");
		$pdf->addText(140,640,12,$reportArr[0]['parceltable'][0]['parcel_num']);
		$pdf->addText(300,640,12,"<b>Year Last Appraised:</b>");
		$pdf->addText(440,640,12,$reportArr[0]['parceltable'][0]['yr_apprsd']);
		$pdf->addText(45,620,12,"<b>Account Number:</b>");
		$pdf->addText(160,620,12,$reportArr[0]['parceltable'][0]['account_no']);
		$pdf->addText(300,620,12,"<b>Total Appraised Value:</b>");
		$pdf->addText(440,620,12,"$".number_format($totalappval));
	
		$pdf->ezSetDy(-40);
		$cols = array('col1'=>'Col1', 'col2'=>'Col2');
		$data = array(	array('col1'=>"<b>Owner:</b>", 'col2'=>$reportArr[5]['addresstable'][0]['name']),
						array('col1'=>"<b>Owner:</b>", 'col2'=>$reportArr[5]['addresstable'][0]['name2']), 
						array('col1'=>"<b>Property Address:</b>", 'col2'=>$propaddr), 
						array('col1'=>"<b>Mailing Address:</b>", 'col2'=>$mailaddr));
		$options = array('showHeadings'=>0);
		$pdf->ezTable($data, $cols, "<b>GENERAL INFORMATION</b>", $options);
		
		$pdf->ezSetDy(-20);
		$cols = array('col1'=>'Col1', 'col2'=>'Col2');
		$data = array(array('col1'=>"<b>Tax District:</b>", 'col2'=>$reportArr[0]['parceltable'][0]['tax_dist']),array('col1'=>"<b>Total Taxes:</b>", 'col2'=>"$".number_format($reportArr[5]['addresstable'][0]['total_tax_due'])), array('col1'=>"<b>Amount Paid:</b>", 'col2'=>$reportArr[5]['addresstable'][0]['amt_paid']));
		$options = array('showHeadings'=>0);
		$pdf->ezTable($data, $cols, "<b>TAX INFORMATION</b>", $options);
		$pdf->ezSetDy(-5);
		$pdf->ezText("(Tax amount for year 2009. Subject to change for 2010 tax year.)",8,array('justification'=>'center'));
		
		$pdf->ezSetDy(-20);
		$cols = array('col1'=>'Col1', 'col2'=>'Col2', 'col3'=>'Col3', 'col4'=>'Col4');
		$data = array(
					array('col1'=>"<b>Deeded Acre:</b>", 		'col2'=>$reportArr[0]['parceltable'][0]['d_acre'], 		'col3'=>"<b>Deed Book:</b>", 	'col4'=>$reportArr[0]['parceltable'][0]['d_book']),
					array('col1'=>"<b>Total Acre:</b>", 		'col2'=>$reportArr[0]['parceltable'][0]['total_acre'], 	'col3'=>"<b>Deed Page:</b>", 	'col4'=>$reportArr[0]['parceltable'][0]['d_bpage']),
					array('col1'=>"<b>Timber Acre:</b>", 		'col2'=>$reportArr[0]['parceltable'][0]['timber_acre'], 'col3'=>"<b>Deed Date:</b>", 	'col4'=>$reportArr[0]['parceltable'][0]['d_date']),
					array('col1'=>"<b>Deeded Lot Frontage:</b>",'col2'=>$reportArr[0]['parceltable'][0]['d_lotd'], 		'col3'=>"<b>Section:</b>", 		'col4'=>$reportArr[0]['parceltable'][0]['section']),
					array('col1'=>"<b>Lot Frontage:</b>",		'col2'=>$reportArr[0]['parceltable'][0]['d_lotf'], 		'col3'=>"<b>Township:</b>", 	'col4'=>$reportArr[0]['parceltable'][0]['township']),
					array('col1'=>"<b>Land Type:</b>",			'col2'=>$reportArr[9]['landtypetable'][0]['DESCR'], 	'col3'=>"<b>Range:</b>", 		'col4'=>$reportArr[0]['parceltable'][0]['range']),
					array('col1'=>"<b>Subdivision:</b>",		'col2'=>$reportArr[0]['parceltable'][0]['subdivision'], 'col3'=>"<b>Neighborhood:</b>", 'col4'=>$reportArr[0]['parceltable'][0]['nbhd'])
					);
		$options = array('showHeadings'=>0);
		$pdf->ezTable($data, $cols, "<b>OTHER INFORMATION</b>", $options);
		
		$pdf->setLineStyle(2);
		$pdf->line(50,230,550,230);
		
		$pdf->addText(140,200,20,"<b>Building Components Information</b>");
		
		$pdf->ezSetDy(-95);
		
		$cols = array('col1'=>'Col1', 'col2'=>'Col2', 'col3'=>'Col3', 'col4'=>'Col4', 'col5'=>'Col5', 'col6'=>'Col6', 'col7'=>'Col7', 'col8'=>'Col8');
		$data = array(
					array(	'col1'=>"<b>Type:</b>", 'col2'=>$reportArr[14]['foundationtypetable'][0]['foundationtypedescription'],
							'col3'=>"<b>Percentage:</b>", 'col4'=>$reportArr[14]['foundationtypetable'][0]['percentagedescription'],           
							'col5'=>"<b>Material:</b>", 'col6'=>$reportArr[15]['foundationmaterialtable'][0]['foundationmaterialdescription'],
							'col7'=>"<b>Percentage:</b>", 'col8'=>$reportArr[15]['foundationmaterialtable'][0]['percentagedescription']
						)
					);
		$options = array('showHeadings'=>0);
		$pdf->ezTable($data, $cols, "<b>FOUNDATION INFORMATION</b>", $options);
		
		$pdf->addText(198,50,12,"<b>Coosa County, Alabama Disclaimer</b>");
		$pdf->addText(155,40,10,"<i>Information deemed reliable but not guaranteed. Copyright 2010.</i>");
		$pdf->ezNewPage();
		
		
		$cols = array('col1'=>'<b>Name</b>', 'col2'=>'<b>Percentage</b>');
		$data = array();
		foreach($reportArr[16]['exteriorwallstable'] as $row){
			array_push($data, array('col1'=>$row['ewdescription'], 'col2'=>$row['percentagedescription']));
		}
		$options = array('showHeadings'=>1);
		$pdf->ezTable($data, $cols, "<b>EXTERIOR WALLS</b>", $options);
		
		$pdf->ezSetDy(-20);
		
		$cols = array('col1'=>'<b>Name</b>', 'col2'=>'<b>Percentage</b>');
		$data = array();
		foreach($reportArr[17]['interiorfinishtable'] as $row){
			array_push($data, array('col1'=>$row['ifdescription'], 'col2'=>$row['percentagedescription']));
		}
		$options = array('showHeadings'=>1);
		$pdf->ezTable($data, $cols, "<b>INTERIOR FINISH</b>", $options);
		
		$pdf->ezSetDy(-20);
		
		$cols = array('col1'=>'<b>Name</b>', 'col2'=>'<b>Percentage</b>');
		$data = array();
		foreach($reportArr[18]['floortable'] as $row){
			array_push($data, array('col1'=>$row['DESCR'], 'col2'=>$row['Description']));
		}
		$options = array('showHeadings'=>1);
		$pdf->ezTable($data, $cols, "<b>FLOORING</b>", $options);
		
		$pdf->ezSetDy(-20);
		
		$cols = array('col1'=>'<b>Name</b>', 'col2'=>'<b>Percentage</b>');
		$data = array();
		foreach($reportArr[19]['rooftypetable'] as $row){
			array_push($data, array('col1'=>$row['DESCR'], 'col2'=>$row['Description']));
		}
		$options = array('showHeadings'=>1);
		$pdf->ezTable($data, $cols, "<b>ROOF TYPE</b>", $options);
		
		$pdf->ezSetDy(-20);
		
		$cols = array('col1'=>'<b>Name</b>', 'col2'=>'<b>Percentage</b>');
		$data = array();
		foreach($reportArr[20]['roofmaterialtable'] as $row){
			array_push($data, array('col1'=>$row['DESCR'], 'col2'=>$row['Description']));
		}
		$options = array('showHeadings'=>1);
		$pdf->ezTable($data, $cols, "<b>ROOF MATERIAL</b>", $options);
		
		$pdf->ezSetDy(-20);
		
		$cols = array(
						'col1'=>'<b>Name</b>', 
						'col2'=>'<b>NO1</b>',
						'col3'=>'<b>NO2</b>',
						'col4'=>'<b>NO3</b>',
						'col5'=>'<b>NO4</b>',
						'col6'=>'<b>NO5</b>',
						'col7'=>'<b>NO6</b>',
				);
		$data = array();
		foreach($reportArr[21]['plumbingtable'] as $row){
			array_push($data, array(
									'col1'=>$row['DESCR'],
									'col2'=>$row['plsf_no1'],
									'col3'=>$row['plsf_no2'],
									'col4'=>$row['plsf_no3'],
									'col5'=>$row['plsf_no4'],
									'col6'=>$row['plsf_no5'],
									'col7'=>$row['plsf_no6']
								)
						);
		}
		$options = array('showHeadings'=>1);
		$pdf->ezTable($data, $cols, "<b>PLUMBING SPECIAL FEATURES</b>", $options);
		
		$pdf->ezSetDy(-20);
		
		$cols = array(
						'col1'=>'<b>Name</b>', 
						'col2'=>'<b>NO1</b>',
						'col3'=>'<b>NO2</b>',
						'col4'=>'<b>NO3</b>',
						'col5'=>'<b>NO4</b>',
						'col6'=>'<b>NO5</b>',
						'col7'=>'<b>NO6</b>',
				);
		$data = array();
		foreach($reportArr[22]['specialadjustmenttable'] as $row){
			array_push($data, array(
									'col1'=>$row['Description'],
									'col2'=>$row['plsf_no1'],
									'col3'=>$row['plsf_no2'],
									'col4'=>$row['plsf_no3'],
									'col5'=>$row['plsf_no4'],
									'col6'=>$row['plsf_no5'],
									'col7'=>$row['plsf_no6']
								)
						);
		}
		$options = array('showHeadings'=>1);
		$pdf->ezTable($data, $cols, "<b>SPECIAL ADJUSTMENTS</b>", $options);
		
		$pdf->ezSetDy(-20);
		
		$cols = array(
						'col1'=>'<b>Name</b>', 
						'col2'=>'<b>Area1</b>',
						'col3'=>'<b>Area2</b>',
						'col4'=>'<b>Area3</b>'
				);
		$data = array();
		foreach($reportArr[23]['heatairtable'] as $row){
			array_push($data, array(
									'col1'=>$row['DESCR'],
									'col2'=>$row['heat_area1'],
									'col3'=>$row['heat_area2'],
									'col4'=>$row['heat_area3']
								)
						);
		}
		$options = array('showHeadings'=>1);
		$pdf->ezTable($data, $cols, "<b>HEAT & AIR CONDITION</b>", $options);
		
		$pdf->ezSetDy(-20);
		
		$cols = array(
						'col1'=>'<b>Name</b>', 
						'col2'=>'<b>NO1</b>',
						'col3'=>'<b>NO2</b>'
				);
		$data = array();
		foreach($reportArr[24]['fireplacestable'] as $row){
			array_push($data, array(
									'col1'=>$row['DESCR'],
									'col2'=>$row['fp_no1'],
									'col3'=>$row['fp_no2']
								)
						);
		}
		$options = array('showHeadings'=>1);
		$pdf->ezTable($data, $cols, "<b>FIREPLACES</b>", $options);
		
		
		$pdf->ezNewPage();
		
		$cols = array('col1'=>'Col1', 'col2'=>'Col2', 'col3'=>'Col3', 'col4'=>'Col4', 'col5'=>'Col5', 'col6'=>'Col6');
		$data = array(
					array(	'col1'=>"<b>Year Built:</b>",				'col2'=>$reportArr[10]['buildingtable'][0]['yr_built'], 		
							'col3'=>"<b>Effective Year Built:</b>", 	'col4'=>$reportArr[10]['buildingtable'][0]['eff_yr_blt'], 	
							'col5'=>"<b>Year Remodeled:</b>", 			'col6'=>$reportArr[10]['buildingtable'][0]['yr_remodel']),
					array(	'col1'=>"<b>Apartments:</b>",				'col2'=>$reportArr[10]['buildingtable'][0]['no_rms_apt'] + $reportArr[10]['buildingtable'][0]['no_rms_apt1'] + $reportArr[10]['buildingtable'][0]['no_rms_apt2'], 		
							'col3'=>"<b>Building Rooms:</b>", 			'col4'=>$reportArr[10]['buildingtable'][0]['no_rooms'], 	
							'col5'=>"<b>Stories:</b>", 					'col6'=>$reportArr[10]['buildingtable'][0]['story_type'])
					);
		$options = array('showHeadings'=>0);
		$pdf->ezTable($data, $cols, "<b>AGE INFORMATION</b>", $options);
		
		$pdf->ezSetDy(-40);
		
		$cols = array('col1'=>'Col1', 'col2'=>'Col2', 'col3'=>'Col3', 'col4'=>'Col4', 'col5'=>'Col5', 'col6'=>'Col6');
		$data = array(
					array(	'col1'=>"<b>First Floor Area:</b>",			'col2'=>$reportArr[10]['buildingtable'][0]['base_area'], 		
							'col3'=>"<b>Upper Floor Area:</b>", 		'col4'=>$reportArr[10]['buildingtable'][0]['upper_area'], 	
							'col5'=>"<b>Appendage Adjusted Area:</b>", 	'col6'=>$reportArr[10]['buildingtable'][0]['app_adj_area']),
					array(	'col1'=>"<b></b>",							'col2'=>"", 		
							'col3'=>"<b>Upper Floor Adjusted:</b>", 	'col4'=>$reportArr[10]['buildingtable'][0]['upper_adj'], 	
							'col5'=>"<b>Total Adjusted Area:</b>", 		'col6'=>$reportArr[10]['buildingtable'][0]['total_adj'])
					);
		$options = array('showHeadings'=>0);
		$pdf->ezTable($data, $cols, "<b>SQUARE FOOTAGE INFORMATION</b>", $options);
		
		$pdf->ezSetDy(-40);
		
		$cols = array('col1'=>'Col1', 'col2'=>'Col2', 'col3'=>'Col3', 'col4'=>'Col4', 'col5'=>'Col5', 'col6'=>'Col6');
		$data = array(
					array(	'col1'=>"<b>Percent Good:</b>",				'col2'=>$reportArr[10]['buildingtable'][0]['condition'], 		
							'col3'=>"<b>Functional Obsolescence:</b>", 	'col4'=>$reportArr[10]['buildingtable'][0]['f_obsolete'], 	
							'col5'=>"<b>Economic Obsolescence:</b>", 	'col6'=>$reportArr[10]['buildingtable'][0]['e_obsolete'])
					);
		$options = array('showHeadings'=>0);
		$pdf->ezTable($data, $cols, "<b>DEPRECIATION INFORMATION</b>", $options);
		
		$pdf->ezSetDy(-40);
		
		// MAP IMAGE: query spatial db -> bbox, make wms query
		// -------------------------------------------------------------
		$squery =  "SELECT ST_Extent(geometry(ST_Buffer(geog,300))) as bbox FROM \"US_AL_Coosa_Parcels\" WHERE \"PARCELID\" = '".$parcel."';";
		$sdbConn = pg_connect("host=".$sdbHost." port=".$sdbPort." dbname=".$sdbDatabase." user=".$sdbUser." password=".$sdbPass);
		$sdbResult = pg_query($sdbConn, $squery);
		while($line = pg_fetch_assoc($sdbResult)){
			// strip off box info
			$box = $line['bbox'];
			$bbox = str_replace("BOX(", "", $box);
			$bbox = str_replace(")", "", $bbox);
			$bbox = str_replace(" ", ",", $bbox);
		}
		$parcelImgSrc = $WMSUrl_alCoosa."SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&SRS=EPSG:4326&BBOX=".$bbox."&FORMAT=image/jpeg&EXCEPTIONS=application/vnd.ogc.se_inimage&LAYERS=US_AL_Coosa_Parcels,US_AL_Coosa_Lake,US_AL_Coosa_Water,US_AL_Coosa_Subdivision&WIDTH=500&HEIGHT=400&TILED=false&TRANSPARENT=FALSE";
		$pdf->ezImage($parcelImgSrc,0,0,fit);
		// -------------------------------------------------------------
		
		$pdf->addText(198,50,12,"<b>Coosa County, Alabama Disclaimer</b>");
		$pdf->addText(155,40,10,"<i>Information deemed reliable but not guaranteed. Copyright 2010.</i>");
}

// buffer output to browser
$pdf->ezStream();
// End PDF Generation and write to file.
//$pdfcode = $pdf->ezOutput();
//$fp = fopen('download.pdf','wb');
//fwrite($fp,$pdfcode);
//fclose($fp);

?>




