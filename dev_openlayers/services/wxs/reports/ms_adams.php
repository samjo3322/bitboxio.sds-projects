<?php
/* ---------------------------------------------------------------------
 * FILE: index.php				DATE:06/28/2010
 * ---------------------------------------------------------------------
 * This script accepts a http request and build a PDF report package
 * ---------------------------------------------------------------------*/

require_once('../classes/krumo/class.krumo.php');
require_once('../includes/config.php');
require_once('../includes/functions.php');
require_once('../classes/class.ezpdf.php');

// change parcel list into an array and loop to build sql
$parcelArr = explode(",",$param0);

// ================================================================
// Begin PDF Generation
// ================================================================

$pdf = new Cezpdf('C','portrait');

// PUBLICATION INFO
$pdf->addInfo('Title', 'Adams County');
$pdf->addInfo('Author', 'Adams County, Mississippi');
$pdf->addInfo('Subject', 'GIS Internet Report');
$pdf->addInfo('CreationDate', date("F j, Y, g:i a"));

// PAGE SETUP
//$pdf->openHere('Fit');
$pdf->ezSetMargins(30,50,20,20);
$pdf->selectFont('../fonts/Helvetica.afm');
$tmp = array('b'=>'../fonts/Helvetica-Bold.afm','bi'=>'../fonts/Helvetica-BoldOblique.afm','i'=>'../fonts/Helvetica-Oblique.afm');
$pdf->setFontFamily('../fonts/Helvetica.afm', $tmp);

// Loops through parcel list and build report(s)
$i = 0;
foreach($parcelArr as $parcel){
		
		// add graphic banner if available
		$pdf->ezImage('ms_adams_co.jpg',0,500,none);
		
		
		// CAMA: query cama db
		// -------------------------------------------------------------
		putenv('TDSVER=70');	
		$outputArr = array();
		$sql = "SELECT * FROM msadams_report_view WHERE parcel = '".$parcel."'";
		$dbConn = mssql_connect($dbHost, $dbUser, $dbPass) or die("Could not connect to MSSQL: ".mssql_get_last_message());
		$selected = mssql_select_db($dbDatabase, $dbConn) or die("Couldn't open database $dbDatabase ".mssql_get_last_message());
		$dbResult = queryDB($sql,$dbConn);
		while($line = mssql_fetch_assoc($dbResult)){
			array_push($outputArr,$line);
		}
		mssql_free_result($dbResult);
		mssql_close($dbConn);
		
		
		$pdf->addText(45,640,12,"<b>Parcel Number:</b>");
		$pdf->addText(140,640,12,$outputArr[0]['parcel']);
		$pdf->addText(300,640,12,"<b>Year Last Appraised:</b>");
		$pdf->addText(440,640,12,$outputArr[0]['appraisalyear']);
		
		$pdf->addText(45,620,12,"<b>Account Number:</b>");
		$pdf->addText(160,620,12,$outputArr[0]['account']);
		$pdf->addText(300,620,12,"<b>Total Appraised Value:</b>");
		$pdf->addText(440,620,12,$outputArr[0]['appraisalvalue']);
		
		$pdf->ezSetDy(-60);
		$cols = array('col1'=>'Col1', 'col2'=>'Col2');
		$data = array(array('col1'=>"<b>Owner:</b>", 'col2'=>$outputArr[0]['name']),array('col1'=>"<b>Company:</b>", 'col2'=>$outputArr[0]['company']), array('col1'=>"<b>Property Address:</b>", 'col2'=>$outputArr[0]['address']), array('col1'=>"<b>Mailing Address:</b>", 'col2'=>$outputArr[0]['mailaddress']));
		$options = array('showHeadings'=>0);
		$pdf->ezTable($data, $cols, "<b>GENERAL INFORMATION</b>", $options);

		$pdf->ezSetDy(-50);
		$cols = array('col1'=>'Col1', 'col2'=>'Col2');
		$data = array(array('col1'=>"<b>Tax District:</b>", 'col2'=>$outputArr[0]['taxdistrict']),array('col1'=>"<b>Tax Year:</b>", 'col2'=>$outputArr[0]['taxyear']), array('col1'=>"<b>Total Taxes:</b>", 'col2'=>$outputArr[0]['taxtotal']), array('col1'=>"<b>Amount Paid:</b>", 'col2'=>$outputArr[0]['taxpaid']));
		$options = array('showHeadings'=>0);
		$pdf->ezTable($data, $cols, "<b>TAX INFORMATION</b>", $options);
		$pdf->ezSetDy(-5);
		$pdf->ezText("(Tax amount for year 2009. Subject to change for 2010 tax year.)",8,array('justification'=>'center'));
		
		$pdf->ezSetDy(-50);
		$cols = array('col1'=>'Col1', 'col2'=>'Col2', 'col3'=>'Col3', 'col4'=>'Col4');
		$data = array(
					array('col1'=>"<b>Deeded Acre:</b>", 			'col2'=>$outputArr[0]['deedacre'], 			'col3'=>"<b>Deed Book:</b>", 		'col4'=>$outputArr[0]['deedbook']),
					array('col1'=>"<b>Total Acre:</b>", 			'col2'=>$outputArr[0]['acretotal'], 		'col3'=>"<b>Deed Page:</b>", 		'col4'=>$outputArr[0]['deedpage']),
					array('col1'=>"<b>Timber Acre:</b>", 			'col2'=>$outputArr[0]['acretimber'], 		'col3'=>"<b>Deed Date:</b>", 		'col4'=>$outputArr[0]['salesdate']),
					array('col1'=>"<b>Deeded Lot Frontage:</b>", 	'col2'=>$outputArr[0]['deedlotfrontage'], 	'col3'=>"<b>Section:</b>", 			'col4'=>$outputArr[0]['section']),
					array('col1'=>"<b>Lot Frontage:</b>", 			'col2'=>$outputArr[0]['frontage'], 			'col3'=>"<b>Township:</b>", 		'col4'=>$outputArr[0]['township']),
					array('col1'=>"<b>Land Type:</b>", 				'col2'=>$outputArr[0]['description'], 		'col3'=>"<b>Range:</b>", 			'col4'=>$outputArr[0]['range']),
					array('col1'=>"<b>Subdivision:</b>", 			'col2'=>$outputArr[0]['subdivision'], 		'col3'=>"<b>Neighborhood:</b>", 	'col4'=>$outputArr[0]['nbhd'])
					);
		$options = array('showHeadings'=>0);
		$pdf->ezTable($data, $cols, "<b>OTHER INFORMATION</b>", $options);
		$pdf->addText(175,54,12,"<b>Adams County, Mississippi Disclaimer</b>");
		$pdf->addText(155,40,10,"<i>Information deemed reliable but not guaranteed. Copyright 2010.</i>");
		$pdf->setColor(255,0,0);
		$pdf->addText(120,220,128,"<b><i>SAMPLE</i></b>",-45);
		$pdf->setColor(0,0,0);
		$pdf->ezNewPage();
		// -------------------------------------------------------------
		
		
			
		// MAP IMAGE: query spatial db -> bbox, make wms query
		// -------------------------------------------------------------
		$squery =  "SELECT ST_Extent(geometry(ST_Buffer(geog,300))) as bbox FROM \"US_MS_Adams_Parcels\" WHERE \"PARCELID\" = '".$parcel."';";
		$sdbConn = pg_connect("host=".$sdbHost." port=".$sdbPort." dbname=".$sdbDatabase." user=".$sdbUser." password=".$sdbPass);
		$sdbResult = pg_query($sdbConn, $squery);
		while($line = pg_fetch_assoc($sdbResult)){
			// strip off box info
			$box = $line['bbox'];
			$bbox = str_replace("BOX(", "", $box);
			$bbox = str_replace(")", "", $bbox);
			$bbox = str_replace(" ", ",", $bbox);
		}
		$parcelImgSrc = $WMSUrl_msAdams."SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&SRS=EPSG:4326&BBOX=".$bbox."&FORMAT=image/jpeg&EXCEPTIONS=application/vnd.ogc.se_inimage&LAYERS=US_MS_Adams_Parcels,US_MS_Adams_CityStreet,US_MS_Adams_CountyRoad,US_MS_Adams_USRoads,US_MS_Adams_RailRoad,US_MS_Adams_County_Anno,US_MS_Adams_Subdivisions_Anno,US_MS_Adams_LotNumber,US_MS_Adams_ParcelNumber&WIDTH=500&HEIGHT=400&TILED=false&TRANSPARENT=FALSE";
		$pdf->ezImage($parcelImgSrc,0,0,fit);
		// -------------------------------------------------------------

		//$pdf->addText(150,300,10,"the quick brown fox <b>jumps</b><i>over</i> the lazy dog!",-10);
		$pdf->ezNewPage();
}

// buffer output to browser
$pdf->ezStream();
// End PDF Generation and write to file.
//$pdfcode = $pdf->ezOutput();
//$fp = fopen('download.pdf','wb');
//fwrite($fp,$pdfcode);
//fclose($fp);
?>




