<?php
/* ---------------------------------------------------------------------
 * FILE: webservice.php				DATE:06/10/2010
 * ---------------------------------------------------------------------
 * This script accepts a http request and returns a response as data
 * in the follow formats:
 * 		1. XML
 * 		2. JSON
 * 		3. KRUMO (Debugging)
 * 
 * PARAMETERS: 
 * 		Required: $output, $svcname
 * 		Optional: seecode, param1...*
 * 
 * EXAMPLE: ?svcname=0&output=krumo
 * ---------------------------------------------------------------------
 */

require_once('../classes/krumo/class.krumo.php');
require_once('../includes/config.php');
require_once('../includes/functions.php');  

//debug
//$svcname = '0';											// server id
//$output = 'xml';											// output
//$param0 = 'PARCELNO';										// sds id
//$param1 = 'AL_Russell_Parcels';							// pg table
//$param2 = 'POINT(-73.17128 40.61515)';					// geometry
//$param3 = '500';											// buffer
//$param4 = '-72.981969,40.606939 -72.981129,40.607588'; 	// extent


switch ($svcname) {
	// SELECT: performs a spatial query (dwithin) and returns parcel no(s).
	// $param0 - colum(s) to select
	// $param1 - table(s) to select
	// $param2 - geometry (pg syntax)
	// $param3 - distance units in meters
	// $param4 - map extent
    case 0: // SELECT: performs a spatial query (dwithin) and returns parcel no(s).
		if($param0 != "" && $param1 != "" && $param2 != "" && $param3 != ""){
			$sql1 = "SELECT ST_Extent(geometry(geog)) as gbox FROM \"".$param1."\" WHERE ST_DWithin(geog, geography(ST_GeomFromText('".$param2."')), ".$param3.");";
			$sql2 = "SELECT gid, \"PARCELID\" as PARCELNO, ST_AsGML(geog) as GML, ST_Extent(geometry(geog)) as bbox FROM \"".$param1."\" WHERE ST_DWithin(geog, geography(ST_GeomFromText('".$param2."')), ".$param3.") GROUP BY gid, PARCELNO, GML;";
		}
		break;
}

//echo $sql1."<p></p>";
//echo $sql2;

if($sql1 != "" && $sql2 != ""){
	
	$sdbConn = pg_connect("host=".$sdbHost." port=".$sdbPort." dbname=".$sdbDatabase." user=".$sdbUser." password=".$sdbPass);
	$sdbResult = pg_query($sdbConn, $sql1);
	$extent = pg_fetch_row($sdbResult);
	$extent = str_replace("BOX(", "", $extent[0]);
	$extent = str_replace(")", "", $extent);
	$extent = str_replace(",", " ", $extent);
			
	$wfs .= '<?xml version="1.0" encoding="ISO-8859-1"?>';
	$wfs .= "\n<wfs:FeatureCollection \n\txmlns:ms=\"http://mapserver.gis.umn.edu/mapserver\" \n\txmlns:wfs=\"http://www.opengis.net/wfs\" \n\txmlns:gml=\"http://www.opengis.net/gml\" \n\txmlns:ogc=\"http://www.opengis.net/ogc\" \n\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \n\txsi:schemaLocation=\"http://www.opengis.net/wfs http://schemas.opengis.net/wfs/1.0.0/WFS-basic.xsd \n\t\t\t\thttp://mapserver.gis.umn.edu/mapserver https://127.0.0.1/cgi-bin/mapserv?map=C:\ms4w\sds\maps\wfs_us_al_russell.map&amp;SERVICE=WFS&amp;VERSION=1.0.0&amp;REQUEST=DescribeFeatureType&amp;TYPENAME=".$param1."&amp;OUTPUTFORMAT=XMLSCHEMA\">";
	$wfs .= "\n\t<gml:boundedBy><gml:Box srsName=\"EPSG:4326\"><gml:coordinates>".$extent."</gml:coordinates></gml:Box></gml:boundedBy>";
	
	pg_free_result($sdbResult);
	pg_close($sdbConn);
	
	
	$outputArr = array();
	$sdbConn = pg_connect("host=".$sdbHost." port=".$sdbPort." dbname=".$sdbDatabase." user=".$sdbUser." password=".$sdbPass);
	$sdbResult = pg_query($sdbConn, $sql2);
	
	//$extent = pg_fetch_row($sdbResult
	
	
	

	$i = 0;
	while ($line = pg_fetch_assoc($sdbResult)) {
		
		if($i == 0){
			
			
		}
		
		
		// check for parcelno
		if($line['parcelno'] != ""){
			
			$wfs .=	"\n\t<gml:featureMember>\n\t<ms:".$param1.">\n\t\t<gml:boundedBy>\n\t\t\t<gml:Box srsName=\"EPSG:4326\">\n\t\t\t\t<gml:coordinates>".$extent."</gml:coordinates>\n\t\t\t</gml:Box>\n\t\t</gml:boundedBy>\n";

			foreach($line as $key=>$value){
				
				if($key == 'gml'){
					$wfs .= "\t\t<ms:msGeometry>\n";
					$wfs .= "\t\t\t".$value;
					$wfs .= "\n\t\t</ms:msGeometry>\n";
				}else{
					$wfs .= "\t\t<ms:".strtoupper($key).">".$value."</ms:".strtoupper($key).">\n";
				}	
				
				
			}
			$wfs .= "\t\t<ms:GROUPBOX>".$extent."</ms:GROUPBOX>\n";
			$wfs .= "\t\t</ms:".$param1.">\n\t</gml:featureMember>";
			
		}

		$i++;
	}
	
	pg_free_result($sdbResult);
	pg_close($sdbConn);
	$wfs .= "\n</wfs:FeatureCollection>";
	
	header("Content-Type: text/xml");
	echo $wfs;
}

?>
