<?php
/* ---------------------------------------------------------------------
 * FILE: index.php				DATE:04/20/2010
 * ---------------------------------------------------------------------
 * This script accepts a http request and returns a response as data
 * in the follow formats:
 * 		1. XML
 * 		2. JSON
 * 		3. KRUMO (Debugging)
 * 
 * PARAMETERS: 
 * 		Required: $output, $svcname
 * 		Optional: seecode, param1...*
 * 
 * EXAMPLE: ?svcname=0&output=krumo
 * ---------------------------------------------------------------------*/

require_once('../classes/krumo/class.krumo.php');
require_once('../includes/config.php');
require_once('../includes/functions.php');

switch ($svcname) {
    case 0: // used to perform an upsert to the session table 
		if($param0 != ''){
			$sql = "IF EXISTS(select [Email] from [sessions] where [Email] = '".$param0."') update [sessions] set [SessionId] = '".$sessionid."', [IPAddress] = '".$_SERVER['REMOTE_ADDR']."', [TimeStamp] = '".date('Y/m/d h:i:s')."' where [Email] = '".$param0."' ELSE insert  [sessions] ([SessionId],[IPAddress], [Email]) values('".$sessionid."', '".$_SERVER['REMOTE_ADDR']."', '".$param0."')";
		}
		$response = false;
		break;
	case 1: // used to get sessionid
		if($param0 != ''){
			$sql = "select [SessionId],[IPAddress],[Email],[TimeStamp] from [sessions] where [IPAddress] = '".$_SERVER['REMOTE_ADDR']."' and [Email] = '".$param0."'";
		}
		break;
	case 2: // used to get sessionid
		if($param0 != ''){
			$sql = "select [SessionId],[IPAddress],[Email],[TimeStamp] from [sessions] where [IPAddress] = '".$_SERVER['REMOTE_ADDR']."' and [SessionId] = '".$param0."'";
		}
		break;
}	

if($sql != ""){
	putenv('TDSVER=70');	
	$outputArr = array();
	$dbConn = mssql_connect($dbHost, $dbUser, $dbPass) or die("Could not connect to MSSQL: ".mssql_get_last_message());
	$selected = mssql_select_db($dbDatabase, $dbConn) or die("Couldn't open database $dbDatabase ".mssql_get_last_message());
	$dbResult = queryDB($sql,$dbConn);
	//enable return of multiple datasets
	if($response){
		$i = 0;
		do {	
			while($line = mssql_fetch_assoc($dbResult)){
				$item = array("item$i"=>$line);
				array_push($outputArr,$item);
			}
			$i++;
		}while (mssql_next_result($dbResult));
		mssql_free_result($dbResult);
		mssql_close($dbConn);
		echo output_result($output, $outputArr);
	}else{
		$blank = array('Result'=>'No return expected');
		echo output_result($output, $blank);
	}
}
?>




