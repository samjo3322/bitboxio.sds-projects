<?php
/* ---------------------------------------------------------------------
 * FILE: admin_webservice.php				DATE:04/20/2010
 * ---------------------------------------------------------------------
 * This script accepts a http request and returns a response as data
 * in the follow formats:
 * 		1. XML
 * 		2. JSON
 * 		3. KRUMO (Debugging)
 * 
 * PARAMETERS: 
 * 		Required: $output, $svcname
 * 		Optional: seecode, param1...*
 * 
 * EXAMPLE: ?svcname=0&output=krumo
 * ---------------------------------------------------------------------*/

require_once('../classes/krumo/class.krumo.php');
require_once('../includes/config.php');
require_once('../includes/functions.php');

// PREDEFINED VARIABLE DETECTED SERVER SIDE
array_push($out, array('EVENT_DATE'=>date('Y/m/d h:i:s A')));
array_push($out, array('SERVER_ADDR'=>$_SERVER['SERVER_ADDR']));
array_push($out, array('SERVER_NAME'=>$_SERVER['SERVER_NAME']));
array_push($out, array('SERVER_SOFTWARE'=>$_SERVER['SERVER_SOFTWARE']));
array_push($out, array('SERVER_PROTOCOL'=>$_SERVER['SERVER_PROTOCOL']));
array_push($out, array('REQUEST_METHOD'=>$_SERVER['REQUEST_METHOD']));
array_push($out, array('HTTP_REFERER'=>$_SERVER['HTTP_REFERER']));
array_push($out, array('HTTP_USER_AGENT'=>$_SERVER['HTTP_USER_AGENT']));
array_push($out, array('REMOTE_ADDR'=>$_SERVER['REMOTE_ADDR']));


// USER/MAP INFORMATION FROM CLIENT
if($_REQUEST['user'] != ''){
	array_push($out, array('USERNAME'=>base64_decode($_REQUEST['user'])));
}else{
	array_push($out, array('USERNAME'=>'ANONYMOUS'));
}

if($_REQUEST['map'] != ''){
	array_push($out, array('MAP'=>base64_decode($_REQUEST['map'])));
}else{
	array_push($out, array('MAP'=>'UNKNOWN'));
}


// CATEGORY/KEYWORD COMBINATION FROM CLIENT
if($_REQUEST['category'] != ''){
	array_push($out, array('CATEGORY'=>base64_decode($_REQUEST['category'])));
}else{
	array_push($out, array('CATEGORY'=>'UNKNOWN'));
}

if($_REQUEST['keyword'] != ''){
	array_push($out, array('KEYWORD'=>base64_decode($_REQUEST['keyword'])));
}else{
	array_push($out, array('KEYWORD'=>'UNKNOWN'));
}

// Debug
//krumo($out);

switch ($svcname) {
    case 0: // used to store tracking information 
		$sql = "insert  [tracking] ([EventDate], [ServerAddress], [ServerName], [ServerSoftware], [ServerProtocol], [RequestMethod], [HTTPReferer], [HTTPUserAgent], [RemoteAddress], [UserName], [Map], [Category], [Keyword])values ('".$out[0]['EVENT_DATE']."', '".$out[1]['SERVER_ADDR']."', '".$out[2]['SERVER_NAME']."', '".$out[3]['SERVER_SOFTWARE']."', '".$out[4]['SERVER_PROTOCOL']."', '".$out[5]['REQUEST_METHOD']."', '".$out[6]['HTTP_REFERER']."', '".$out[7]['HTTP_USER_AGENT']."', '".$out[8]['REMOTE_ADDR']."', '".$out[9]['USERNAME']."', '".$out[10]['MAP']."', '".$out[11]['CATEGORY']."', '".$out[12]['KEYWORD']."')";
		$response = false;
		break;
	case 1: // used to read tracking information (general select)
			$filter = "";
			if($param0 != "" && $param1 != ""){
				$filter = " AND (EventDate >= '".$param0."' AND EventDate <= '".$param1."')";
			}
	
			$sql = "	CREATE TABLE #Visitors(
							UniqueUsers int,
							EventDate date,
							HTTPUserAgent nvarchar(150),
							RemoteAddress nvarchar(50),
							Category nvarchar(150),
							Keyword nvarchar(150)
						)

						INSERT INTO #Visitors(UniqueUsers,EventDate, HTTPUserAgent, RemoteAddress, Category, Keyword)
						SELECT		DISTINCT 
									COUNT(RemoteAddress) as UniqueUsers, 
									DATEADD(dd, 0, DATEDIFF(dd, 0, EventDate)), 
									HTTPUserAgent,
									RemoteAddress,
									Category, 
									Keyword  
									FROM		SDSMap.dbo.tracking 
						WHERE		Category LIKE '%MAP%' ".$filter."
						GROUP BY	EventDate,
									[Count],
									HTTPUserAgent,
									RemoteAddress,
									Category, 
									Keyword
									
						SELECT		SUM(UniqueUsers) AS Users, 
									EventDate AS [Date]
						FROM		#Visitors 
						GROUP BY	EventDate


						SELECT		SUM(UniqueUsers) AS Users,
									Keyword
						FROM		#Visitors 
						WHERE		Keyword LIKE '%County%'
						GROUP BY	Keyword
						
						DROP table #Visitors
			
					";
		break;
	
}	

if($sql != ""){
	putenv('TDSVER=70');	
	$outputArr = array();
	$dbConn = mssql_connect($dbHost, $dbUser, $dbPass) or die("Could not connect to MSSQL: ".mssql_get_last_message());
	$selected = mssql_select_db($dbDatabase, $dbConn) or die("Couldn't open database $dbDatabase ".mssql_get_last_message());
	$dbResult = queryDB($sql,$dbConn);
	//enable return of multiple datasets
	if($response){
		$i = 0;
		do {	
			while($line = mssql_fetch_assoc($dbResult)){
				$item = array("item$i"=>$line);
				array_push($outputArr,$item);
			}
			$i++;
		}while (mssql_next_result($dbResult));
		mssql_free_result($dbResult);
		mssql_close($dbConn);
		echo output_result($output, $outputArr);
	}else{
		$blank = array('Result'=>'No return expected');
		echo output_result($output, $blank);
	}
}
?>




