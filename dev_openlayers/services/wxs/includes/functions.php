<?php

function queryDB($dbQuery,$dbConn) {
	$dbResult = mssql_query("SET ansi_nulls ON") or die('Query failed!'.mssql_get_last_message());
	$dbResult = mssql_query("SET ANSI_WARNINGS ON") or die('Query failed!'.mssql_get_last_message());
	$dbResult = mssql_query($dbQuery, $dbConn) or die('Query failed!'.mssql_get_last_message());
	return $dbResult;
}

function output_result($format, $arr){
	switch($format){
		case xml:
			$result = array_to_xml($arr);
			break;
		case json:
			$result = json_encode($arr);
			break;
		case krumo:
			$result = krumo($arr);
			break;
	}
	return $result;
}

function array_to_xml($array, $level=1) { 
    if ($level==1) {
		$xml = '';
        $xml .= '<?xml version="1.0" encoding="ISO-8859-1"?>'."\n<items>\n";
    }
	
    foreach ($array as $key=>$value) {
        if (is_array($value)) {
            $multi_tags = true;
			 
            foreach($value as $key2=>$value2) {
				
                if (is_array($value2)) {
                    $xml .= str_repeat("\t\t",$level)."<$key2>\n";
                    $xml .= array_to_xml($value2, $level+1);
                    $xml .= str_repeat("\t\t",$level)."</$key2>\n";
                    $multi_tags = true;
                } else {
					
                    if (trim($value2)!='') {
						
                        if (htmlspecialchars($value2)!=$value2) {
                            $xml .= str_repeat("\t\t",$level).
                                    "<$key2><![CDATA[$value2]]>".
                                    "</$key2>\n";
                        } else {
                            $xml .= str_repeat("\t\t",$level).
                                    "<$key2>$value2</$key2>\n";
                        }
						
                    }
					
                    $multi_tags = true;
                }
            }
			
            if (!$multi_tags and count($value)>0) {
                $xml .= str_repeat("\t\t",$level)."<$key>\n";
                $xml .= array_to_xml($value, $level+1);
                $xml .= str_repeat("\t\t",$level)."</$key>\n";
            }
			
        } else {
			
            if (trim($value)!='') {
				
                if (htmlspecialchars($value)!=$value) {
                    $xml .= str_repeat("\t\t",$level)."<$key>".
                            "<![CDATA[$value]]></$key>\n";
                } else {
                    $xml .= str_repeat("\t\t",$level).
                            "<$key>$value</$key>\n";
                }
				
            }
        }
    }
    if ($level==1) {
        $xml .= "</items>\n";
    }
    return $xml;
}
?>
