<?php
$env = 2;	// Change number to change environment.

$dbinfo = array();
$env_local = array('dbhost' => 'CROUCH\SQLEXPRESS', 'dbuser' => 'emapsSvcAcct', 'dbpass' => 'P@$$W0rd', 'dbname' => 'SDSMap', 'dbtype' => 'mssql');
$env_dev = array('dbhost' => 'SAM-DEV\SQLEXPRESS', 'dbuser' => 'emapsSvcAcct', 'dbpass' => 'P@$$W0rd', 'dbname' => 'SDSMap', 'dbtype' => 'mssql');
$env_pub = array('dbhost' => 'EMAPSWEB02\SQLEXPRESS', 'dbuser' => 'emapsSvcAcct', 'dbpass' => 'P@$$W0rd', 'dbname' => 'SDSMap', 'dbtype' => 'mssql');
array_push($dbinfo, $env_local);
array_push($dbinfo, $env_dev);
array_push($dbinfo, $env_pub);
$dbHost = $dbinfo[$env]['dbhost'];				
$dbUser = $dbinfo[$env]['dbuser'];
$dbPass = $dbinfo[$env]['dbpass'];
$dbDatabase = $dbinfo[$env]['dbname'];
$dbType = $dbinfo[$env]['dbtype'];
$svcname = base64_decode($_REQUEST['svcname']);
$output = base64_decode($_REQUEST['output']);
$seecode = base64_decode($_REQUEST['seecode']);
$param0 = base64_decode($_REQUEST['param0']);
$param1 = base64_decode($_REQUEST['param1']);
$param2 = base64_decode($_REQUEST['param2']);
$param3 = base64_decode($_REQUEST['param3']);
$param4 = base64_decode($_REQUEST['param4']);
$param5 = base64_decode($_REQUEST['param5']);
$param6 = base64_decode($_REQUEST['param6']);
$param7 = base64_decode($_REQUEST['param7']);
$param8 = base64_decode($_REQUEST['param8']);
$param9 = base64_decode($_REQUEST['param9']);
$response = true;
$sessionid = uniqid("sds_", more_unique);
$out = array();

// spatial db
$sdbHost = 'localhost';				
$sdbUser = 'postgres';
$sdbPass = 'P@$$W0rd';
$sdbDatabase = 'postgis';
$sdbPort = '5432';

$WMSUrl_alCoosa = "http://172.17.2.62:81/cgi-bin/mapserv.exe?map=C:\ms4w\sds\maps\us_al_coosa.map&";
$usRussellWMSUrl = "http://172.17.2.62:81/cgi-bin/mapserv.exe?map=C:\ms4w\sds\maps\us_al_russell.map&";
$WMSUrl_msAdams = "http://172.17.2.62:81/cgi-bin/mapserv.exe?map=C:\ms4w\sds\maps\us_ms_adams.map&";
$WMSUrl_scFairfield = "http://172.17.2.62:81/cgi-bin/mapserv.exe?map=C:\ms4w\sds\maps\us_sc_fairfield.map&";

// org2org info: used in clip,zip & ship
$ogrcache = "C:/ms4w/Apache/htdocs/cache/";
$ogrpostgis = "host=".$sdbHost." user=".$sdbUser." dbname=".$sdbDatabase." password=".$sdbPass." port=".$sdbPort;

// smtp relay
$smtpRelay = 'emapsweb02.EMAPS.local';
?>
