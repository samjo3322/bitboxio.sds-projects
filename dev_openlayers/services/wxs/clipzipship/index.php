<?php
/* ---------------------------------------------------------------------
 * FILE: index.php				DATE:06/16/2010
 * ---------------------------------------------------------------------
 * PURPOSE: Provide clip, zip & ship capibilities (OGR2OGR)
 * 
 * This script accepts a http request and returns a response as data
 * in the follow formats:
 * 		1. XML
 * 		2. JSON
 * 		3. KRUMO (Debugging)
 * 
 * PARAMETERS: 
 * 		Required: $output, $svcname
 * 		Optional: seecode, param1...*
 * 
 * EXAMPLE: ?svcname=0&output=krumo
 * ---------------------------------------------------------------------*/

require_once('../classes/krumo/class.krumo.php');
require_once('../includes/config.php');
require_once('../includes/functions.php');

$debug = array();
// $param0 format
// $param1 sql
// $param2 email
// $param3 nongraphic
// $param4raster		
$orderid = uniqid("csz_");								// orderid
$orderurl = "";											// order url

//debug
//$format = 'shapefile';
//$sql = 'select gid, the_geom from \"US_AL_Russell_Parcels\" where gid < 20';
//$email = 'scrouch@sds-inc.com';

if($param0 != '' && $param1 != ''){
	
	// 1. output the graphical data using ogr2ogr
	switch($param0){
		case 'KML 2.0/XML':
			$cmd = "ogr2ogr -f \"KML\" ".$ogrcache.$orderid.".kml PG:\"".$ogrpostgis."\" -sql \"".$param1."\"";
			break;
		case 'GML 3.0/XML':
			$cmd = "ogr2ogr -f \"GML\" ".$ogrcache.$orderid.".gml PG:\"".$ogrpostgis."\" -sql \"".$param1."\"";
			break;
		case 'Shape Files':
			$cmd = "ogr2ogr -f \"ESRI Shapefile\" ".$ogrcache.$orderid." PG:\"".$ogrpostgis."\" -sql \"".$param1."\" -nln ".$orderid;
			break;
	}
	//echo $cmd;
	exec($cmd);
	//------------------------------------------------------------------
	
	// 2. output non-graphic data
	if($param3 != ''){
		
	}
	//------------------------------------------------------------------
	
	// 3. output raster data
	if($param4 != ''){
		
	}
	//------------------------------------------------------------------
	
	// 4. zipup all output(s)
	switch($param0){
		case 'KML 2.0/XML':
			$sleep = sleep(1);
			$zip = new ZipArchive();
			$filename = $ogrcache.$orderid.".zip";
	
			if ($zip->open($filename, ZIPARCHIVE::CREATE)!==TRUE) {
				exit("cannot open <$filename>\n");
			}
			
			$zip->addFile($ogrcache.$orderid.".kml");
			$zip->close();
			break;
			
		case 'GML 3.0/XML':
			$sleep = sleep(1);
			$zip = new ZipArchive();
			$filename = $ogrcache.$orderid.".zip";
	
			if ($zip->open($filename, ZIPARCHIVE::CREATE)!==TRUE) {
				exit("cannot open <$filename>\n");
			}
			
			$zip->addFile($ogrcache.$orderid.".gml");
			$zip->addFile($ogrcache.$orderid.".xsd");
			$zip->close();
			break;

		case 'Shape Files':
			$sleep = sleep(5);
			$zip = new ZipArchive();
			$filename = $ogrcache.$orderid.".zip";
	
			if ($zip->open($filename, ZIPARCHIVE::CREATE)!==TRUE) {
				exit("cannot open <$filename>\n");
			}
			$zip->addFile($ogrcache.$orderid."/".$orderid.".dbf");
			$zip->addFile($ogrcache.$orderid."/".$orderid.".shp");
			$zip->addFile($ogrcache.$orderid."/".$orderid.".shx");
			$zip->close();
			break;
	}
	
	//$orderurl = "http://".$_SERVER['SERVER_ADDR'].":81/cache/".$orderid.".zip";
	$orderurl = "http://4.53.252.138:81/cache/".$orderid.".zip";
	
	//------------------------------------------------------------------
	
	// 5. send email
	if($param2 != ''){
		ini_set('SMTP', $smtpRelay );
		ini_set('sendmail_from','clipzipship@sds-inc.com');
		
		// multiple recipients
		$to  = $param2; 
		//$to .= ', '.'wez@example.com'; // note the comma

		// subject
		$subject = 'Your Clip, Zip, and Ship Order '.$orderid;
		
		$tomorrow  = mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"));

		// message
		$message = '
		<html>
		<head>
		  <title>Birthday Reminders for August</title>
		</head>
		<body>
		  <p>Your order will be available for download until '.$tomorrow.'. Please use the following link to download your order. <a href=\"'.$orderurl.'\">'.$orderurl.'</a></p>
		</body>
		</html>
		';

		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// Additional headers
		//$headers = "From: localhost@localhost.com\nReply-To: localhost@localhost.com";
		//$headers .= 'To: <$email>' . "\r\n";
		//$headers .= 'From: <lclipzipship@sds-inc.com>' . "\r\n";
		//$headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
		//$headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";

		// Mail it
		mail($to, $subject, $message, $headers);
	}
	//------------------------------------------------------------------
	
	//##################################################################
	
	$xml .= '<?xml version="1.0" encoding="ISO-8859-1"?>'."\n<items>\n";
	$xml .= "\t<item0>".$orderurl."</item0>\n";
	$xml .= "</items>\n";
	echo $xml;
	
	//echo $orderurl;
	//header('location '.$orderurl);
	
	//##################################################################
	
	
	//debug
	/*
	array_push($debug, array('format'=>$format));
	array_push($debug, array('sql'=>$sql));
	array_push($debug, array('ogrpath'=>$ogrpath));
	array_push($debug, array('ogrcache'=>$ogrcache));
	array_push($debug, array('pgconn'=>$ogrpostgis));
	array_push($debug, array('orderid'=>$orderid));
	array_push($debug, array('orderid'=>$cmd));
	array_push($debug, array('orderurl'=>$orderurl));
	krumo($debug);
	*/
	
}else{
	echo "The 'format' and/or 'filter' was not specified.";
}

?>




