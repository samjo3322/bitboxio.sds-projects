<?php
/* ---------------------------------------------------------------------
 * FILE: storeprod_webservice.php				DATE:4/20/2010
 * ---------------------------------------------------------------------
 * This script accepts a http request and returns a response as data
 * in the follow formats:
 * 		1. XML
 * 		2. JSON
 * 		3. KRUMO (Debugging)
 * 
 * PARAMETERS: 
 * 		Required: $output, $svcname
 * 		Optional: seecode, param1...*
 * 
 * EXAMPLE: ?svcname=0&output=krumo
 * ---------------------------------------------------------------------*/

require_once('../classes/krumo/class.krumo.php');
require_once('../includes/config.php');
require_once('../includes/functions.php');

switch ($svcname) {
    case 0: // used to get a list of states
			$sql = "select [StateId], [StateName] from [states]";
		break;
	case 1: // used to get a list of counties
		if($param0 != ""){
			$sql = "select [states].[StateId], [states].[StateName], [counties].[StateId], [counties].[CountyId], [counties].[CountyName] from [counties], [states] where [states].[StateName] = '".$param0."' and [states].[StateId] = [counties].[StateId]";
		}
		break;
	case 2: //used to get a list of stores
		if($param0 != ""){
			$sql = "select [counties].[CountyName], [counties].[CountyId], [storecountylookup].[CountyId], [storecountylookup].[StoreId], [stores].[StoreId], [stores].[StoreDescription], [storeproductlookup].[StoreId], [storeproductlookup].[ProductId], [products].[ProductId], [products].[ProductName], [items].[ProductId], [items].[ItemDescription], [items].[ItemCode], [items].[ItemLabel], [items].[ItemPrice], [items].[ItemQuantity] from [counties], [storecountylookup], [stores], [storeproductlookup], [products], [items] where [counties].[CountyName] = '".$param0."' and [counties].[CountyId] =  [storecountylookup].[CountyId] and [storecountylookup].[StoreId] = [stores].[StoreId] and [stores].[StoreId] = [storeproductlookup].[StoreId] and [storeproductlookup].[ProductId] = [products].[ProductId] and [products].[ProductId] = [items].[ProductId]";
		}
		break;
}
//echo $svcname;
//echo $sql;
if($sql != ""){
	putenv('TDSVER=70');	
	$outputArr = array();
	$dbConn = mssql_connect($dbHost, $dbUser, $dbPass) or die("Could not connect to MSSQL: ".mssql_get_last_message());
	$selected = mssql_select_db($dbDatabase, $dbConn) or die("Couldn't open database $dbDatabase ".mssql_get_last_message());
	$dbResult = queryDB($sql,$dbConn);
	//enable return of multiple datasets
	if($response){
		$i = 0;
		do {	
			while($line = mssql_fetch_assoc($dbResult)){
				$item = array("item$i"=>$line);
				array_push($outputArr,$item);
			}
			$i++;
		}while (mssql_next_result($dbResult));
		mssql_free_result($dbResult);
		mssql_close($dbConn);
		echo output_result($output, $outputArr);
	}else{
		$blank = array('Result'=>'No return expected');
		echo output_result($output, $blank);
	}
}
?>
