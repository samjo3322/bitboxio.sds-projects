<?php
/* ---------------------------------------------------------------------
 * FILE: storeprod_webservice.php				DATE:4/20/2010
 * ---------------------------------------------------------------------
 * This script accepts a http request and returns a response as data
 * in the follow formats:
 * 		1. XML
 * 		2. JSON
 * 		3. KRUMO (Debugging)
 * 
 * PARAMETERS: 
 * 		Required: $output, $svcname
 * 		Optional: seecode, param1...*
 * 
 * EXAMPLE: ?svcname=0&output=krumo
 * ---------------------------------------------------------------------*/

require_once('../classes/krumo/class.krumo.php');
require_once('../includes/config.php');
require_once('../includes/functions.php');

switch ($svcname) {
    case 0: // used to check for user session cookie
		$xml .= '<?xml version="1.0" encoding="ISO-8859-1"?>'."\n<items><session>";
		$xml .= $_COOKIE['EmapsPlus_Session'];
		$xml .= "</session></items>";
		echo $xml;
		break;
	case 1: // used to set user session cookie
		if($param0 != ''){
			setcookie('EmapsPlus_Session',$param0,time() + (86400 * 90)); 
			//echo $param0;
		}
		break;
	case 2: // used to set user session cookie
			setcookie('EmapsPlus_Session', ''); 
		break;
}
?>
