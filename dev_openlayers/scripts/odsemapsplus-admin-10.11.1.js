if(GUI.emapsplus.status() == 'READY'){
	GUI.emapsplus.admin = {

		siteaddurl 		: 	"http://emaps.emapsplus.com/v10.9.1/svcs/SiteSettings.asmx/addSite",
		sitedeleteurl	:	"http://emaps.emapsplus.com/v10.9.1/svcs/SiteSettings.asmx/deleteSite",
		siteupdateurl	:	"http://emaps.emapsplus.com/v10.9.1/svcs/SiteSettings.asmx/editSite",
		sitelisturl 	: 	"http://emaps.emapsplus.com/v10.9.1/svcs/SiteSettings.asmx/fetchSiteList",
		siteinfourl 	: 	"http://emaps.emapsplus.com/v10.9.1/svcs/SiteSettings.asmx/fetchSiteFormData",
		
		init : function(){ 
			GUI.core.debugmsg('emapsplus.admin is starting'); 
			GUI.emapsplus.admin.buildViewStack();
			GUI.emapsplus.admin.buildSetupVS();
			GUI.emapsplus.admin.buildArcGISVS();
			GUI.emapsplus.admin.buildWebAPIVS();
			GUI.emapsplus.admin.buildGenCode();
			GUI.emapsplus.admin.buildPreview();
		},
		
		buildViewStack : function(){
			GUI.core.debugmsg('building admin sub-tabs'); 
			var div = document.getElementById("emapsplus-admin-com");
			
			var vsctl_attr = new Array();
			vsctl_attr[0] = ["GUI.ui.viewstackClickHandler('emapsadmin-viewstack-', 4, 0);", "Setup", "emapsadmin-viewstack-0"];
			vsctl_attr[1] = ["GUI.ui.viewstackClickHandler('emapsadmin-viewstack-', 4, 1);", "ArcGIS", "emapsadmin-viewstack-1"];
			vsctl_attr[2] = ["GUI.ui.viewstackClickHandler('emapsadmin-viewstack-', 4, 2);", "Web API", "emapsadmin-viewstack-2"];
			vsctl_attr[3] = ["GUI.ui.viewstackClickHandler('emapsadmin-viewstack-', 4, 3);", "Code v10.9.1", "emapsadmin-viewstack-3"];
			vsctl_attr[4] = ["GUI.ui.viewstackClickHandler('emapsadmin-viewstack-', 4, 4);", "Preview v10.9.1", "emapsadmin-viewstack-4"];
			var vsctl = GUI.ui.viewstackControlCom(vsctl_attr, 1235);
			div.appendChild(vsctl);
		},
		
		buildSetupVS : function(obj){
			GUI.core.debugmsg('building viewstack - setup'); 
			var div = document.getElementById('emapsadmin-viewstack-0');
			
			var vswrap_attr = new Array();
			vswrap_attr[0] = ["style","width:100%;height:99%;float:none;text-align:center;"];
			vswrap_attr[1] = ["id","emapsplus-admin-vs"];
			var vswrap = GUI.core.crossXBrowserNode('div', vswrap_attr, div);
			
			// create left column
			var vsleft_attr = new Array();
			vsleft_attr[0] = ["style","width:400px;height:100%;float:left;padding:10px;text-align:left;"];
			vsleft_attr[1] = ["id","emapsplus-admin-vsleft"];
			var vsleft = GUI.core.crossXBrowserNode('div', vsleft_attr, vswrap);
			
			var sitebar_attr = new Array();
			sitebar_attr[0] = ["id","emapsplus-admin-sitebar"];
			var sitebar = GUI.core.crossXBrowserNode('div', sitebar_attr, vsleft);
			
			var combo_box_attr = new Array();
			combo_box_attr[0] = ["id","form-sitelist"];
			combo_box_attr[1] = ["onchange","GUI.emapsplus.admin.handleSiteChange();"];
			var combo_box = GUI.core.crossXBrowserNode('select', combo_box_attr, sitebar);
			
			GUI.emapsplus.admin.clearNode(combo_box);
			
			var defaultopt_attr = new Array();
			var defaultopt = GUI.core.crossXBrowserNode('option', defaultopt_attr, combo_box);
			defaultopt.value = -1;
			defaultopt.appendChild(document.createTextNode('Select A eMaps Site...'));
			
			GUI.core.httpRequest(GUI.emapsplus.admin.sitelisturl, GUI.emapsplus.admin.fetchSiteList);
			GUI.emapsplus.admin.buildFormButtons();
			GUI.emapsplus.admin.buildFormFields();
			
			
			// create right column
			var vsright_attr = new Array();
			vsright_attr[0] = ["style","width:330px;height:100%;float:right;padding:10px;padding-top:50px;text-align:left;"];
			vsright_attr[1] = ["id", "emapsplus-admin-vsright"];
			var vsright = GUI.core.crossXBrowserNode('div', vsright_attr, vswrap);
			
			// add accordian(s)
			var acc_arr = new Array();
			acc_arr[0] = ['Required Settings', 'Content'];
			acc_arr[1] = ['GIS Extent and SRID', 'Content'];
			acc_arr[2] = ['Basemap/Ortho-Imagery', 'Content'];
			acc_arr[3] = ['Title, Subtitle and Link', 'Content'];
			acc_arr[4] = ['External Links', 'Content'];
			acc_arr[5] = ['Simple, Advanced and Active Search ', 'Content'];
			acc_arr[6] = ['Reports, Mailing Labels, Mini-Card', 'Content'];
			
			var acc = GUI.ui.stdAccordion('AccordionContainer', 308, acc_arr);
			vsright.appendChild(acc);
		},
		
		buildArcGISVS : function(){
			GUI.core.debugmsg('building viewstack - arcgis'); 
			var div = document.getElementById('emapsadmin-viewstack-1');
			
			var msg_attr = new Array();
			msg_attr[0] = ["id","iframe-arcgis"];
			msg_attr[1] = ["style","font-size:16pt;font-style:italic;color:#666666;padding-top:5px;"];
			var msg = GUI.core.crossXBrowserNode('div', msg_attr, div);
			msg.innerHTML = "Please select a eMaps site under the 'Setup' tab.";
			
			var iframe_attr = new Array();
			iframe_attr[0] = ["id","mapapi"];
			iframe_attr[1] = ["width","100%"];
			iframe_attr[2] = ["height","1200"];
			iframe_attr[3] = ["src","about:blank"];
			var iframe = GUI.core.crossXBrowserNode('iframe', iframe_attr, div);
		},
		
		buildWebAPIVS : function(){
			GUI.core.debugmsg('building viewstack - webapi'); 
			var div = document.getElementById('emapsadmin-viewstack-2');
			
			var iframe_attr = new Array();
			iframe_attr[0] = ["id","webapi"];
			iframe_attr[1] = ["width","100%"];
			iframe_attr[2] = ["height","1200"];
			iframe_attr[3] = ["style","overflow-x:hidden;"];
			iframe_attr[4] = ["src","http://emaps.emapsplus.com/v10.9.1/svcs/IdentifyFeatures.asmx"];
			var iframe = GUI.core.crossXBrowserNode('iframe', iframe_attr, div);
		},
		
		buildGenCode : function(){
			GUI.core.debugmsg('building viewstack - gen code'); 
			var div = document.getElementById('emapsadmin-viewstack-3');
			
			var msg_attr = new Array();
			msg_attr[0] = ["id","iframe-gencode"];
			msg_attr[1] = ["style","font-size:16pt;font-style:italic;color:#666666;padding-top:5px;"];
			var msg = GUI.core.crossXBrowserNode('div', msg_attr, div);
			msg.innerHTML = "This feature is under construction.";
			
			var codebox = GUI.ui.stdTxtArea('gencode', '', 'width:100%;height:1200px;');
			div.appendChild(codebox);
		},
		
		buildPreview : function(){
			GUI.core.debugmsg('building viewstack - sneak peek'); 
			var div = document.getElementById('emapsadmin-viewstack-4');
			
			var msg_attr = new Array();
			msg_attr[0] = ["id","iframe-sneakpeek"];
			msg_attr[1] = ["style","font-size:16pt;font-style:italic;color:#666666;padding-top:5px;"];
			var msg = GUI.core.crossXBrowserNode('div', msg_attr, div);
			msg.innerHTML = "This feature is under construction.";
			
			/*
			var iframe_attr = new Array();
			iframe_attr[0] = ["id","sneakpeek"];
			iframe_attr[1] = ["width","100%"];
			iframe_attr[2] = ["height","1200"];
			iframe_attr[3] = ["src","about:blank"];
			var iframe = GUI.core.crossXBrowserNode('iframe', iframe_attr, div);
			*/
		},
		
		buildFormButtons : function(){
			var display = document.getElementById('emapsplus-admin-sitebar');
			var btn_delete_attr = new Array();
			btn_delete_attr[0] = ["id", "form-btn-delete"];
			btn_delete_attr[1] = ["type", "submit"];
			btn_delete_attr[2] = ["value", "Remove"];
			btn_delete_attr[3] = ["onclick", "GUI.emapsplus.admin.handleDeleteSite();"];
			var btn_delete = GUI.core.crossXBrowserNode('input', btn_delete_attr, display);
			btn_delete.disabled = true;
			
			var btn_update_attr = new Array();
			btn_update_attr[0] = ["id", "form-btn-update"];
			btn_update_attr[1] = ["type", "submit"];
			btn_update_attr[2] = ["value", "Update"];
			btn_update_attr[3] = ["onclick", "GUI.emapsplus.admin.handleUpdateSite();"];
			var btn_update = GUI.core.crossXBrowserNode('input', btn_update_attr, display);
			btn_update.disabled = true;
		
			var btn_add_attr = new Array();
			btn_add_attr[0] = ["id", "form-btn-add"];
			btn_add_attr[1] = ["type", "submit"];
			btn_add_attr[2] = ["value", "Add New"];
			btn_add_attr[3] = ["onclick", "GUI.emapsplus.admin.handleAddSite();"];
			var btn_add = GUI.core.crossXBrowserNode('input', btn_add_attr, display);
			btn_add.disabled = false;
		},
		
		buildFormFields : function(){
			var display = document.getElementById('emapsplus-admin-vsleft');
			
			GUI.core.debugmsg("building required fields!");
			var divRequired_attr = new Array();
			divRequired_attr[0] = ["style", "width:100%;padding:5px;background-color:#ffffff;padding-bottom:25px;"];
			var divRequired = GUI.core.crossXBrowserNode('div', divRequired_attr, display);
			
			divRequired.innerHTML = "Required Fields:";
			divRequired.appendChild(GUI.ui.stdTxtField("form-txt-code", "SITE CODE: ", 390));
			divRequired.appendChild(document.createElement('br'));
			divRequired.appendChild(GUI.ui.stdTxtField("form-txt-toolbar", "TOOLBAR: ", 390));
			divRequired.appendChild(document.createElement('br'));
			divRequired.appendChild(GUI.ui.stdTxtField("form-txt-xmin", "XMIN: ", 390));
			divRequired.appendChild(document.createElement('br'));
			divRequired.appendChild(GUI.ui.stdTxtField("form-txt-ymin", "YMIN: ", 390));
			divRequired.appendChild(document.createElement('br'));
			divRequired.appendChild(GUI.ui.stdTxtField("form-txt-xmax", "XMAX: ", 390));
			divRequired.appendChild(document.createElement('br'));
			divRequired.appendChild(GUI.ui.stdTxtField("form-txt-ymax", "YMAX: ", 390));
			divRequired.appendChild(document.createElement('br'));
			divRequired.appendChild(GUI.ui.stdTxtField("form-txt-srid", "SRID: ", 390));
			divRequired.appendChild(document.createElement('br'));
			divRequired.appendChild(GUI.ui.stdTxtField("form-txt-mapbaseurl", "BASEMAP URL: ", 390));
			
			GUI.core.debugmsg("building optional fields!");
			var divOptional_attr = new Array();
			divOptional_attr[0] = ["style", "width:100%;padding:5px;background-color:#ffffff;"];
			var divOptional = GUI.core.crossXBrowserNode('div', divOptional_attr, display);
			
			divOptional.innerHTML = "Optional Fields:";
			divOptional.appendChild(GUI.ui.stdTxtField("form-txt-title", "TITLE: ", 390));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdTxtField("form-txt-subtitle", "SUBTITLE: ", 390));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdTxtField("form-txt-link", "LINK: ", 390));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdTxtField("form-txt-ortho", "ORTHO URL: ", 390));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdTxtArea("form-txt-activelayers", "ACTIVE LAYERS: ", "width:390px;height:75px;"));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdTxtArea("form-txt-layergroups", "LAYER GROUPS: ", "width:390px;height:75px;"));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdTxtField("form-txt-extlinks", "EXTERNAL LINKS: ", 390));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdTxtField("form-txt-extcol", "EXTRA COLUMN: ", 390));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdTxtArea("form-txt-simsearch", "SIMPLE SEARCH: ", "width:390px;height:75px;"));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdRadioBool("form-txt-themactive", "THEMATICS ACTIVE: "));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdTxtField("form-txt-themfix", "THEMATICS FIXED: ", 390));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdTxtField("form-txt-themrng", "THEMATICS RANGE: ", 390));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdRadioBool("form-txt-disactive", "DISCLAIMER ACTIVE: "));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdTxtArea("form-txt-dismsg", "DISCLAIMER TEXT: ", "width:390px;height:75px;"));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdRadioBool("form-txt-docactive", "DOCMGR ACTIVE: "));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdTxtField("form-txt-docurl", "DOCMGR URL: ", 390));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdTxtField("form-txt-helpurl", "HELP URL: ", 390));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdTxtField("form-txt-reporturl", "REPORT URL: ", 390));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdTxtField("form-txt-datamarturl", "DATAMART URL: ", 390));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdTxtArea("form-txt-minicardconf", "MINI-CARD CONF: ", "width:390px;height:75px;"));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdTxtField("form-txt-minicardurl", "MINI-CARD URL: ", 390));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdRadioBool("form-txt-mailactive", "MAIL-LABEL ACTIVE: "));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdTxtField("form-txt-mailurl", "MAIL-LABEL URL: ", 390));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdRadioBool("form-txt-advqueryactive", "ADV-QUERY ACTIVE: "));
			divOptional.appendChild(document.createElement('br'));
			divOptional.appendChild(GUI.ui.stdTxtArea("form-txt-advqueryconf", "ADV-QUERY CONF: ", "width:390px;height:75px;"));
		},
		
		clearNode : function(node){
			while (node.hasChildNodes()) {node.removeChild(node.lastChild);}
		},
		
		fetchSiteList : function(xml){
			var combo_box = document.getElementById('form-sitelist');
			var x = xml.documentElement.getElementsByTagName("Table");
			
			for (i=0;i<x.length;i++){
				var data = x[i].getElementsByTagName("siteid")[0].firstChild.nodeValue;
				var label = x[i].getElementsByTagName("code")[0].firstChild.nodeValue;
				var choice_attr = new Array();
				var choice = GUI.core.crossXBrowserNode('option', choice_attr, combo_box);
				
				choice.value = data;
				choice.appendChild(document.createTextNode(label));
			}
		},
		
		fetchSiteSettings : function(xml){
			GUI.core.debugmsg("populating form...");
			var x = xml.documentElement.getElementsByTagName("Table");
			
			var code; try{ code = x[0].getElementsByTagName("code")[0].firstChild.nodeValue; } catch(err) { code = ''; }
			var toolbar; try{ toolbar = x[0].getElementsByTagName("toolbar")[0].firstChild.nodeValue; } catch(err) { toolbar = ''; }
			var extxmin; try{ extxmin = x[0].getElementsByTagName("extxmin")[0].firstChild.nodeValue; } catch(err) { extxmin = ''; }
			var extymin; try{ extymin = x[0].getElementsByTagName("extymin")[0].firstChild.nodeValue; } catch(err) { extymin = ''; }
			var extxmax; try{ extxmax = x[0].getElementsByTagName("extxmax")[0].firstChild.nodeValue; } catch(err) { extxmax = ''; }
			var extymax; try{ extymax = x[0].getElementsByTagName("extymax")[0].firstChild.nodeValue; } catch(err) { extymax = ''; }
			var srid; try{ srid = x[0].getElementsByTagName("srid")[0].firstChild.nodeValue; } catch(err) { srid = ''; }
			var mapbaseurl; try{ mapbaseurl = x[0].getElementsByTagName("mapbaseurl")[0].firstChild.nodeValue; } catch(err) { mapbaseurl = ''; }
			var title; try{ title = x[0].getElementsByTagName("title")[0].firstChild.nodeValue; } catch(err) { title = ''; }
			var subtitle; try{ subtitle = x[0].getElementsByTagName("subtitle")[0].firstChild.nodeValue; } catch(err) { subtitle = ''; }
			var link; try{ link = x[0].getElementsByTagName("link")[0].firstChild.nodeValue; } catch(err) { link = ''; }
			var mapimgurl; try{ mapimgurl = x[0].getElementsByTagName("mapimgurl")[0].firstChild.nodeValue; } catch(err) { mapimgurl = ''; }
			var activelayersel; try{ activelayersel = x[0].getElementsByTagName("activelayersel")[0].firstChild.nodeValue; } catch(err) { activelayersel = ''; }
			var layergroups; try{ layergroups = x[0].getElementsByTagName("layergroups")[0].firstChild.nodeValue; } catch(err) { layergroups = ''; }
			var extlinks; try{ extlinks = x[0].getElementsByTagName("extlinks")[0].firstChild.nodeValue; } catch(err) { extlinks = ''; }
			var extracolumn; try{ extracolumn = x[0].getElementsByTagName("extracolumn")[0].firstChild.nodeValue; } catch(err) { extracolumn = ''; }
			var searchsimple; try{ searchsimple = x[0].getElementsByTagName("searchsimple")[0].firstChild.nodeValue; } catch(err) { searchsimple = ''; }
			var thematicactive; try{ thematicactive = x[0].getElementsByTagName("thematicactive")[0].firstChild.nodeValue; } catch(err) { thematicactive = ''; }
			var thematicfixed; try{ thematicfixed = x[0].getElementsByTagName("thematicfixed")[0].firstChild.nodeValue; } catch(err) { thematicfixed = ''; }
			var thematicrange; try{ thematicrange = x[0].getElementsByTagName("thematicrange")[0].firstChild.nodeValue; } catch(err) { thematicrange = ''; }
			var disclaimeractive; try{ disclaimeractive = x[0].getElementsByTagName("disclaimeractive")[0].firstChild.nodeValue; } catch(err) { disclaimeractive = ''; }
			var disclaimermsg; try{ disclaimermsg = x[0].getElementsByTagName("disclaimermsg")[0].firstChild.nodeValue; } catch(err) { disclaimermsg = ''; }
			var advancequeryactive; try{ advancequeryactive = x[0].getElementsByTagName("advancequeryactive")[0].firstChild.nodeValue; } catch(err) { advancequeryactive = ''; }
			var docactive; try{ docactive = x[0].getElementsByTagName("docactive")[0].firstChild.nodeValue; } catch(err) { docactive = ''; }
			var docurl; try{ docurl = x[0].getElementsByTagName("docurl")[0].firstChild.nodeValue; } catch(err) { docurl = ''; }
			var helpurl; try{ helpurl = x[0].getElementsByTagName("helpurl")[0].firstChild.nodeValue; } catch(err) { helpurl = ''; }
			var reporturl; try{ reporturl = x[0].getElementsByTagName("reporturl")[0].firstChild.nodeValue; } catch(err) { reporturl = ''; }
			var datamarturl; try{ datamarturl = x[0].getElementsByTagName("datamarturl")[0].firstChild.nodeValue; } catch(err) { datamarturl = ''; }
			var minipropcardconf; try{ minipropcardconf = x[0].getElementsByTagName("minipropcardconf")[0].firstChild.nodeValue; } catch(err) { minipropcardconf = ''; }
			var minipropcardurl; try{ minipropcardurl = x[0].getElementsByTagName("minipropcardurl")[0].firstChild.nodeValue; } catch(err) { minipropcardurl = ''; }
			var maillabelactive; try{ maillabelactive = x[0].getElementsByTagName("maillabelactive")[0].firstChild.nodeValue; } catch(err) { maillabelactive = ''; }
			var maillabelurl; try{ maillabelurl = x[0].getElementsByTagName("maillabelurl")[0].firstChild.nodeValue; } catch(err) { maillabelurl = ''; }
			var advancequeryactive; try{ advancequeryactive = x[0].getElementsByTagName("advancequeryactive")[0].firstChild.nodeValue; } catch(err) { advancequeryactive = ''; }
			var advancequeryconf; try{ advancequeryconf = x[0].getElementsByTagName("advancequeryconf")[0].firstChild.nodeValue; } catch(err) { advancequeryconf = ''; }
			
			document.getElementById('form-txt-code').value = code;
			document.getElementById('form-txt-toolbar').value = toolbar;
			document.getElementById('form-txt-xmin').value = extxmin;
			document.getElementById('form-txt-ymin').value = extymin;
			document.getElementById('form-txt-xmax').value = extxmax;
			document.getElementById('form-txt-ymax').value = extymax;
			document.getElementById('form-txt-srid').value = srid;
			document.getElementById('form-txt-mapbaseurl').value = mapbaseurl;
			document.getElementById('form-txt-title').value = title;
			document.getElementById('form-txt-subtitle').value = subtitle;
			document.getElementById('form-txt-link').value = link;
			document.getElementById('form-txt-ortho').value = mapimgurl;
			document.getElementById('form-txt-activelayers').value = activelayersel;
			document.getElementById('form-txt-layergroups').value = layergroups;
			document.getElementById('form-txt-extlinks').value = extlinks;
			document.getElementById('form-txt-extcol').value = extracolumn;
			document.getElementById('form-txt-simsearch').value = searchsimple;
			if(thematicactive == "false" || thematicactive == "no" || thematicactive == ""){
				GUI.emapsplus.admin.setCheckedValue(document.getElementsByName('form-txt-themactive'), 0);
			}else{
				GUI.emapsplus.admin.setCheckedValue(document.getElementsByName('form-txt-themactive'), 1);
			}
			document.getElementById('form-txt-themfix').value = thematicfixed;
			document.getElementById('form-txt-themrng').value = thematicrange;
			if(disclaimeractive == "false" || disclaimeractive == "no" || disclaimeractive == ""){
				GUI.emapsplus.admin.setCheckedValue(document.getElementsByName('form-txt-disactive'), 0);
			}else{
				GUI.emapsplus.admin.setCheckedValue(document.getElementsByName('form-txt-disactive'), 1);
			}
			document.getElementById('form-txt-dismsg').value = disclaimermsg;
			
			if(advancequeryactive == "false" || advancequeryactive == "no" || advancequeryactive == ""){
				GUI.emapsplus.admin.setCheckedValue(document.getElementsByName('form-txt-advqueryactive'), 0);
			}else{
				GUI.emapsplus.admin.setCheckedValue(document.getElementsByName('form-txt-advqueryactive'), 1);
			}
			if(docactive == "false" || docactive == "no" || docactive == ""){
				GUI.emapsplus.admin.setCheckedValue(document.getElementsByName('form-txt-docactive'), 0);
			}else{
				GUI.emapsplus.admin.setCheckedValue(document.getElementsByName('form-txt-docactive'), 1);
			}
			document.getElementById('form-txt-docurl').value = docurl;
			document.getElementById('form-txt-helpurl').value = helpurl;
			document.getElementById('form-txt-reporturl').value = reporturl;
			document.getElementById('form-txt-datamarturl').value = datamarturl;
			document.getElementById('form-txt-minicardconf').value = minipropcardconf;
			document.getElementById('form-txt-minicardurl').value = minipropcardurl;
			if(maillabelactive == "false" || maillabelactive == "no" || maillabelactive == ""){
				GUI.emapsplus.admin.setCheckedValue(document.getElementsByName('form-txt-mailactive'), 0);
			}else{
				GUI.emapsplus.admin.setCheckedValue(document.getElementsByName('form-txt-mailactive'), 1);
			}
			document.getElementById('form-txt-mailurl').value = maillabelurl;
			if(advancequeryactive == "false" || advancequeryactive == "no" || advancequeryactive == ""){
				GUI.emapsplus.admin.setCheckedValue(document.getElementsByName('form-txt-advqueryactive'), 0);
			}else{
				GUI.emapsplus.admin.setCheckedValue(document.getElementsByName('form-txt-advqueryactive'), 1);
			}
			document.getElementById('form-txt-advqueryconf').value = advancequeryconf;
			
			// update iframe sources
			GUI.core.debugmsg("updating sub tabs");
			document.getElementById('mapapi').src = mapbaseurl;
			//document.getElementById('sneakpeek').src = "http://emaps.emapsplus.com/v10.9.1/kiosk/temp.html?sitecode=" + code;

			//var gencode = "<!DOCTYPE html>\n<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\">\n\t<head>\n\t\t<title>eMaps</title>\n\t\t<link rel=\"stylesheet\" href=\"./common/style.css\" />\n\t\t<script type=\"text/javascript\" src=\"./common/emaps.js\"></script>\n\t</head>\n\t<body onload=\"setTimeout(function() { emaps.init('" + code + "'); }, 500)\">\n\t</body>\n</html>";
			//document.getElementById('gencode').value = gencode;		
		},
		
		selectedSite : function(){
			var _selsite = document.getElementById('form-sitelist').value;
			GUI.emapsplus.admin.toggleFormButtons(_selsite);
			return _selsite;
		},
		
		toggleFormButtons : function(sel){
			GUI.core.debugmsg('selected: ' + sel);
			if(sel != -1){
				GUI.core.debugmsg("enable buttons");
				document.getElementById('form-btn-delete').disabled = false;
				document.getElementById('form-btn-update').disabled = false;
				
				GUI.core.debugmsg("updating tabs!");
				//GUI.emapsplus.admin.toggleTabMsg(document.getElementById('iframe-sneakpeek'), false);
				//GUI.emapsplus.admin.toggleTabMsg(document.getElementById('iframe-gencode'), false);
				GUI.emapsplus.admin.toggleTabMsg(document.getElementById('iframe-arcgis'), false);
			}else{
				document.getElementById('form-btn-delete').disabled = true;
				document.getElementById('form-btn-update').disabled = true;
				GUI.core.debugmsg("disabling buttons...");
				GUI.core.debugmsg("clearing form...");
				document.getElementById('form-txt-code').value = "";
				document.getElementById('form-txt-toolbar').value = "";
				document.getElementById('form-txt-xmin').value = "";
				document.getElementById('form-txt-ymin').value ="";
				document.getElementById('form-txt-xmax').value = "";
				document.getElementById('form-txt-ymax').value = "";
				document.getElementById('form-txt-srid').value = "";
				document.getElementById('form-txt-mapbaseurl').value = "";
				document.getElementById('form-txt-title').value = "";
				document.getElementById('form-txt-subtitle').value = "";
				document.getElementById('form-txt-link').value = "";
				document.getElementById('form-txt-ortho').value = "";
				document.getElementById('form-txt-activelayers').value = "";
				document.getElementById('form-txt-layergroups').value = "";
				document.getElementById('form-txt-extlinks').value = "";
				document.getElementById('form-txt-extcol').value = "";
				document.getElementById('form-txt-simsearch').value = "";
				document.getElementsByName('form-txt-themactive')[1].checked;
				document.getElementById('form-txt-themfix').value = "";
				document.getElementById('form-txt-themrng').value = "";
				document.getElementsByName('form-txt-disactive')[1].checked;
				document.getElementById('form-txt-dismsg').value = "";
				document.getElementsByName('form-txt-docactive')[1].checked;
				document.getElementById('form-txt-docurl').value = "";
				document.getElementById('form-txt-helpurl').value = "";
				document.getElementById('form-txt-reporturl').value = "";
				document.getElementById('form-txt-datamarturl').value = "";
				document.getElementById('form-txt-minicardconf').value = "";
				document.getElementById('form-txt-minicardurl').value = "";
				document.getElementsByName('form-txt-mailactive')[1].checked;
				document.getElementById('form-txt-mailurl').value = "";
				document.getElementsByName('form-txt-advqueryactive')[1].checked;
				document.getElementById('form-txt-advqueryconf').value = "";
				
				//GUI.core.debugmsg("updating tabs!");
				//GUI.emapsplus.admin.toggleTabMsg(document.getElementById('iframe-sneakpeek'), true);
				//GUI.emapsplus.admin.toggleTabMsg(document.getElementById('iframe-gencode'), true);
				GUI.emapsplus.admin.toggleTabMsg(document.getElementById('iframe-arcgis'), true);
				
				//GUI.core.debugmsg("clearing iframe source(s)");
				//document.getElementById('sneakpeek').src = "about:blank";  
				//document.getElementById('mapapi').src = "about:blank"; 
				
				//GUI.core.debugmsg("clearing code generation");
				//document.getElementById('gencode').value = "";
			}
		},
		
		toggleTabMsg : function(node, toggle){
			if(toggle){
				node.style.visibility = "visible";
			}else{
				node.style.visibility = "hidden";
			}
		},
		
		setCheckedValue : function (radioObj, newValue) {
			if(!radioObj)
				return;
			var radioLength = radioObj.length;
			if(radioLength == undefined) {
				radioObj.checked = (radioObj.value == newValue.toString());
				return;
			}
			for(var i = 0; i < radioLength; i++) {
				radioObj[i].checked = false;
				if(radioObj[i].value == newValue.toString()) {
					radioObj[i].checked = true;
				}
			}
		},
		
		getCheckedValue : function (radioObj) {
			if(!radioObj){ return ""; }
			
			var radioLength = radioObj.length;
			
			if(radioLength == undefined){
				if(radioObj.checked){
					return radioObj.value;
				}else{
					return "";
				}
			}
			
			for(var i = 0; i < radioLength; i++) {
				if(radioObj[i].checked) {
					return radioObj[i].value;
				}
			}
			
			return "";
		},
		
		paramsFromForm : function(){
			var params = "";
			params += "siteid=" + GUI.emapsplus.admin.selectedSite();
			params += "&code=" + encodeURI(document.getElementById('form-txt-code').value);
			params += "&toolbar=" + encodeURI(document.getElementById('form-txt-toolbar').value);
			params += "&title=" + encodeURI(document.getElementById('form-txt-title').value);
			params += "&subtitle=" + encodeURI(document.getElementById('form-txt-subtitle').value);
			params += "&link=" + encodeURI(document.getElementById('form-txt-link').value);
			params += "&extxmin=" + encodeURI(document.getElementById('form-txt-xmin').value);
			params += "&extymin=" + encodeURI(document.getElementById('form-txt-ymin').value);
			params += "&extxmax=" + encodeURI(document.getElementById('form-txt-xmax').value);
			params += "&extymax=" + encodeURI(document.getElementById('form-txt-ymax').value);
			params += "&srid=" + encodeURI(document.getElementById('form-txt-srid').value);
			params += "&mapbaseurl=" + encodeURI(document.getElementById('form-txt-mapbaseurl').value);
			params += "&mapimgurl=" + encodeURI(document.getElementById('form-txt-ortho').value);
			params += "&activelayersel=" + encodeURI(document.getElementById('form-txt-activelayers').value);
			params += "&layergroups=" + encodeURI(document.getElementById('form-txt-layergroups').value);
			params += "&extlinks=" + encodeURI(document.getElementById('form-txt-extlinks').value);
			params += "&extracolumn=" + encodeURI(document.getElementById('form-txt-extcol').value);
			params += "&searchsimple=" + encodeURI(document.getElementById('form-txt-simsearch').value);
			if(GUI.emapsplus.admin.getCheckedValue(document.getElementsByName('form-txt-themactive')) == 1){
				params += "&thematicactive=true";
			}else{
				params += "&thematicactive=false";
			}
			params += "&thematicfixed=" + encodeURI(document.getElementById('form-txt-themfix').value);
			params += "&thematicrange=" + encodeURI(document.getElementById('form-txt-themrng').value);
			if(GUI.emapsplus.admin.getCheckedValue(document.getElementsByName('form-txt-disactive')) == 1){
				params += "&disclaimeractive=yes";
			}else{
				params += "&disclaimeractive=no";
			}
			params += "&disclaimermsg=" + encodeURI(document.getElementById('form-txt-dismsg').value);
			if(GUI.emapsplus.admin.getCheckedValue(document.getElementsByName('form-txt-docactive')) == 1){
				params += "&docactive=true";
			}else{
				params += "&docactive=false";
			}
			params += "&docurl=" + encodeURI(document.getElementById('form-txt-docurl').value);
			params += "&helpurl=" + encodeURI(document.getElementById('form-txt-helpurl').value);
			params += "&reporturl=" + encodeURI(document.getElementById('form-txt-reporturl').value);
			params += "&datamarturl=" + encodeURI(document.getElementById('form-txt-datamarturl').value);
			params += "&minipropcardconf=" + encodeURI(document.getElementById('form-txt-minicardconf').value);
			params += "&minipropcardurl=" + encodeURI(document.getElementById('form-txt-minicardurl').value);
			if(GUI.emapsplus.admin.getCheckedValue(document.getElementsByName('form-txt-mailactive')) == 1){
				params += "&maillabelactive=true";
			}else{
				params += "&maillabelactive=false";
			}
			params += "&maillabelurl=" + encodeURI(document.getElementById('form-txt-mailurl').value);
			if(GUI.emapsplus.admin.getCheckedValue(document.getElementsByName('form-txt-advqueryactive')) == 1){
				params += "&advancequeryactive=true";
			}else{
				params += "&advancequeryactive=false";
			}
			params += "&advancequeryconf=" + encodeURI(document.getElementById('form-txt-advqueryconf').value);
			
			return params;
		},
		
		handleSiteChange : function(){
			var _selsite = GUI.emapsplus.admin.selectedSite();
			console.log(_selsite);
			if(_selsite != -1){
				GUI.core.httpRequest(GUI.emapsplus.admin.siteinfourl + "?siteid=" + _selsite, GUI.emapsplus.admin.fetchSiteSettings);
			}
		},
		
		handleDeleteSite : function(){
			GUI.core.debugmsg('deleting site...');
			GUI.core.httpPostData(GUI.emapsplus.admin.sitedeleteurl, "siteid=" + GUI.emapsplus.admin.selectedSite(), GUI.emapsplus.admin.handleHttpPostResponse);
			
			if(GUI.core.getQuerystring('sdstoken') != ""){
				window.location = "emapsplus.html?sdstoken=" + GUI.core.getQuerystring('sdstoken');
			}else{
				window.location = "emapsplus.html";
			}
		},
		
		handleUpdateSite : function(){
			GUI.core.debugmsg('updating site...');
			var params = GUI.emapsplus.admin.paramsFromForm();
			GUI.core.httpPostData(GUI.emapsplus.admin.siteupdateurl, params, GUI.emapsplus.admin.handleHttpPostResponse);
		},
		
		handleAddSite : function(){
			if(document.getElementById('form-txt-code').value != '' && document.getElementById('form-txt-toolbar').value != "" && document.getElementById('form-txt-xmin').value != "" && document.getElementById('form-txt-ymin').value != "" & document.getElementById('form-txt-xmax').value != "" && document.getElementById('form-txt-ymax').value != "" && document.getElementById('form-txt-srid').value != "" && document.getElementById('form-txt-mapbaseurl').value != ""){
				GUI.core.debugmsg('adding site...');
				var params = GUI.emapsplus.admin.paramsFromForm();
				GUI.core.httpPostData(GUI.emapsplus.admin.siteaddurl, params, GUI.emapsplus.admin.handleHttpPostResponse);
				
				if(GUI.core.getQuerystring('sdstoken') != ""){
					window.location = "emapsplus.html?sdstoken=" + GUI.core.getQuerystring('sdstoken');
				}else{
					window.location = "emapsplus.html";
				}		
				
			}else{
				alert('All field in the grey box are required.');
			}
		},
		
		handleHttpPostResponse : function(txt){
			GUI.core.debugmsg(txt);
			GUI.core.httpRequest(GUI.emapsplus.admin.sitelisturl, GUI.emapsplus.admin.buildSiteList);
		},
	};
	
	GUI.emapsplus.admin.init();
	
}else{
	console.log('GUI.emapsplus status failied!');
}
