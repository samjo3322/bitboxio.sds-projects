if(GUI.docman.status() == 'READY'){
	GUI.docman.checkinout = {

		init : function(){ 
			GUI.core.debugmsg('docman.checkinout is starting'); 
			GUI.docman.checkinout.buildFormPage();
		},
		
		buildFormPage : function(){
			var display = document.getElementById("docman-checkinout-com");
			
			// make search buttons
			var formBox1_attr = new Array();
			formBox1_attr[0] = ["style", "width:100%;text-align:right;"];
			formBox1_attr[1] = ["id", "formbox-checkinout-1"];
			var formBox1 = GUI.core.crossXBrowserNode("div", formBox1_attr, display);
			
			var searchBtn = GUI.ui.stdBtn('checkinout-btn-search','Search', 'handleCheckInOutSearch');
			var clearBtn = GUI.ui.stdBtn('checkinout-btn-clear','Clear', 'handleCheckInOutSearch');
			
			formBox1.appendChild(searchBtn);
			formBox1.appendChild(clearBtn);
			
			
			// make listboxes
			var formBox2_attr = new Array();
			formBox2_attr[0] = ["style", "text-align:left;display:inline;padding:5px;margin:3px;"];
			formBox2_attr[1] = ["id", "formbox-checkinout-2"];
			var formBox2 = GUI.core.crossXBrowserNode("div", formBox2_attr, display);
			
			var listBtnGroup1 = GUI.ui.stdListBoxBtns('handler1();','handler2();', 150);
			var listBtnGroup2 = GUI.ui.stdListBoxBtns('handler1();','handler2();', 150);
			
			var availableListBox_opt = new Array();
			availableListBox_opt[0] = ["a", "item #1 ..."];
			availableListBox_opt[1] = ["b", "item #2"];
			availableListBox_opt[2] = ["c", "item #3"];
			var availableListBox = GUI.ui.stdListBox('coorespondence-listbox-availalbe', 'Available Fields', availableListBox_opt, 8);
			var selectedListBox_opt = new Array();
			selectedListBox_opt[0] = ["a", "item #1 ..."];
			selectedListBox_opt[1] = ["b", "item #2"];
			selectedListBox_opt[2] = ["c", "item #3"];
			var selectedListBox = GUI.ui.stdListBox('coorespondence-listbox-availalbe', 'Selected Fields', selectedListBox_opt, 8);
			var sortbyListBox_opt = new Array();
			sortbyListBox_opt[0] = ["a", "item #1 ..."];
			sortbyListBox_opt[1] = ["b", "item #2"];
			sortbyListBox_opt[2] = ["c", "item #3"];
			var sortbyListBox = GUI.ui.stdListBox('coorespondence-listbox-availalbe', 'Sort By Fields', sortbyListBox_opt, 8);
			
			formBox2.appendChild(availableListBox);
			formBox2.appendChild(listBtnGroup1);
			formBox2.appendChild(selectedListBox);
			formBox2.appendChild(listBtnGroup2);
			formBox2.appendChild(sortbyListBox);
			
			
			// make search form
			var formBox3_attr = new Array();
			formBox3_attr[0] = ["style", "width:100%;overflow:auto;text-align:center;margin:3px;"];
			formBox3_attr[1] = ["id", "formbox-checkinout-3"];
			var formBox3 = GUI.core.crossXBrowserNode("div", formBox3_attr, display);
			
			var fieldTxtField = GUI.ui.stdTxtField('coorespondence-txt-field', 'FIELD', 260);
			
			var qualiferComboBox_opt = new Array();
			qualiferComboBox_opt[0] = ["a", "option #1"];
			qualiferComboBox_opt[1] = ["b", "option #2"];
			qualiferComboBox_opt[2] = ["c", "option #3"];
			var qualiferComboBox = GUI.ui.stdCombo('coorespondence-combo-datasrc', 'Select Qualifer...', qualiferComboBox_opt);
			
			var valueTxtField = GUI.ui.stdTxtField('coorespondence-txt-value', 'VALUE', 125);
			
			var addBtn = GUI.ui.stdBtn('coorespondence-btn-add','Add', 'alert(\'process btn\');');
			
			formBox3.appendChild(fieldTxtField);
			formBox3.appendChild(qualiferComboBox);
			formBox3.appendChild(valueTxtField);
			formBox3.appendChild(addBtn);
			
			// make results grid
			var formBox4_attr = new Array();
			formBox4_attr[0] = ["style", "float:none;text-align:left;margin:3px;"];
			formBox4_attr[1] = ["id", "formbox-checkinout-4"];
			var formBox4 = GUI.core.crossXBrowserNode("div", formBox4_attr, display);
			
			var checkinoutgrid_head = new Array();
			checkinoutgrid_head[0] = ["file"];
			checkinoutgrid_head[1] = ["Title"];
			checkinoutgrid_head[2] = ["Type"];
			checkinoutgrid_head[3] = ["Date"];
			checkinoutgrid_head[4] = ["Keyword"];
			checkinoutgrid_head[5] = ["Selected"];
			checkinoutgrid_head[6] = ["Actions"];
			
			var checkinoutgrid_data = new Array();
			checkinoutgrid_data[0] = ['Application Incomplete Letter.docx', '', 'DMR', '12/02/2011', '', '<input type="checkbox"/>', '<div class="popmod" onclick="TINY.box.show({html:TINY.modal1,animate:true,close:true,boxid:\'success\',top:5})">Modal Btn'];
			checkinoutgrid_data[1] = ['Application Incomplete Letter.docx', '', 'DMR', '12/02/2011', '', '<input type="checkbox"/>', '<div class="popmod" onclick="TINY.box.show({html:TINY.modal1,animate:true,close:true,boxid:\'success\',top:5})">Modal Btn'];
			checkinoutgrid_data[2] = ['Application Incomplete Letter.docx', '', 'DMR', '12/02/2011', '', '<input type="checkbox"/>', '<div class="popmod" onclick="TINY.box.show({html:TINY.modal1,animate:true,close:true,boxid:\'success\',top:5})">Modal Btn'];

			var checkinoutgrid = GUI.ui.stdGrid('table', checkinoutgrid_head, checkinoutgrid_data);
			
			formBox4.appendChild(checkinoutgrid);
		}
		
	};
	
	GUI.docman.checkinout.init();
	
}else{
	console.log('GUI.docman status failied!');
}







