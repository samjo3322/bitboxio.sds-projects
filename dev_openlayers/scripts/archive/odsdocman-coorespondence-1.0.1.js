if(GUI.docman.status() == 'READY'){
	GUI.docman.coorespondence = {

		init : function(){ 
			GUI.core.debugmsg('docman.coorespondence is starting'); 
			GUI.docman.coorespondence.buildViewStack();
			GUI.docman.coorespondence.buildContactForm();
			GUI.docman.coorespondence.buildQueryInfoForm();
			GUI.docman.coorespondence.buildTempInfoSearchForm();
			GUI.docman.coorespondence.buildLetterForm();
		},
		
		buildViewStack : function(){
			var display = document.getElementById("docman-coorespondence-com");
			
			// make viewstack control bar
			var vsctl_attr = new Array();
			vsctl_attr[0] = ["GUI.ui.viewstackClickHandler('coorespondence-viewstack-', 3, 0);", "Contact Info", "coorespondence-viewstack-0"];
			vsctl_attr[1] = ["GUI.ui.viewstackClickHandler('coorespondence-viewstack-', 3, 1);", "Query Info", "coorespondence-viewstack-1"];
			vsctl_attr[2] = ["GUI.ui.viewstackClickHandler('coorespondence-viewstack-', 3, 2);", "Template Info", "coorespondence-viewstack-2"];
			vsctl_attr[3] = ["GUI.ui.viewstackClickHandler('coorespondence-viewstack-', 3, 3);", "Letter Info", "coorespondence-viewstack-3"];
			var vsctl = GUI.ui.viewstackControlCom(vsctl_attr);
			display.appendChild(vsctl);
		},
		
		buildContactForm : function(){
			var display = document.getElementById("coorespondence-viewstack-0");
			display.innerHTML = 'still trying to figure this section out';
		},
		
		buildQueryInfoForm : function(){
			var display = document.getElementById("coorespondence-viewstack-1");
			
			// make container
			var formBar1_attr = new Array();
			formBar1_attr[0] = ["style", "text-align:left;padding:10px;margin:3px;"];
			var formBar1 = GUI.core.crossXBrowserNode("div", formBar1_attr, display);
			var formBar2_attr = new Array();
			formBar2_attr[0] = ["style", "text-align:left;display:inline;padding:10px;margin:3px;"];
			var formBar2 = GUI.core.crossXBrowserNode("div", formBar2_attr, display);
			var formBar3_attr = new Array();
			formBar3_attr[0] = ["style", "text-align:center;float:left;padding:10px;margin:3px;"];
			var formBar3 = GUI.core.crossXBrowserNode("div", formBar3_attr, display);
			
			// make dropdowns
			var dataComboBox_opt = new Array();
			dataComboBox_opt[0] = ["a", "option #1"];
			dataComboBox_opt[1] = ["b", "option #2"];
			dataComboBox_opt[2] = ["c", "option #3"];
			var dataComboBox = GUI.ui.stdCombo('coorespondence-combo-datasrc', 'Select A Data Source...', dataComboBox_opt);
			var qualiferComboBox_opt = new Array();
			qualiferComboBox_opt[0] = ["a", "option #1"];
			qualiferComboBox_opt[1] = ["b", "option #2"];
			qualiferComboBox_opt[2] = ["c", "option #3"];
			var qualiferComboBox = GUI.ui.stdCombo('coorespondence-combo-datasrc', 'Select Qualifer...', qualiferComboBox_opt);
			
			// make buttons
			var clearBtn = GUI.ui.stdBtn('coorespondence-btn-clearqueryinfo','Clear', 'alert(\'process btn\');');
			var addBtn = GUI.ui.stdBtn('coorespondence-btn-add','Add', 'alert(\'process btn\');');
			var runBtn = GUI.ui.stdBtn('coorespondence-btn-run','Run', 'alert(\'process btn\');');
			var listBtnGroup1 = GUI.ui.stdListBoxBtns('handler1();','handler2();', 150);
			var listBtnGroup2 = GUI.ui.stdListBoxBtns('handler1();','handler2();', 150);
			
			// make textfields
			var fieldTxtField = GUI.ui.stdTxtField('coorespondence-txt-field', 'FIELD', 260);
			var valueTxtField = GUI.ui.stdTxtField('coorespondence-txt-value', 'VALUE', 125);
			
			// make listboxes
			var availableListBox_opt = new Array();
			availableListBox_opt[0] = ["a", "item #1 ..."];
			availableListBox_opt[1] = ["b", "item #2"];
			availableListBox_opt[2] = ["c", "item #3"];
			var availableListBox = GUI.ui.stdListBox('coorespondence-listbox-availalbe', 'Available Fields', availableListBox_opt, 8);
			var selectedListBox_opt = new Array();
			selectedListBox_opt[0] = ["a", "item #1 ..."];
			selectedListBox_opt[1] = ["b", "item #2"];
			selectedListBox_opt[2] = ["c", "item #3"];
			var selectedListBox = GUI.ui.stdListBox('coorespondence-listbox-availalbe', 'Selected Fields', selectedListBox_opt, 8);
			var sortbyListBox_opt = new Array();
			sortbyListBox_opt[0] = ["a", "item #1 ..."];
			sortbyListBox_opt[1] = ["b", "item #2"];
			sortbyListBox_opt[2] = ["c", "item #3"];
			var sortbyListBox = GUI.ui.stdListBox('coorespondence-listbox-availalbe', 'Sort By Fields', sortbyListBox_opt, 8);
			
			// add to layout
			formBar1.appendChild(dataComboBox);
			formBar1.appendChild(clearBtn);
			
			formBar2.appendChild(availableListBox);
			formBar2.appendChild(listBtnGroup1);
			formBar2.appendChild(selectedListBox);
			formBar2.appendChild(listBtnGroup2);
			formBar2.appendChild(sortbyListBox);
			
			formBar3.appendChild(fieldTxtField);
			formBar3.appendChild(qualiferComboBox);
			formBar3.appendChild(valueTxtField);
			formBar3.appendChild(addBtn);
			formBar3.appendChild(runBtn);
		},
		
		buildTempInfoSearchForm : function(){
			
			var display = document.getElementById("coorespondence-viewstack-2");
			
			// make container
			var formBar_attr = new Array();
			formBar_attr[0] = ["style", "text-align:center;padding:30px;margin:3px;"];
			var formBar = GUI.core.crossXBrowserNode("div", formBar_attr, display);
			
			// make textfields
			var titleTxtField = GUI.ui.stdTxtField('coorespondence-txt-title', 'TITLE', 260);
			var keywordTxtField = GUI.ui.stdTxtField('coorespondence-txt-keyword', 'KEYWORD', 125);
			
			// make buttons
			var searchBtn = GUI.ui.stdBtn('coorespondence-btn-search','Search', 'handleSearch');
			var clearBtn = GUI.ui.stdBtn('coorespondence-btn-clear','Clear', 'handleClear');
			
			// add to layout
			formBar.appendChild(titleTxtField);
			formBar.appendChild(keywordTxtField);
			formBar.appendChild(searchBtn);
			formBar.appendChild(clearBtn);
			
		},
		
		buildLetterForm : function(){
			var display = document.getElementById("coorespondence-viewstack-3");
			
			var editor = GUI.ui.editorControl('correspondence-content');
			
			display.appendChild(editor);
		}
		
	};
	
	GUI.docman.coorespondence.init();
	
}else{
	console.log('GUI.docman status failied!');
}







