if(GUI.emapsplus.status() == 'READY'){
	
	GUI.emapsplus.map = {
		myMap: null,
		isMaxScreen : false,
		currLocation : null,
		watchId : null,
		locGraphic : null,
		myMap : null,
		navToolbar : null,
		navToolZoom : null,
		navToolPan : null,
		drawToolbar : null,
		measureToolbar: null,
		srid : "EPSG:4326",
		fromProjection : new OpenLayers.Projection("EPSG:4326"),   // Transform from WGS 1984
        toProjection   : new OpenLayers.Projection("EPSG:900913"), // to Spherical Mercator Projection
        position : new OpenLayers.LonLat(-84.508063,43.452708).transform( GUI.emapsplus.map.fromProjection, GUI.emapsplus.map.toProjection),
        zoom : 10, 
		geoLocStyle : { fillColor: '#000', fillOpacity: 0.1, strokeWidth: 0 },
		vectorLayer : new OpenLayers.Layer.Vector('vector'),
		osmLayer : new OpenLayers.Layer.OSM({opacity:0.5}),
		baseLayer : new OpenLayers.Layer.WMS("Basemap", "http://localhost:7422/index.cgi?map=.\\maps\\al_madison_parcel_shape.map&", {isBaseLayer:true, opacity: 0.5, layers: 'parcel'} ),
		maxExtent : new OpenLayers.Bounds(-84.508063,43.452708,-80.892807,45.258969),
		simpleSearchParam : "Parcel|PARCELID|Ex. 000-00000:Owner|OWNER|Ex. John Smith",
		layerGroups : "Parcels|true|baseLayer:Weather*|true|weatherLayer",

		init : function(){ 
			GUI.core.debugmsg('emapsplus.map is starting'); 
			GUI.emapsplus.map.buildToolbar();
			GUI.emapsplus.map.buildMap();
		},
		
		buildToolbar : function(){
			GUI.core.debugmsg('building map toolbar...'); 
			var div = document.getElementById("emapsplus-map-com");
			
			var model_attr = new Array();
			model_attr[0] = ["id", "menuModel"];
			model_attr[1] = ["style", "display:none;"];
			var model = GUI.core.crossXBrowserNode('div', model_attr, div);
			
			var menu_attr = new Array();
			menu_attr[0] = ["id", "menuDiv"];
			var menu = GUI.core.crossXBrowserNode('div', menu_attr, div);
			
			var menuModel = new DHTMLSuite.menuModel();
			menuModel.addItem(100,'&nbsp;','','',false,'','');
			menuModel.addItem(1,'Help','','',false,'Help','');
			menuModel.setSubMenuWidth(1,130);
			menuModel.addItem(1001,'Web','','http://emaps.emapsplus.com/help/',1,'Open Help Page','');
			menuModel.addItem(1002,'Email','','mailto:emapsplus@sds-inc.com?subject=Help request from DREAMaps&body=Please enter your help request and we will respond as soon as we are able.%0A%0a%0A%0ARegards,%0AeMaps Support Team',1,'Send Email','');
			menuModel.addSeparator();
			
			menuModel.addItem(15,'Bookmarks','','',false,'Bookmarks','');
			menuModel.setSubMenuWidth(15,130);
			menuModel.addItem(1501,'Original Extent','','',15,'return to original extent','GUI.emapsplus.map.myMap.zoomToMaxExtent();');
			menuModel.addItem(1502,'My Bookmarks','','',15,'open local bookmarks','alert("todo:)');
			menuModel.addSeparator();
			
			menuModel.addItem(14,'Settings','','',false,'Help','');
			
			menuModel.addItem(1401,'Bookmark','','',14,'capture map reference','');
			menuModel.addItem(1402,'Max/Min Screen','','',14,'toggle screen size','GUI.emapsplus.map.toolToggleMapScreen();');
			menuModel.addItem(1403,'Clear Cache','','',14,'clear map tiles','OpenLayers.Control.CacheWrite.clearCache();');
			menuModel.addItem(1404,'Layers','','',14,'Layer Manager','GUI.emapsplus.map.toggleToolBox("toolbox-layers", true);');
			menuModel.addSeparator();
			
			menuModel.addItem(16,'Toolbar','','',false,'Toolbar','');
			menuModel.setSubMenuWidth(16,130);
			menuModel.addItem(1601,'Geo Locate','','',16,'find your location','GUI.emapsplus.map.geoLocate();');
			menuModel.addItem(1602,'Print','','',16,'Print','window.print();');
			menuModel.addItem(1603,'Measure','','',16,'Measure Tool','GUI.emapsplus.map.toggleToolBox("toolbox-measure", true);');
			menuModel.addItem(1604,'Search','','',16,'Search Map','GUI.emapsplus.map.toggleToolBox("toolbox-search", true);');
			menuModel.addItem(1605,'Select','','',16,'Select Tool','GUI.emapsplus.map.toggleToolBox("toolbox-select", true);');
			
			menuModel.addItemsFromMarkup('menuModel');
			menuModel.setMainMenuGroupWidth(00);	
			menuModel.init();
			
			DHTMLSuite.configObj.setCssPath('styles/');
			
			var menuBar = new DHTMLSuite.menuBar();
			menuBar.addMenuItems(menuModel);
			menuBar.setMenuItemLayoutCss('style.css');// name for the menu items
			menuBar.setLayoutCss('style.css');
			menuBar.setMenuItemCssPrefix('Custom_');
			menuBar.setCssPrefix('Custom_');
			menuBar.setTarget('menuDiv');
			menuBar.init();
			
			GUI.emapsplus.map.toolboxSelect();
			GUI.emapsplus.map.toolboxSearch();
			GUI.emapsplus.map.toolboxMeasure();
			GUI.emapsplus.map.toolboxLayers();
			
		},
		
		toolboxSelect : function(){
			GUI.core.debugmsg('building select toolbox...'); 
			
			// create content area first
			var toolSelDiv;
			try
			{
				toolSelDiv = document.createElement('<div id="toolbox-select-dialogbox">');
			}catch(err){
				toolSelDiv = document.createElement('div');
				toolSelDiv.setAttribute("id", "toolbox-select-dialogbox");
			}
			
			var code = '';
			code +=	'<input type="radio" name="typeSel" value="none" id="noneToggle" onclick="GUI.emapsplus.map.toggleSelControl(this);GUI.emapsplus.map.vectorLayer.removeAllFeatures();" checked="checked" /><label for="noneToggle">Deactivate</label><p>';
			code +=	'<input type="radio" name="typeSel" value="point" id="pointToggle" onclick="GUI.emapsplus.map.toggleSelControl(this);GUI.emapsplus.map.vectorLayer.removeAllFeatures();" /><label for="pointToggle">By Point</label><p>';
			code +=	' <input type="radio" name="typeSel" value="line" id="lineToggle" onclick="GUI.emapsplus.map.toggleSelControl(this);GUI.emapsplus.map.vectorLayer.removeAllFeatures();" /><label for="lineToggle">By Line</label><p>';
			code +=	' <input type="radio" name="typeSel" value="polygon" id="polygonToggle" onclick="GUI.emapsplus.map.toggleSelControl(this);GUI.emapsplus.map.vectorLayer.removeAllFeatures();" /><label for="polygonToggle">By Polygon</label><p>';
			code +=	'<input type="radio" name="typeSel" value="box" id="boxToggle" onclick="GUI.emapsplus.map.toggleSelControl(this);GUI.emapsplus.map.vectorLayer.removeAllFeatures();" /><label for="boxToggle">By Box</label>';
			
			toolSelDiv.innerHTML = code;
			
			var toolSel = GUI.ui.floatDiaBox("toolbox-select", "Select", toolSelDiv, "GUI.emapsplus.map.toggleToolBox('toolbox-select', false);", "width:135px;height:150px;top:-430px;left:100px;");
			document.body.appendChild(toolSel);
			Drag.init(document.getElementById("toolbox-select"));
		},
		
		toggleSelControl : function(element) {
             for(key in drawControls) {
				var control = drawControls[key];
				if(element.value == key && element.checked) {
					control.activate();
				} else {
					control.deactivate();
				}
			}
        },
		
		toolboxSearch : function(){
			GUI.core.debugmsg('building search toolbox...'); 
			
			var toolSearchDiv;
			try
			{
				toolSearchDiv = document.createElement('<div id="toolbox-search-dialogbox">');
			}catch(err){
				toolSearchDiv = document.createElement('div');
				toolSearchDiv.setAttribute("id", "toolbox-search-dialogbox");
			}
			
			var code = '<select id="toolbox-search-type">';
			var typeParam = GUI.emapsplus.map.simpleSearchParam;
			var types = typeParam.split(':');
			for(var i = 0; i < types.length; i++){
				var props = types[i].split('|');
				code += '<option value="' + props[1] + '">' + props[0] + '</option>';
			} 
			
			code +=	'</select>';
			code +=	'&nbsp;';
			code +=	'<input id="toolbox-search-keyword" type="text" size="20" onfocus="this.value=null;">';
			code +=	'&nbsp;';
			code +=	'<input id="toolbox-search-submit" type="submit" value="Go" onclick="alert(\'search\');"><br>';
			code +=	'<div id="toolbox-search-example" style="font-size:9px;font-style:italic;padding:3px;color:#999999;"></div>';
			
			toolSearchDiv.innerHTML = code;
			
			var toolSearch = GUI.ui.floatDiaBox("toolbox-search", "Search", toolSearchDiv, "GUI.emapsplus.map.toggleToolBox('toolbox-search', false);", "width:265px;height:90px;top:-650px;left:250px;");
			document.body.appendChild(toolSearch);
			Drag.init(document.getElementById("toolbox-search"));
		},
		
		changeSearchExample : function(){
			var selType = document.getE
		},
		
		toolboxMeasure : function(){
			GUI.core.debugmsg('building map measure toolbox...'); 
			var toolMeasureDiv;
			try
			{
				toolMeasureDiv = document.createElement('<div id="toolbox-measure-dialogbox">');
			}catch(err){
				toolMeasureDiv = document.createElement('div');
				toolMeasureDiv.setAttribute("id", "toolbox-measure-dialogbox");
			}
			
			var code = '';
			code +=	'<input type="radio" name="typeMeasure" value="none" id="noneToggle"onclick="GUI.emapsplus.map.toggleMeasureControl(this);GUI.emapsplus.map.vectorLayer.removeAllFeatures();" checked="checked" /><label for="noneToggle">Deactivate</label><p>';
			code +=	'<input type="radio" name="typeMeasure" value="line" id="lineToggle" onclick="GUI.emapsplus.map.toggleMeasureControl(this);GUI.emapsplus.map.vectorLayer.removeAllFeatures();" /><label for="lineToggle">By Distance</label><p>';
			code +=	' <input type="radio" name="typeMeasure" value="polygon" id="polygonToggle" onclick="GUI.emapsplus.map.toggleMeasureControl(this);GUI.emapsplus.map.vectorLayer.removeAllFeatures();" /><label for="polygonToggle">By Area</label><p>&nbsp;<p>';
			code +=	'<div id="output"></div>';
			
			toolMeasureDiv.innerHTML = code;

			var toolMeasure= GUI.ui.floatDiaBox("toolbox-measure", "Measure", toolMeasureDiv, "GUI.emapsplus.map.toggleToolBox('toolbox-measure', false);GUI.emapsplus.map.measureToolbar.clearResult();", "width:165px;height:125px;top:-625px;left:100px;");
			document.body.appendChild(toolMeasure);
			Drag.init(document.getElementById("toolbox-measure"));
		},
		
		toggleMeasureControl : function(element) {
            for(key in measureControls) {
                var control = measureControls[key];
                if(element.value == key && element.checked) {
                    control.activate();
                } else {
                    control.deactivate();
                }
            }
        },
        
        handleMeasurements : function(event) {
            var geometry = event.geometry;
            var units = event.units;
            var order = event.order;
            var measure = event.measure;
            var element = document.getElementById('output');
            var out = "";
            if(order == 1) {
                out += "measure: " + measure.toFixed(3) + " " + units;
            } else {
                out += "measure: " + measure.toFixed(3) + " " + units + "<sup>2</" + "sup>";
            }
            element.innerHTML = out;
        },
		
		toolboxLayers : function(){
			GUI.core.debugmsg('building layer toolbox...'); 
			
			// create content area first
			var div;
			try
			{
				div = document.createElement('<div id="toolbox-layers-dialogbox">');
			}catch(err){
				div = document.createElement('div');
				div.setAttribute("id", "toolbox-layers-dialogbox");
			}

			var code = '';
			code +=	'<b>Imagery:</b>&nbsp;&nbsp;&nbsp;';
			code +=	'<input type="range" value="0.3" min="0" max="1" step="0.01" id="toolbox-slider-imgalpha" onchange="GUI.emapsplus.map.tiledLayer.setOpacity(this.value);GUI.emapsplus.map.referenceLayer.setOpacity(this.value);" onmousedown="Drag.lock=true" onmouseup="Drag.lock=false;" /><br>';
			code +=	'<b>Basemap:</b>&nbsp;&nbsp;&nbsp;';
			code +=	'<input type="range" value="1.3" min="0" max="1" step="0.01" id="toolbox-slider-basealpha" onchange="GUI.emapsplus.map.baseLayer.opacity=this.value;GUI.emapsplus.map.baseLayer.redraw();" onmousedown="Drag.lock=true" onmouseup="Drag.lock=false;" /><br>';
			code +=	'<br><hr width="90%"><br>';
			code +=	'<b>Layer Groups:</b>&nbsp;&nbsp;&nbsp;<br><br>';

			var layergroups = GUI.emapsplus.map.layerGroups;

			if(layergroups != ""){
				var groups = layergroups.split(':');
				for(var i = 0; i < groups.length; i++){
					var props = groups[i].split('|');
					var check = '';
					if(props[1] == 'true'){
						check = 'checked';
					}
					code += '&nbsp;&nbsp;&nbsp;<input type="checkbox" name="emapsplus-checkbox-layergroups" onclick="GUI.emapsplus.map.toggleLayer(GUI.emapsplus.map.' +  props[2] + ');" value="' + props[2]  + '" ' + check + ' onmousedown="Drag.lock=true" onmouseup="Drag.lock=false;">&nbsp;' + props[0] + '<br>';
				}
			}
			
			code +=	'&nbsp;<br><br>';
			code +=	'*Remote connection required.<br>';
			code +=	'**Creative-Commons Attribution.<br>';
			code +=	'***API key required.<br><br>';
			
			div.innerHTML = code;

			var toolLayers = GUI.ui.floatDiaBox("toolbox-layers", "Layer Manager", div, "GUI.emapsplus.map.toggleToolBox('toolbox-layers', false);", "width:230px;height:325px;top:-775px;left:150px;");
			document.body.appendChild(toolLayers);
			Drag.init(document.getElementById("toolbox-layers"));
		},
		
		toggleImagery : function(){
			
		},
		
		changeVisibleLayerGroups : function(){
			
		},
		
		buildMap : function(){
			GUI.core.debugmsg('loading map configuration...'); 
			var div = document.getElementById("emapsplus-map-com");
			
			var map_attr = new Array();
			map_attr[0] = ["id", "map"];
			map_attr[1] = ["style", "width:100%;height:395px;"];
			var map = GUI.core.crossXBrowserNode('div', map_attr, div);
			
			GUI.emapsplus.map.initMap();
		},
		
		 initMap : function() {
			 
			 GUI.core.debugmsg('initializing map...'); 
			 
			 GUI.emapsplus.map.myMap = new OpenLayers.Map('map',{
                allOverlays: true,
				projection: GUI.emapsplus.map.srid,
				maxExtent: GUI.emapsplus.map.maxExtent,
				
				controls: [
                        //new OpenLayers.Control.Navigation(),
                        //new OpenLayers.Control.PanZoomBar(),
                        new OpenLayers.Control.ScaleLine(),
                        new OpenLayers.Control.MousePosition(),
                        new OpenLayers.Control.KeyboardDefaults()
                        ],
                    numZoomLevels: 10
			 }),
             
             
			 GUI.emapsplus.map.myMap.addLayer(GUI.emapsplus.map.baseLayer);
			 GUI.emapsplus.map.myMap.addLayer(GUI.emapsplus.map.osmLayer);
             GUI.emapsplus.map.myMap.addLayer(GUI.emapsplus.map.vectorLayer);
             
             GUI.emapsplus.map.myMap.zoomToMaxExtent();
            
			 OpenLayers.Control.CustomNavToolbar = OpenLayers.Class(OpenLayers.Control.Panel, {
				initialize: function(options) {
					OpenLayers.Control.Panel.prototype.initialize.apply(this, [options]);
					this.addControls([
					  new OpenLayers.Control.Navigation(),
					  new OpenLayers.Control.ZoomBox({alwaysZoom:false}),
					]);

					this.displayClass = 'olControlNavToolbar'
				},
				
				draw: function() {
					var div = OpenLayers.Control.Panel.prototype.draw.apply(this, arguments);
					this.defaultControl = this.controls[0];
					return div;
				}
			});
            
            
            // ------------ this is for the cache feature ---------------
            cacheWrite = new OpenLayers.Control.CacheWrite({
				autoActivate: true,
				imageFormat: "image/png",
				eventListeners: {
					cachefull: function() { status.innerHTML = "Cache full."; }
				}
			});
			
			GUI.emapsplus.map.myMap.addControl(cacheWrite);
			// ---------------------------------------------------------
			
			
			// ------------ this is for the geolocate tool --------------- 
			var pulsate = function(feature) {
				var point = feature.geometry.getCentroid(),
					bounds = feature.geometry.getBounds(),
					radius = Math.abs((bounds.right - bounds.left)/2),
					count = 0,
					grow = 'up';

				var resize = function(){
					if (count>16) {
						clearInterval(window.resizeInterval);
					}
					var interval = radius * 0.03;
					var ratio = interval/radius;
					switch(count) {
						case 4:
						case 12:
							grow = 'down'; break;
						case 8:
							grow = 'up'; break;
					}
					if (grow!=='up') {
						ratio = - Math.abs(ratio);
					}
					feature.geometry.resize(1+ratio, point);
					GUI.emapsplus.map.vectorLayer.drawFeature(feature);
					count++;
				};
				window.resizeInterval = window.setInterval(resize, 50, point, radius);
			};
			
			var geolocate = new OpenLayers.Control.Geolocate({
				bind: false,
				geolocationOptions: {
					enableHighAccuracy: false,
					maximumAge: 0,
					timeout: 7000
				}
			});
			
			GUI.emapsplus.map.myMap.addControl(geolocate);
			var firstGeolocation = true;
			geolocate.events.register("locationupdated",geolocate,function(e) {
				GUI.emapsplus.map.vectorLayer.removeAllFeatures();
				var circle = new OpenLayers.Feature.Vector(
					OpenLayers.Geometry.Polygon.createRegularPolygon(
						new OpenLayers.Geometry.Point(e.point.x, e.point.y),
						e.position.coords.accuracy/2,
						40,
						0
					),
					{},
					GUI.emapsplus.map.geoLocStyle
				);
				GUI.emapsplus.map.vectorLayer.addFeatures([
					new OpenLayers.Feature.Vector(
						e.point,
						{},
						{
							graphicName: 'cross',
							strokeColor: '#f00',
							strokeWidth: 2,
							fillOpacity: 0,
							pointRadius: 10
						}
					),
					circle
				]);
				if (firstGeolocation) {
					GUI.emapsplus.map.myMap.zoomToExtent(GUI.emapsplus.map.vectorLayer.getDataExtent());
					pulsate(circle);
					firstGeolocation = false;
					this.bind = true;
				}
			});
			
			geolocate.events.register("locationfailed",this,function() {
				OpenLayers.Console.log('Location detection failed');
			});
			
			document.getElementById('menuItemText1404').onclick = function() {
				GUI.emapsplus.map.vectorLayer.removeAllFeatures();
				geolocate.deactivate();
				geolocate.watch = false;
				firstGeolocation = true;
				geolocate.activate();
			};
			// ---------------------------------------------------------
			
			
			// ------------ this is for the select tool ----------------
			
			 drawControls = {
                    point: new OpenLayers.Control.DrawFeature(GUI.emapsplus.map.vectorLayer,
                        OpenLayers.Handler.Point),
                    line: new OpenLayers.Control.DrawFeature(GUI.emapsplus.map.vectorLayer,
                        OpenLayers.Handler.Path),
                    polygon: new OpenLayers.Control.DrawFeature(GUI.emapsplus.map.vectorLayer,
                        OpenLayers.Handler.Polygon),
                    box: new OpenLayers.Control.DrawFeature(GUI.emapsplus.map.vectorLayer,
                        OpenLayers.Handler.RegularPolygon, {
                            handlerOptions: {
                                sides: 4,
                                irregular: true
                            }
                        }
                    )
                };

                for(var key in drawControls) {
                    GUI.emapsplus.map.myMap.addControl(drawControls[key]);
                }
			
			// ---------------------------------------------------------
			
			
			// ------------ this is for the measure tool ---------------
			var sketchSymbolizers = {
                "Point": {
                    pointRadius: 4,
                    graphicName: "square",
                    fillColor: "white",
                    fillOpacity: 1,
                    strokeWidth: 1,
                    strokeOpacity: 1,
                    strokeColor: "#333333"
                },
                "Line": {
                    strokeWidth: 3,
                    strokeOpacity: 1,
                    strokeColor: "#666666",
                    strokeDashstyle: "dash"
                },
                "Polygon": {
                    strokeWidth: 2,
                    strokeOpacity: 1,
                    strokeColor: "#666666",
                    fillColor: "white",
                    fillOpacity: 0.3
                }
            };
            
            var measureStyle = new OpenLayers.Style();
            measureStyle.addRules([  new OpenLayers.Rule({symbolizer: sketchSymbolizers}) ]);
            var styleMap = new OpenLayers.StyleMap({"default": measureStyle});
            
            var renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
            renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;

            measureControls = {
                line: new OpenLayers.Control.Measure(
                    OpenLayers.Handler.Path, {
                        persist: true,
                        handlerOptions: {
                            layerOptions: {
                                renderers: renderer,
                                styleMap: styleMap
                            }
                        }
                    }
                ),
                polygon: new OpenLayers.Control.Measure(
                    OpenLayers.Handler.Polygon, {
                        persist: true,
                        handlerOptions: {
                            layerOptions: {
                                renderers: renderer,
                                styleMap: styleMap
                            }
                        }
                    }
                )
            };
            
            var control;
            for(var key in measureControls) {
                control = measureControls[key];
                control.events.on({
                    "measure": GUI.emapsplus.map.handleMeasurements,
                    "measurepartial": GUI.emapsplus.map.handleMeasurements
                });
                GUI.emapsplus.map.myMap.addControl(control);
            }
			// ---------------------------------------------------------
            
            var panel = new OpenLayers.Control.CustomNavToolbar();
	        GUI.emapsplus.map.myMap.addControl(panel);
           
		},
		
		toggleLayer : function (layer){
			if(layer.getVisibility){
				//alert('hide');
				layer.setVisibility = false;
			}else{
				//alert('show');
				layer.setVisibility = true;
			}
		},
		
		toggleToolBox : function (id, isOpen){
			var toolbox = document.getElementById(id);
			if(isOpen){
				toolbox.style.visibility = 'visible';
			}else{
				toolbox.style.visibility = 'hidden';
			}
			
		},
			
		toolToggleMapScreen : function(){
			var container = document.getElementById('container');
			var ctlbar = document.getElementById('controlbar');
			var header = document.getElementById('header');
			var content = document.getElementById('content');
			var gistab = document.getElementById('gistab');
			var tab = document.getElementById('tabbar');
			var map = document.getElementById('map');
			var footer = document.getElementById('footer');
			
			if(GUI.emapsplus.map.isMaxScreen){
				ctlbar.style.display="block";
				header.style.display="block";
				gistab.style.display="inline";
				footer.style.display="block";
				
				container.style.width="865px";
				container.style.height="450px";
				content.style.top="24px";
				tab.style.height="450px";
				map.style.height="400px";
				
				
				GUI.emapsplus.map.isMaxScreen = false;
			}else{
				ctlbar.style.display="none";
				header.style.display="none";
				footer.style.display="none";
				gistab.style.display="none";
				
				container.style.width="100%";
				container.style.height="800px";	
				content.style.top="0px";						// determine dynamically
				tab.style.height="800px";
				map.style.width="100%";
				map.style.height="800px";
				
				GUI.emapsplus.map.isMaxScreen = true;
			}
			
			GUI.emapsplus.map.myMap.updateSize();
		},
		
	};
	
	GUI.emapsplus.map.init();
	
}else{
	console.log('GUI.emapsplus status failied!');
}
