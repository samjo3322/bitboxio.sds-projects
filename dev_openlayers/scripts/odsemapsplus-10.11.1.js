if(GUI.core.status() == 'READY'){
	
	GUI.emapsplus = {
	
		status : function(){ 
			return 'READY';
		},
		license : "Lic. Unlimited Users",
		version : "v10.11.1",
		appname : "eMapsPlus",
		rsoadminurl	: "services/rso/rso-admin-token-emaps.html",
		rsoauthurl	: "services/rso/rso-auth-token-emaps.html",
		
		init : function(){ 
			GUI.core.debugmsg('emapsplus is starting');
			
			GUI.core.debugmsg('loading ui...');
			
			GUI.core.loadjscssfile('styles/style.css', 'css');
			GUI.core.loadjscssfile('scripts/odscorelib-1.0.1.js', 'js');
			GUI.core.loadjscssfile('scripts/odsui-1.0.1.js', 'js');
			GUI.core.loadjscssfile('scripts/menu-for-applications.js', 'js');
			GUI.core.loadjscssfile('scripts/dom-drag.js', 'js');

			setTimeout("GUI.emapsplus.buildAppContainer()", 500);
		},
		
		buildAppContainer : function(){
			GUI.core.debugmsg('building app container...');
			var container_attr = new Array();
			container_attr[0] = ["id", "container"];
			var container = GUI.core.crossXBrowserNode('div', container_attr, document.body);
			
			GUI.emapsplus.buildCtlBar();
			GUI.emapsplus.buildHeader();
			GUI.emapsplus.buildNavTabs();
			GUI.emapsplus.buildFooter();
			
			if(GUI.core.debug){
				GUI.emapsplus.buildDebug();
			}
		},
		
		buildCtlBar : function(){
			GUI.core.debugmsg('building app controlbar...');
			var ctlbar_attr = new Array();
			ctlbar_attr[0] = ["id", "controlbar"];
			var ctlbar = GUI.core.crossXBrowserNode('div', ctlbar_attr, document.getElementById('container'));
			
			// add debug btn if enabled
			if(GUI.core.debug){
				var debugbtn_attr = new Array();
				debugbtn_attr[0] = ["src", "./assets/debug.png"];
				debugbtn_attr[1] = ["align", "right"];
				debugbtn_attr[2] = ["onclick", "toogleOdsConsoleBox();"];
				debugbtn_attr[3] = ["style", "margin-top:5px;"];
				var debugbtn = GUI.core.crossXBrowserNode('img', debugbtn_attr, ctlbar);
			}
			
			// append token if found
			if(GUI.core.getQuerystring('sdstoken') != ""){
				var dreamurl = "index.html?sdstoken=" + GUI.core.getQuerystring('sdstoken');
			}else{
				var dreamurl = "index.html";
			}
			
			var link_attr = new Array();
			link_attr[0] = ["href", dreamurl];
			link_attr[1] = ["alt", "DREAM"];
			link_attr[2] = ["style", "float:left"];
			var link = GUI.core.crossXBrowserNode('a', link_attr, ctlbar);
			
			var banimg_attr = new Array();
			banimg_attr[0] = ["src", "assets/dream-banner.png"];
			banimg_attr[1] = ["alt", "DREAM"];
			banimg_attr[2] = ["align", "left"];
			banimg_attr[3] = ["border", "0"];
			var banimg = GUI.core.crossXBrowserNode('img', banimg_attr, link);
			
			var welcome_attr = new Array();
			welcome_attr[0] = ["style", "padding-top:5px;"];
			var welcome = GUI.core.crossXBrowserNode('div', welcome_attr, ctlbar);
			
			if(GUI.core.getQuerystring('sdstoken') == ""){
				var html	=	'<marquee style="width:320px;color:#999999;padding-right:25px;">Sign in with user called \'guest\' or \'admin\'...</marquee>';
				html 		+=	'>> <input type="textfield" id="rso-txt-userid" value="user" onfocus="this.value=\'\';" style="width:95px;font-size:12px;font-style:italic;color:#666666" />';
				html 		+=	'<input type="password" id="rso-txt-pass" value="pass" onfocus="this.value=\'\';" style="width:65px;font-size:12px;font-style:italic;color:#666666" />';
				html 		+=	'<a onmouseover="this.style.cursor=\'pointer\';" onclick="GUI.emapsplus.handleSignIn();">Sign In</a>';
				welcome.innerHTML = html;
			}else{
				welcome.innerHTML = "<a href=\"analyst.html\" onmouseover=\"this.style.cursor=\'pointer\';\">Sign Out</a> | <a onclick=\"alert('enter license key...');\" onmouseover=\"this.style.cursor=\'pointer\';\">License</a>";
			}		
		},
		
		buildHeader : function(){
			GUI.core.debugmsg('building app header...');
			var header_attr = new Array();
			header_attr[0] = ["id", "header"];
			var header = GUI.core.crossXBrowserNode('div', header_attr, document.getElementById('container'));
			header.innerHTML = GUI.emapsplus.appname;
			
			var logo_attr = new Array();
			logo_attr[0] = ["src", "assets/ios-icon-E-57.png"];
			logo_attr[1] = ["alt", "emapsplus Logo"];
			logo_attr[2] = ["align", "right"];
			var logo = GUI.core.crossXBrowserNode('img', logo_attr, header);
		},
		
		buildNavTabs : function(){
			GUI.core.debugmsg('building app navtabs...');
			var tabs_attr = new Array();
			tabs_attr[0] = ["id", "tabbar"];
			tabs_attr[1] = ["class", "tabs"];
			var tabs = GUI.core.crossXBrowserNode('div', tabs_attr, document.getElementById('container'));
			
			GUI.core.debugmsg('checking user status...'); 
			GUI.emapsplus.map(); // public view
			
			
			if(GUI.core.getQuerystring('sdstoken') != ""){
				
				var auth = GUI.emapsplus.handleAuthorization(GUI.core.getQuerystring('sdstoken'));
				
				switch(auth){
					
					case 'authorizedUser':
						
						break;
						
					case 'adminUser':
						GUI.emapsplus.admin();
						break;
						
					default:
						// do nothing
						break;
				}
			}	
		},
		
		buildFooter : function(){
			GUI.core.debugmsg('building app footer...');
			var footer_attr = new Array();
			footer_attr[0] = ["id", "footer"];
			var footer = GUI.core.crossXBrowserNode('div', footer_attr, document.getElementById('container'));
			var code = "<br/>&copy;2010-2012 Smart Open Digital Solutions. All rights reserved. | " + GUI.emapsplus.appname + " " + GUI.emapsplus.version;
			if(GUI.core.getQuerystring('sdstoken') != ""){
				code += " | " + GUI.emapsplus.license;
			}
			footer.innerHTML = code;
		},
		
		buildDebug : function(){
			GUI.core.debugmsg('building app debugger...');
			
			var div = document.getElementById('container');
			var console_attr = new Array();
			console_attr[0] = ["id", "ods-consolewin"];
			console_attr[1] = ["style", "width:99%;padding:8px;"];
			var console = GUI.core.crossXBrowserNode('div', console_attr, div);
			
			var debug_attr = new Array();
			debug_attr[0] = ["id", "ods-consolebox"];
			debug_attr[1] = ["style", "width:100%;font-size:10pt;display:none;"];
			var debuger = GUI.core.crossXBrowserNode('div', debug_attr, console);
			
			var bugscript_attr = new Array();
			bugscript_attr[0] = ["style", "text/javascript"];
			var bugscript = GUI.core.crossXBrowserNode('script', bugscript_attr, debuger);
			var code	=	'\tvar odsconsolebox = document.getElementById(\'ods-consolebox\');\n';
			code		+=	'\tvar debugwin;\n';
			code		+=	'\ttry{\n';
			code		+=	'\t\tdebugwin = document.createElement(\'&lt;textarea id="ods-console"  style="width:99%;height:75px;background:#333333;color:#00ff00;" /&gt;\');\n';
			code		+=	'\t}catch(err){\n';
			code		+=	'\t\tdebugwin = document.createElement(\'textarea\');\n';
			code		+=	'\t\tdebugwin.setAttribute("id", "ods-console");\n';
			code		+=	'\t\tdebugwin.setAttribute("style", "width:99%;height:75px;background:#333333;color:#00ff00;");}\n';
			code		+=	'\todsconsolebox.appendChild(debugwin);\n';
			code		+=	'\tfunction toogleOdsConsoleBox(){\n';
			code		+=	'\t\tvar odsconsolebox =  document.getElementById(\'ods-consolebox\');\n';
			code		+=	'\t\tif(odsconsolebox.style.display == "none"){\n';
			code		+=	'\t\t\todsconsolebox.style.display = "inline";\n';
			code		+=	'\t\t\todsconsolebox.style.width = "100%";\n';
			code		+=	'\t\t}else{';
			code		+=	'\t\t\todsconsolebox.style.display = "none";}\n';
			code		+=	'}';
			
			bugscript.innerHTML = code;
		},
		
		admin : function(){
			GUI.core.debugmsg('loading admin...');
			GUI.core.loadjscssfile('scripts/odsemapsplus-admin-10.11.1.js', 'js');
			
			var tab_attr = new Array();
			tab_attr[0] = ["id", "emapsplus-tab-admin"];
			tab_attr[1] = ["class", "tab"];
			var tab = GUI.core.crossXBrowserNode('div', tab_attr, document.getElementById('tabbar'));
			
			var radiobtn_attr = new Array();
			radiobtn_attr[0] = ["id", "tab-1"];
			radiobtn_attr[1] = ["type", "radio"];
			radiobtn_attr[2] = ["name", "tab-group1"];
			var radiobtn = GUI.core.crossXBrowserNode('input', radiobtn_attr, tab);
			
			var lbl_attr = new Array();
			lbl_attr[0] = ["for", "tab-1"];
			lbl_attr[1] = ["onclick", "GUI.emapsplus.resetTabHeight(1320);GUI.emapsplus.toggleMapView(false);"];
			var lbl = GUI.core.crossXBrowserNode('label', lbl_attr, tab);
			lbl.innerHTML = "Administration";
			
			var content_attr = new Array();
			content_attr[0] = ["class", "content"];
			var content = GUI.core.crossXBrowserNode('div', content_attr, tab);

			var display_attr = new Array();
			display_attr[0] = ["name", "emapsplus-admin-com"];
			display_attr[1] = ["id", "emapsplus-admin-com"];
			display_attr[2] = ["style", "text-align:left;overflow:hidden;height:1305px;padding-right:5px;"];
			var display_attr = GUI.core.crossXBrowserNode('div', display_attr, content);
		},
		
		map : function(){
			GUI.core.debugmsg('loading map...');
			GUI.core.loadjscssfile('scripts/odsemapsplus-map-10.11.1.js', 'js');
			
			var tab_attr = new Array();
			tab_attr[0] = ["id", "emapsplus-tab-map"];
			tab_attr[1] = ["class", "tab"];
			var tab = GUI.core.crossXBrowserNode('div', tab_attr, document.getElementById('tabbar'));
			
			var radiobtn_attr = new Array();
			radiobtn_attr[0] = ["id", "tab-2"];
			radiobtn_attr[1] = ["type", "radio"];
			radiobtn_attr[2] = ["name", "tab-group1"];
			radiobtn_attr[3] = ["checked", "true"];
			var radiobtn = GUI.core.crossXBrowserNode('input', radiobtn_attr, tab);
			
			var lbl_attr = new Array();
			lbl_attr[0] = ["for", "tab-2"];
			lbl_attr[1] = ["onclick", "GUI.emapsplus.resetTabHeight(450);GUI.emapsplus.toggleMapView(true);"];
			lbl_attr[2] = ["id", "gistab"];
			var lbl = GUI.core.crossXBrowserNode('label', lbl_attr, tab);
			lbl.innerHTML = "GIS Viewer";
			
			var content_attr = new Array();
			content_attr[0] = ["class", "content"];
			content_attr[1] = ["id", "content"];
			var content = GUI.core.crossXBrowserNode('div', content_attr, tab);

			var display_attr = new Array();
			display_attr[0] = ["name", "emapsplus-map-com"];
			display_attr[1] = ["id", "emapsplus-map-com"];
			display_attr[2] = ["style", "text-align:left;"];
			var display_attr = GUI.core.crossXBrowserNode('div', display_attr, content);
		},
		
		handleSignIn : function(){
			// get user reference
			var usr = document.getElementById('rso-txt-userid').value;
			
			switch(usr){
				
				case 'guest':
					window.location.href = GUI.emapsplus.rsoauthurl;
					break;
					
				case 'admin':
					window.location.href = GUI.emapsplus.rsoadminurl;
				
				default: 
					alert('user cannot be determined!');
					break;
			}
		},
		
		handleAuthorization : function(token){
			
			// this is a mock method
			var profilekey = '';
			
			if(token == "qweqrr323234werer45yh.ghjgdeeee") { profilekey = "authorizedUser"; }
			if(token == "ttzxxvdsjjkl.ghjgdeeee") { profilekey = "adminUser"; }
			
			return profilekey;
		},
		
		resetTabHeight : function (height){
			var tab = document.getElementById('tabbar');
			tab.style.height = height + "px";
		},
		
		toggleMapView : function(hide){
			var map = document.getElementById('map');
			if(!hide){
				map.style.visibility = "hidden";
			}else{
				map.style.visibility = "visible";
			}
		}
		
	};
	
}else{
	console.log('GUI.core status failied!');
}







