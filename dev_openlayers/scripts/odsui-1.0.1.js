if(GUI.core.status() == 'READY'){
	
	GUI.ui = {
		
		stdNavTabBar : function(){
			//
		},
		
		stdHeader : function(label, css) {
			var el;
			try
			{
				el = document.createElement('<div class="' + css + '">');
				el.appendChild(document.createTextNode(label));
			}catch(err){
				el = document.createElement('div');
				el.setAttribute("class", css);
				el.appendChild(document.createTextNode(label));
			}
			return el;
		},
		
		stdBtn : function(id, label, handler){
			var el;
			try
			{
				el = document.createElement('<button id="' + id + '" onclick="' + handler + '">');
				el.appendChild(document.createTextNode(label));
			}catch(err){
				el = document.createElement('button');
				el.setAttribute("id", id);
				el.setAttribute("onclick", handler);
				el.appendChild(document.createTextNode(label));
			}
			return el;
		},
		
		stdTxtField : function(id, defaultLbl, width){
			var el;
			try
			{
				el = document.createElement('<input type="text" id="' + id + '" style="background-color:#dddddd;color:#666666;width:' + width + 'px;" value="' + defaultLbl + ' onfocus="this.value=null;">');
			}catch(err){
				el = document.createElement('input');
				el.setAttribute("type", "text");
				el.setAttribute("id", id);
				el.setAttribute("width", width);
				el.setAttribute("style", 'background-color:#dddddd;color:#666666;width:' + width + 'px;');
				el.setAttribute("onfocus", "this.value=null;");
				el.value = defaultLbl
			}
			return el;
		},
		
		stdTxtArea : function(id, defaultLbl, style){
			var el;
			try
			{
				el = document.createElement('<textarea id="' + id + '" style="' + style + '" value="' + defaultLbl + ' onfocus="this.value=null;">');
			}catch(err){
				el = document.createElement('textarea');
				el.setAttribute("id", id);
				el.setAttribute("style", style);
				el.setAttribute("onfocus", "this.value=null;");
				el.value = defaultLbl
			}
			return el;
		},
		
		stdRadioBool : function(id, labeltxt, selopt){
			var div;
			try
			{
				div = document.createElement('<div id="' + id + '" style="padding:5px;float:none;height:18px;">');
			}catch(err){
				div = document.createElement('div');
				div.setAttribute("id", id);
				div.setAttribute("style", "padding:5px;float:none;height:18px;");
			}
			
			var wrapleft_attr = new Array();
			wrapleft_attr[0] = ["style", "padding:3px;width:130px;float:left;verticle-align:middle;"];
			var wrapleft = GUI.core.crossXBrowserNode('div', wrapleft_attr, div);
			var wrapright_attr = new Array();
			wrapright_attr[0] = ["style", "padding:3px;verticle-align:middle;"];
			var wrapright = GUI.core.crossXBrowserNode('div', wrapright_attr, div);
			
			var label_attr = new Array();
			var label = GUI.core.crossXBrowserNode('b', label_attr, wrapleft);
			label.innerHTML = labeltxt;
			
			var radio1_attr = new Array();
			radio1_attr[0] = ["name", id];
			radio1_attr[1] = ["type", "radio"];
			radio1_attr[2] = ["value", 1];
			radio1_attr[3] = ["style", "display:inline;"];
			var radio1 = GUI.core.crossXBrowserNode('input', radio1_attr, wrapright);
			var on = document.createTextNode('ON  ');
			wrapright.appendChild(on);
			
			var radio2_attr = new Array();
			radio2_attr[0] = ["name", id];
			radio2_attr[1] = ["type", "radio"];
			radio2_attr[2] = ["value", 0];
			radio2_attr[3] = ["style", "display:inline;"];
			var radio2 = GUI.core.crossXBrowserNode('input', radio2_attr, wrapright);
			var off = document.createTextNode('OFF');
			wrapright.appendChild(off);
			
			return div;
		},
		
		stdCombo : function(id, defaultVal, options){
			var el;
			var sel;
			try
			{
				el = document.createElement('<select id="' + id + '" style="background-color:#dddddd;color:#666666;">');
			}catch(err){
				el = document.createElement('select');
				el.setAttribute("id", id);
				el.setAttribute("style", "background-color:#dddddd;color:#666666;");
			}
			
			try
			{
				sel = document.createElement('<option>');
			}catch(err){
				sel = document.createElement('option');
			}
			
			sel.appendChild(document.createTextNode(defaultVal));
			el.appendChild(sel);
			
			
			for(var i=0;i<options.length;i++){
				try
				{
					opt = document.createElement('<option>');
				}catch(err){
					opt = document.createElement('option');
				}
				opt.value = options[i][0];
				opt.appendChild(document.createTextNode(options[i][1]));
				el.appendChild(opt);
			}
			
			return el;
		},
		
		stdListBoxBtns : function(handler1, handler2, height) {
			
			var btn1;
			var btn2;
			var elBox;
			var br = document.createElement('br');
			
			try
			{
				elBox = document.createElement('<div style="float:left;vertical-align:middle;padding:8px;height: ' + height + 'px;padding-top:20px;">');
			}catch(err){
				elBox = document.createElement('div');
				elBox.setAttribute("style", "float:left;vertical-align:middle;padding:8px;height: " + height + "px;padding-top:20px;");
			}
			
			
			try
			{
				btn1 = document.createElement('<button onclick="' + handler1 + '">');
				btn1.appendChild(document.createTextNode('<<'));
				btn2 = document.createElement('<button onclick="' + handler2 + '">');
				btn2.appendChild(document.createTextNode('>>'));
			}catch(err){
				btn1 = document.createElement('button');
				btn1.setAttribute("onclick", handler1);
				btn1.appendChild(document.createTextNode('<<'));
				btn2 = document.createElement('button');
				btn2.setAttribute("onclick", handler2);
				btn2.appendChild(document.createTextNode('>>'));
			}
			
			elBox.appendChild(btn1);
			elBox.appendChild(br);
			elBox.appendChild(btn2);
			
			return elBox;
		},
		
		stdListBox : function(id, label, options, size){
			
			var el;
			var sel;
			var elBox
			var br = document.createElement('br');
			
			try
			{
				elBox = document.createElement('<div style="float:left;padding:10px;">');
			}catch(err){
				elBox = document.createElement('div');
				elBox.setAttribute("style", "float:left;padding:10px;");
			}
			
			elBox.innerHTML = label;
			elBox.appendChild(br);
			
			try
			{
				el = document.createElement('<select id="' + id + '" size="' + size + '" style="background-color:#dddddd;color:#666666;width:210px;" multiple>');
			}catch(err){
				el = document.createElement('select');
				el.setAttribute("id", id);
				el.setAttribute("size", size);
				el.setAttribute("multiple", true);
				el.setAttribute("style", "background-color:#dddddd;color:#666666;width:210px;");
			}
			
			
			for(var i=0;i<options.length;i++){
				try
				{
					opt = document.createElement('<option>');
				}catch(err){
					opt = document.createElement('option');
				}
				opt.value = options[i][0];
				opt.appendChild(document.createTextNode(options[i][1]));
				el.appendChild(opt);
			}
			
			elBox.appendChild(el);
			
			return elBox;
		},
		
		stdLabel : function(txt, id, style){
			var lbl;
			try
			{
				lbl = document.createElement('<div id="' + id + '" style="' + style + '>');
			}catch(err){
				lbl = document.createElement('div');
				lbl.setAttribute("id", id);
				lbl.setAttribute("style", style);
			}
			
			lbl.innerHTML = txt;
			
			return lbl;
		},
		
		stdCal : function(){
			
		},
		
		stdGrid : function(id, headObj, dataObj){
			var griddiv = document.createElement('div');
			
			// make table tag
			var tbl;
			try
			{
				tbl = document.createElement('<table cellpadding="0" cellspacing="0" border="0" id="' + id + 'class="sortable">');
			}catch(err){
				tbl = document.createElement('table');
				tbl.setAttribute("cellspacing", "0");
				tbl.setAttribute("cellpadding", "0");
				tbl.setAttribute("border", "0");
				tbl.setAttribute("id", id);
				tbl.setAttribute("class", "sortable");
			}
			
			// make table header
			var thead = document.createElement('thead');
			var tr = document.createElement('tr');
			for(var i=0; i<headObj.length; i++){
				var th = document.createElement('th');
				th.innerHTML = '<h3>' + headObj[i][0] + '</h3>';
				tr.appendChild(th);
			}
			
			// make table body
			var tbody = document.createElement('tbody');
			for(var j=0; j<dataObj.length; j++){
				var row = document.createElement('tr');
				var data = dataObj[j];
				for(var k=0; k<data.length; k++){
					var td = document.createElement('td');
					td.innerHTML = data[k];
					row.appendChild(td);
				}
				
				tbody.appendChild(row);
			}
			
			// make controldiv
			var gridctl;
			try
			{
				gridctl = document.createElement('<div id="controls">');
			}catch(err){
				gridctl = document.createElement('div');
				gridctl.setAttribute("id", "controls");
			}
			
			var gridct_lbl2;
			try
			{
				gridct_lbl2 = document.createElement('<div id="text">');
			}catch(err){
				gridct_lbl2 = document.createElement('div');
				gridct_lbl2.setAttribute("id", "text");
			}

			gridct_lbl2.innerHTML = 'Displaying ' + '<span id="currentpage"></span> of <span id="pagelimit"></span>';
			gridctl.appendChild(gridct_lbl2);
			
			// make nav control
			var gridctl_nav;
			try
			{
				gridctl_nav = document.createElement('<div id="navigation">');
			}catch(err){
				gridctl_nav = document.createElement('div');
				gridctl_nav.setAttribute("id", "navigation");
			}
			
			var gridctl_navimg1; // first
			try
			{
				gridctl_navimg1 = document.createElement('<img src="assets/first.gif" width="16" height="16" alt="first page" onclick="sorter.move(-1,true)">');
			}catch(err){
				gridctl_navimg1 = document.createElement('img');
				gridctl_navimg1.setAttribute("src", "assets/first.gif");
				gridctl_navimg1.setAttribute("width", "16");
				gridctl_navimg1.setAttribute("height", "16");
				gridctl_navimg1.setAttribute("alt", "first page");
				gridctl_navimg1.setAttribute("onclick", "sorter.move(-1,true)");
			}
			gridctl_nav.appendChild(gridctl_navimg1);
			
			var gridctl_navimg2; // previous
			try
			{
				gridctl_navimg2 = document.createElement('<img src="assets/previous.gif" width="16" height="16" alt="previous page" onclick="sorter.move(-1)">');
			}catch(err){
				gridctl_navimg2 = document.createElement('img');
				gridctl_navimg2.setAttribute("src", "assets/previous.gif");
				gridctl_navimg2.setAttribute("width", "16");
				gridctl_navimg2.setAttribute("height", "16");
				gridctl_navimg2.setAttribute("alt", "previous page");
				gridctl_navimg2.setAttribute("onclick", "sorter.move(-1)");
			}
			gridctl_nav.appendChild(gridctl_navimg2);
			
			var gridctl_navimg3; // next
			try
			{
				gridctl_navimg3 = document.createElement('<img src="assets/next.gif" width="16" height="16" alt="next page" onclick="sorter.move(1)">');
			}catch(err){
				gridctl_navimg3 = document.createElement('img');
				gridctl_navimg3.setAttribute("src", "assets/next.gif");
				gridctl_navimg3.setAttribute("width", "16");
				gridctl_navimg3.setAttribute("height", "16");
				gridctl_navimg3.setAttribute("alt", "next page");
				gridctl_navimg3.setAttribute("onclick", "sorter.move(1)");
			}
			gridctl_nav.appendChild(gridctl_navimg3);
			
			var gridctl_navimg4; // next
			try
			{
				gridctl_navimg4 = document.createElement('<img src="assets/last.gif" width="16" height="16" alt="last page" onclick="sorter.move(1, true)">');
			}catch(err){
				gridctl_navimg4 = document.createElement('img');
				gridctl_navimg4.setAttribute("src", "assets/last.gif");
				gridctl_navimg4.setAttribute("width", "16");
				gridctl_navimg4.setAttribute("height", "16");
				gridctl_navimg4.setAttribute("alt", "last page");
				gridctl_navimg4.setAttribute("onclick", "sorter.move(1, true)");
			}
			gridctl_nav.appendChild(gridctl_navimg4);
			gridctl.appendChild(gridctl_nav);
			
			// make per page ctl
			var gridctl_perpage;
			try
			{
				gridctl_perpage = document.createElement('<div id="perpage">');
			}catch(err){
				gridctl_perpage = document.createElement('div');
				gridctl_perpage.setAttribute("id", "perpage");
			}
			
			var gridctl_sel;
			try
			{
				gridctl_sel = document.createElement('<select onchange="sorter.size(this.value)">');
			}catch(err){
				gridctl_sel = document.createElement('select');
				gridctl_sel.setAttribute("onchange", "sorter.size(this.value)");
			}
			gridctl_perpage.appendChild(gridctl_sel);
			
			var gridctl_opts = new Array(); // adding options: 5, 10, 20, 50, 100
			gridctl_opts[0] = [5, ''];
			gridctl_opts[1] = [10, 'selected'];
			gridctl_opts[2] = [20, ''];
			gridctl_opts[3] = [50, ''];
			gridctl_opts[4] = [100, ''];
			for(var l=0; l<gridctl_opts.length; l++){
				var opt = document.createElement('option');
				opt.value = gridctl_opts[l][0];
				opt.innerHTML = gridctl_opts[l][0];
				gridctl_sel.appendChild(opt);
			}
			
			var gridct_lbl1 = document.createElement('span');
			gridct_lbl1.innerHTML = 'Per Page&nbsp;';
			
			gridctl_perpage.appendChild(gridctl_sel);
			gridctl_perpage.appendChild(gridct_lbl1);
			gridctl.appendChild(gridctl_perpage);
			
			// javascrip tag
			var jsscript;
			try
			{
				jsscript = document.createElement('<script type="text/javascript">');
			}catch(err){
				jsscript = document.createElement('script');
				jsscript.setAttribute("id", "text/javascript");
			}
			var jstxt 	= 	'var sorter = new TINY.table.sorter("sorter");';
			jstxt 		+= 	'sorter.head = "head";';
			jstxt 		+= 	'sorter.asc = "asc";';
			jstxt 		+= 	'sorter.desc = "desc";';
			jstxt 		+= 	'sorter.even = "evenrow";';
			jstxt 		+= 	'sorter.odd = "oddrow";';
			jstxt 		+= 	'sorter.evensel = "evenselected";';
			jstxt 		+= 	'sorter.oddsel = "oddselected";';
			jstxt 		+= 	'sorter.paginate = true;';
			jstxt 		+= 	'sorter.currentid = "currentpage";';
			jstxt 		+= 	'sorter.limitid = "pagelimit";';
			jstxt 		+= 	'sorter.init("table",1);';
			jstxt 		+= 	'function openJS(){alert(\'loaded\');}';
			jstxt 		+= 	'function closeJS(){alert(\'closed\');}';
			jsscript.innerHTML = jstxt;
			gridctl.appendChild(jsscript);
			
			// add to layout
			thead.appendChild(tr);
			tbl.appendChild(thead);
			tbl.appendChild(tbody);
			griddiv.appendChild(gridctl);
			griddiv.appendChild(tbl);
			
			return griddiv;
		},
		
		stdUnOrderedList : function(id){
			var el;
			try
			{
				el = document.createElement('<ul id="' + id + '" class="ckbox">');
			}catch(err){
				el = document.createElement('ul');
				el.setAttribute("id", id);
				el.setAttribute("class", "ckbox");
			}
			return el;
		},
		
		stdCheckBox : function(id, name, val, handler, style, checked){
			var el;
			try
			{
				el = document.createElement('<input type="checkbox" id="' + id + '" value="' + val + '" onchange="' + handler + '" style="' + style + '" checked="' + checked + '">');
			}catch(err){
				el = document.createElement('input');
				el.setAttribute("type", "checkbox");
				el.setAttribute("id", id);
				el.setAttribute("onchange", handler);
				el.setAttribute("style", style);
				el.setAttribute("checked", checked);
				el.value = val;
			}
			return el;
		},
		
		stdAccordion : function(id, width, obj){
			var el;
			try
			{
				el = document.createElement('<div id="' + id + '" class="AccordionContainer" style="width:' + width + 'px;" >');
			}catch(err){
				el = document.createElement('div');
				el.setAttribute("id", id);
				el.setAttribute("class", "AccordionContainer");
				el.setAttribute("style", 'width:' + width + 'px;');
			}
			
			for(var i=0; i<obj.length; i++){
				
				var acc;
				try{
					acc = document.createElement('<div onclick="runAccordion(' + i + ');">');
				}catch(err){
					acc = document.createElement('div');
					acc.setAttribute('onclick', 'runAccordion(' + i +');');
				}
				el.appendChild(acc);
				
				var acctitle;
				try{
					acctitle = document.createElement('<div class="AccordionTitle" onselectstart="return false;">');
				}catch(err){
					acctitle = document.createElement('div');
					acctitle.setAttribute('class', 'AccordionTitle');
					acctitle.setAttribute('onselectstart', 'return false');
				}
				acctitle.innerHTML = obj[i][0];
				acc.appendChild(acctitle);
				
				var acccontent;
				try{
					acccontent = document.createElement('<div id="Accordion' + i + 'Content" class="AccordionTitle" onselectstart="return false;">');
				}catch(err){
					acccontent = document.createElement('div');
					acccontent.setAttribute('id', 'Accordion' + i + 'Content');
					acccontent.setAttribute('class', 'AccordionContent');
					acccontent.setAttribute('style', 'width:' + width + 'px;');
				}
				acccontent.innerHTML = obj[i][1];
				acc.appendChild(acccontent);
			}
			
			return el;
		},
		
		fileUpload : function(id, handler){
			var el;
			try
			{
				el = document.createElement('<input type="file" id="' + id + '" size="40" style="background-color:#dddddd;color:#666666;" onchange="' + handler + '" multiple>');
			}catch(err){
				el = document.createElement('input');
				el.setAttribute("type", "file");
				el.setAttribute("id", id);
				el.setAttribute("size", "60");
				el.setAttribute("style", 'background-color:#dddddd;color:#666666;');
				el.setAttribute("onchange", handler);
				el.setAttribute("multiple", "");
			}
			return el;
		},
		
		viewstackControlCom : function(btnObj, vsHeight){
			
			var z;
			try
			{
				z = document.createElement('<div style="width:100%;padding:2px;">');
			}catch(err){
				z = document.createElement('div');
				z.setAttribute("style", 'width:100%;padding:2px;');
			}
			
			var d;
			try
			{
				d = document.createElement('<div style="text-align:center;width:100%;z-index:10;">');
			}catch(err){
				d = document.createElement('div');
				d.setAttribute("style", 'text-align:right;width:100%;');
			}
			
			var c;
			try
			{
				c = document.createElement('<div style="text-align:center;width:100%;height:' + vsHeight + 'px;border:1px solid #ccc;">');
			}catch(err){
				c = document.createElement('div');
				c.setAttribute("style", 'text-align:center;width:100%;height:' + vsHeight + 'px;border:1px solid #ccc;');
			}
			
			for(var i=0; i<btnObj.length; i++){
				
				// control bar
				var b;
				try
				{
					b = document.createElement('<div class="vsbutton">');
				}catch(err){
					b = document.createElement('div');
					b.setAttribute("class", 'vsbutton');
				}
				b.innerHTML = '<a onclick="' + btnObj[i][0] + '">' + btnObj[i][1] + '</a>';
				d.appendChild(b);
				
				
				// viewstack section divs
				var mode;
				if(i == 0){
					mode = 'inline';
				}else{
					mode = 'none';
				}
				var s;
				try
				{
					s = document.createElement('<div id="' + btnObj[i][2] + '" style="width:100%;height:' + (vsHeight - 5) + 'px;display:' + mode + ';">');
				}catch(err){
					s = document.createElement('div');
					s.setAttribute("id", btnObj[i][2]);
					s.setAttribute("style", 'width:100%;height:' + (vsHeight - 5) + 'px;display:' + mode + ';');
				}
				c.appendChild(s);
			}
			
			z.appendChild(d)
			z.appendChild(c);
			
			return z;
		},
		
		viewstackClickHandler : function(vs, len, index){
			
			for(var i=0; i<=len; i++){
				var el = document.getElementById(vs + i);
				
				if(i == index){
					el.style.display = 'inline';
				}else{
					el.style.display = 'none';
				}
			}
		},
		
		editorControl : function(id){
			var wrap;
			try
			{
				wrap = document.createElement('<div style="width:100%;">');
			}catch(err){
				wrap = document.createElement('div');
				wrap.setAttribute("style", 'width:100%;padding-top:5px;');
			}
			
			// textarea
			var txtarea;
			try
			{
				txtarea = document.createElement('<textarea id="' + id + '" name="content" style="width:100%;height:300px;">');
			}catch(err){
				txtarea = document.createElement('textarea');
				txtarea.setAttribute("id", id);
				txtarea.setAttribute("name", "content");
				txtarea.setAttribute("style", "width:100%;height:300px;");
			}
			wrap.appendChild(txtarea);
			
			var config;
			try
			{
				config = document.createElement('<script type="text/javascript">');
			}catch(err){
				config = document.createElement('script');
				config.setAttribute("type", 'text/javascript');
			}
			
			var code	= 	'tinyMCE.init({\n';
			code		+=	'\tmode : "exact",\n';
			code		+=	'\telements : "correspondence-content",\n';
			code		+=	'\ttheme : "advanced",\n';
			code		+=	'\tplugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",\n';
			code		+=	'\ttheme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",\n';
			code		+=	'\ttheme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",\n';
			code		+=	'\ttheme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",\n';
			code		+=	'\ttheme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",\n';
			code		+=	'\ttheme_advanced_toolbar_location : "top",\n';
			code		+=	'\ttheme_advanced_toolbar_align : "left",\n';
			code		+=	'\ttheme_advanced_statusbar_location : "bottom",\n';
			code		+=	'\ttheme_advanced_resizing : true,\n';
			code		+=	'\tskin : "o2k7",\n';
			code		+=	'\tskin_variant : "silver",\n';
			code		+=	'\tcontent_css : "scripts/tiny_mce/themes/advanced/skins/o2k7/content.css",\n';
			code		+=	'\ttemplate_external_list_url : "js/template_list.js",\n';
			code		+=	'\texternal_link_list_url : "js/link_list.js",\n';
			code		+=	'\texternal_image_list_url : "js/image_list.js",\n';
			code		+=	'\tmedia_external_list_url : "js/media_list.js",\n';
			code		+=	'\ttemplate_replace_values : {\n';
			code		+=	'\t\tusername : "samc3322",\n';
			code		+=	'\t\tstaffid : "3322"\n';
			code		+=	'\t}\n';
			code		+=	'});';
			
			config.innerHTML = code;
			wrap.appendChild(config);
			
			return wrap;
		},
		
		multipartForm : function(id, method, action){
			var el;
			try
			{
				el = document.createElement('<form id="' + id + '" method="' + method + '" action="' + action + '" enctype="multipart/form-data" target="_new">');
			}catch(err){
				el = document.createElement('form');
				el.setAttribute("method", method);
				el.setAttribute("action", action);
				el.setAttribute("enctype", "enctype");
			}
			return el;
		},
		
		
		floatDiaBox : function(id, title, content, handler, style){
			var dia;
			try
			{
				dia = document.createElement('<div id="' + id + '" class="floatDiaBox-wrap" style="width:' + style + '">');
			}catch(err){
				dia = document.createElement('div');
				dia.setAttribute("id", id);
				dia.setAttribute("class", "floatDiaBox-wrap");
				dia.setAttribute("style", style);
			}
			
			var head;
			try
			{
				head = document.createElement('<div class="floatDiaBox-title">');
			}catch(err){
				head = document.createElement('div');
				head.setAttribute("class", "floatDiaBox-title");
			}
			head.innerHTML = title + "<img src=\"./assets/min.png\" align=\"right\" onclick=\"" + handler + "\">";
			dia.appendChild(head);
			
			var box;
			try
			{
				box = document.createElement('<div class="width:100%;height:100%;background-color:#ffffff;padding:10px;border-style:solid;border-top-width:1px;border-color:#444444;">');
			}catch(err){
				box = document.createElement('div');
				box.setAttribute("class", "floatDiaBox-content");
			}
			box.appendChild(content);
			dia.appendChild(box);
			
			return dia;
		}
	};
	
}else{
	console.log('GUI.core status failied!');
}







