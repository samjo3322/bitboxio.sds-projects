IF NOT EXISTS ( SELECT  *
                FROM    master.dbo.syslogins
                WHERE   loginname = N'SDSDoc' ) 
    CREATE LOGIN [SDSDoc] WITH PASSWORD = 'p@ssw0rd'
GO

CREATE USER [SDSDoc] FOR LOGIN [SDSDoc]
GO

PRINT N'Creating schemata'
GO
CREATE SCHEMA [doc]
AUTHORIZATION [SDSDoc]
GO

PRINT N'Creating [doc].[BkMrkData]'
GO
CREATE TABLE [doc].[BkMrkData] ( [BkMrkDataID] [int] NOT NULL
                                                     IDENTITY(1, 1),
                                 [dbName] [varchar](50) NOT NULL,
                                 [viewPrefix] [varchar](50) NOT NULL,
                                 [dataSource] [varchar](50) NOT NULL,
                                 [BookmarkName] [varchar](50) NULL,
                                 [query] [varchar](MAX) NOT NULL,
                                 [UseBy] [varchar](150) NULL,
                                 [createdBy] [int] NOT NULL,
                                 [createDate] [datetime] NOT NULL,
                                 [modifiedBy] [int] NULL,
                                 [modifiedDate] [datetime] NULL,
                                 [Note] [varchar](MAX) NULL )
GO

PRINT N'Creating primary key [PK_Bookmark] on [doc].[BkMrkData]'
GO
ALTER TABLE [doc].[BkMrkData] ADD CONSTRAINT [PK_Bookmark] PRIMARY KEY CLUSTERED  ([BkMrkDataID])
GO

PRINT N'Creating [doc].[PR_Get_BkMrkData]'
GO

CREATE PROCEDURE doc.PR_Get_BkMrkData
-- =============================================
-- Author:  Robert Francis
-- Create date:	05/10/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Get_BkMrkData
-- Changelog: 05/10/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    SELECT  BkMrkDataID,
            dbName,
            viewPrefix,
            dataSource,
            BookmarkName,
            query,
            UseBy,
            createdBy,
            createDate,
            modifiedBy,
            modifiedDate,
            Note
    FROM    doc.BkMrkData ;
END

GO

PRINT N'Creating [doc].[PR_Set_BkMrkData]'
GO

CREATE PROCEDURE [doc].[PR_Set_BkMrkData] ( @BkMrkDataID INT,
                                            @dbName VARCHAR(50),
                                            @viewPrefix VARCHAR(50),
                                            @dataSource VARCHAR(50),
                                            @BookmarkName VARCHAR(50),
                                            @query VARCHAR(MAX),
                                            @UseBy VARCHAR(150),
                                            @createdBy INT,
                                            @createDate DATETIME,
                                            @modifiedBy INT,
                                            @modifiedDate DATETIME,
                                            @Note VARCHAR(MAX) )
-- =============================================
-- Author:  Robert Francis
-- Create date:	05/10/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Set_BkMrkData @BkMrkDataID INT, @dbName, @viewPrefix, @dataSource, @BookmarkName, @query, @UseBy, @createdBy, @createDate, @modifiedBy, @modifiedDate, @Note
-- Changelog: 05/10/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    
    MERGE doc.BkMrkData AS target
        USING 
            ( SELECT    @BkMrkDataID,
                        @dbName,
                        @viewPrefix,
                        @dataSource,
                        @BookmarkName,
                        @query,
                        @UseBy,
                        @createdBy,
                        @createDate,
                        @modifiedBy,
                        @modifiedDate,
                        @Note ) AS source ( BkMrkDataID, dbName, viewPrefix,
                                            dataSource, BookmarkName, query,
                                            UseBy, createdBy, createDate,
                                            modifiedBy, modifiedDate, Note )
        ON SOURCE.BkMrkDataID = TARGET.BkMrkDataID
        WHEN MATCHED 
            THEN UPDATE
               SET      dbName = source.dbName,
                        viewPrefix = source.viewPrefix,
                        dataSource = source.dataSource,
                        BookmarkName = source.BookmarkName,
                        query = source.query,
                        UseBy = source.UseBy,
                        createdBy = source.createdBy,
                        createDate = source.createDate,
                        modifiedBy = source.modifiedBy,
                        modifiedDate = source.modifiedDate,
                        Note = source.Note
        WHEN NOT MATCHED 
            THEN INSERT ( dbName,
                          viewPrefix,
                          dataSource,
                          BookmarkName,
                          query,
                          UseBy,
                          createdBy,
                          createDate,
                          modifiedBy,
                          modifiedDate,
                          Note )
               VALUES   ( source.dbName,
                          source.viewPrefix,
                          source.dataSource,
                          source.BookmarkName,
                          source.query,
                          source.UseBy,
                          source.createdBy,
                          source.createDate,
                          source.modifiedBy,
                          source.modifiedDate,
                          source.Note ) ;  
    		 
    IF @@ROWCOUNT <> 1 
        SELECT  1 ;
    ELSE 
        SELECT  0 ;
END

GO

PRINT N'Creating [doc].[refDocumentType]'
GO
CREATE TABLE [doc].[refDocumentType] ( [documentTypeID] [int] NOT NULL
                                                              IDENTITY(1, 1),
                                       [documentType] [nvarchar](50) NULL,
                                       [isActive] [bit] NULL )
GO

PRINT N'Creating primary key [PK_refDocumentType] on [doc].[refDocumentType]'
GO
ALTER TABLE [doc].[refDocumentType] ADD CONSTRAINT [PK_refDocumentType] PRIMARY KEY CLUSTERED  ([documentTypeID])
GO

PRINT N'Creating [doc].[PR_Get_refDocumentType]'
GO

CREATE PROCEDURE [doc].[PR_Get_refDocumentType]
-- =============================================
-- Author:  Robert Francis
-- Create date:	04/07/2011
-- Description:	Retrieves table document's details
-- Notes:  
-- Usage:  exec doc.PR_Get_refDocumentType
-- Changelog: 04/07/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    
    SELECT  documentTypeID,
            documentType,
            isActive
    FROM    doc.refDocumentType
    ORDER BY documentType ;
END

GO

PRINT N'Creating [doc].[MapBkMrkData]'
GO
CREATE TABLE [doc].[MapBkMrkData] ( [MapBkMrkDataID] [int] NOT NULL
                                                           IDENTITY(1, 1),
                                    [DBName] [nvarchar](200) NOT NULL,
                                    [ViewPrefix] [nvarchar](30) NOT NULL,
                                    [DataSource] [nvarchar](60) NOT NULL,
                                    [BookMarkName] [nvarchar](200) NOT NULL,
                                    [BkMrkQry] [nvarchar](500) NOT NULL,
                                    [BkMrkNote] [nvarchar](500) NULL,
                                    [CreatedBy] [int] NOT NULL,
                                    [Created] [datetime] NOT NULL,
                                    [ModifiedBy] [int] NOT NULL,
                                    [Modified] [datetime] NOT NULL,
                                    [IsActive] [bit] NOT NULL )
GO

PRINT N'Creating primary key [PK_MapBkMrkData] on [doc].[MapBkMrkData]'
GO
ALTER TABLE [doc].[MapBkMrkData] ADD CONSTRAINT [PK_MapBkMrkData] PRIMARY KEY CLUSTERED  ([MapBkMrkDataID])
GO

PRINT N'Creating [doc].[PR_Get_MapBkMrkData]'
GO

CREATE PROCEDURE [doc].[PR_Get_MapBkMrkData]
-- =============================================
-- Author:  Robert Francis
-- Create date:	05/20/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Get_MapBkMrkData
-- Changelog: 05/20/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    SELECT  MapBkMrkDataID,
            DBName,
            ViewPrefix,
            DataSource,
            BookMarkName,
            BkMrkQry,
            BkMrkNote,
            CreatedBy,
            Created,
            ModifiedBy,
            Modified,
            IsActive
    FROM    doc.MapBkMrkData ;
END

GO

PRINT N'Creating [doc].[PR_Delete_MapBkMrkData]'
GO

CREATE PROCEDURE doc.PR_Delete_MapBkMrkData @MapBkMrkDataID INT
-- =============================================
-- Author:  Robert Francis
-- Create date:	05/20/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Delete_MapBkMrkData
-- Changelog: 05/20/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    
    UPDATE  doc.MapBkMrkData
    SET     IsActive = 0
    WHERE   MapBkMrkDataID = @Mapbkmrkdataid
END

GO

PRINT N'Creating [doc].[PR_Get_Template_refDocumentType]'
GO

CREATE PROCEDURE [doc].[PR_Get_Template_refDocumentType]
	
-- =============================================
-- Author:  Robert Francis
-- Create date:	04/07/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Get_Template_refDocumentType
-- Changelog: 04/07/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    
    SELECT  documentTypeID,
            documentType,
            isActive
    FROM    doc.refDocumentType
    WHERE   documentType = 'Template' ;
END

GO

PRINT N'Creating [doc].[PR_Get_Bookmark_By_BookmarkID]'
GO

CREATE PROCEDURE doc.PR_Get_Bookmark_By_BookmarkID ( @BookmarkID INT )
	-- =============================================
-- Author:  Robert Francis
-- Create date:	05/10/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Get_Bookmark_By_BookmarkID
-- Changelog: 05/10/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    SELECT  BkMrkDataID BookmarkID,
            dbName,
            viewPrefix,
            dataSource,
            BookmarkName,
            query,
            UseBy,
            createdBy,
            createDate,
            modifiedBy,
            modifiedDate,
            Note
    FROM    doc.BkMrkData
    WHERE   BkMrkDataID = @BookmarkID ;
END

GO

PRINT N'Creating [doc].[PR_Get_Default_refDocumentType]'
GO

CREATE PROCEDURE doc.PR_Get_Default_refDocumentType
-- =============================================
-- Author:  Robert Francis
-- Create date:	04/07/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Get_Default_refDocumentType
-- Changelog: 04/07/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    
    SELECT  documentTypeID,
            documentType,
            isActive
    FROM    doc.refDocumentType
    WHERE   documentType = 'Default' ;
END

GO

PRINT N'Creating [doc].[packetDocument]'
GO
CREATE TABLE [doc].[packetDocument] ( [packetID] [int] NOT NULL,
                                      [documentID] [int] NOT NULL,
                                      [isActive] [bit] NOT NULL )
GO

PRINT N'Creating primary key [PK_packetDocument] on [doc].[packetDocument]'
GO
ALTER TABLE [doc].[packetDocument] ADD CONSTRAINT [PK_packetDocument] PRIMARY KEY CLUSTERED  ([packetID], [documentID])
GO

PRINT N'Creating [doc].[PR_Set_packetDocument]'
GO

CREATE PROCEDURE doc.PR_Set_packetDocument ( @packetID INT,
                                             @documentID INT,
                                             @isActive BIT )
-- =============================================
-- Author:  Robert Francis
-- Create date:	07/21/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Set_packetDocument @packetID, @documentID, @isActive
-- Changelog: 07/21/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;     
    
    INSERT  INTO doc.packetDocument
            ( packetID, documentID, isActive )
    VALUES  ( @packetID, @documentID, @isActive ) ;
     
    IF @@ROWCOUNT <> 1 
        SELECT  1 ;
    ELSE 
        SELECT  0 ;
END


GO

PRINT N'Creating [doc].[documentVersion]'
GO
CREATE TABLE [doc].[documentVersion] ( [documentID] [int] NOT NULL,
                                       [versionedDocID] [int] NOT NULL )
GO

PRINT N'Creating primary key [PK_versioning] on [doc].[documentVersion]'
GO
ALTER TABLE [doc].[documentVersion] ADD CONSTRAINT [PK_versioning] PRIMARY KEY CLUSTERED  ([documentID] DESC, [versionedDocID] DESC)
GO

PRINT N'Creating [doc].[DocEntity]'
GO
CREATE TABLE [doc].[DocEntity] ( [DocEntityID] [int] NOT NULL
                                                     IDENTITY(1, 1),
                                 [documentID] [int] NOT NULL,
                                 [EntityID] [int] NOT NULL,
                                 [EntityType] [nvarchar](25) NOT NULL,
                                 [CreatedBy] [int] NOT NULL,
                                 [Created] [datetime] NOT NULL,
                                 [IsActive] [bit] NOT NULL )
GO

PRINT N'Creating primary key [PK_DocEntity] on [doc].[DocEntity]'
GO
ALTER TABLE [doc].[DocEntity] ADD CONSTRAINT [PK_DocEntity] PRIMARY KEY CLUSTERED  ([DocEntityID])
GO

PRINT N'Creating [doc].[fnParseList]'
GO

CREATE FUNCTION [doc].[fnParseList] ( @Delimiter VARCHAR(10),
                                      @Text VARCHAR(8000) )
RETURNS @Result TABLE ( RowID SMALLINT IDENTITY(1, 1)
                                       PRIMARY KEY,
                        Data VARCHAR(8000) )
-- =============================================
-- Author:  Jeff Kring
-- Create date:	02.24.2010
-- Description:	Parse a string on delimiter indicated
-- Usage:  select * from doc.fnParseList(',', 'abc,def,ghi')
-- Changelog:  02.24.2010, Created
-- =============================================
AS 
BEGIN

    DECLARE @NextPos SMALLINT,
        @LastPos SMALLINT

    SELECT  @NextPos = 0

    WHILE @NextPos <= DATALENGTH(@Text) 
        BEGIN
            SELECT  @LastPos = @NextPos,
                    @NextPos = CASE WHEN CHARINDEX(@Delimiter, @Text,
                                                   @LastPos + 1) = 0
                                    THEN DATALENGTH(@Text) + 1
                                    ELSE CHARINDEX(@Delimiter, @Text,
                                                   @LastPos + 1)
                               END

            INSERT  @Result
                    ( Data )
                    SELECT  SUBSTRING(@Text, @LastPos + 1,
                                      @NextPos - @LastPos - 1)
        END

    RETURN

END


GO

PRINT N'Creating [doc].[PR_Set_DocEntity]'
GO

CREATE PROCEDURE doc.PR_Set_DocEntity ( @DocEntityID INT,
                                        @documentID INT,
                                        @EntityID INT,
                                        @EntityType NVARCHAR(25),
                                        @CreatedBy INT,
                                        @Created DATETIME,
                                        @IsActive BIT,
                                        @ReturnValue INT OUTPUT )
-- =============================================
-- Author:  Robert Francis
-- Create date: 04/26/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Set_DocEntity
-- Changelog: 04/26/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
	
    MERGE doc.DocEntity AS target
        USING 
            ( SELECT    @DocEntityID,
                        @documentID,
                        @EntityID,
                        COALESCE(@EntityType, ''),
                        @CreatedBy,
                        @Created,
                        @IsActive ) AS source ( DocEntityID, documentID,
                                                EntityID, EntityType,
                                                CreatedBy, Created, IsActive )
        ON SOURCE.DocEntityID = TARGET.DocEntityID
        WHEN MATCHED 
            THEN UPDATE
               SET      documentID = source.documentID,
                        EntityID = source.EntityID,
                        EntityType = source.EntityType,
                        CreatedBy = source.CreatedBy,
                        Created = source.Created,
                        IsActive = source.IsActive
        WHEN NOT MATCHED 
            THEN INSERT ( documentID,
                          EntityID,
                          EntityType,
                          CreatedBy,
                          Created,
                          IsActive )
               VALUES   ( source.documentID,
                          source.EntityID,
                          source.EntityType,
                          source.CreatedBy,
                          source.Created,
                          source.IsActive ) ; 
		 
    IF @@ROWCOUNT = 1 
        SELECT  @ReturnValue = 1 ;
    ELSE 
        SELECT  @ReturnValue = 0 ;
END

GO

PRINT N'Creating [doc].[document]'
GO
CREATE TABLE [doc].[document] ( [documentID] [int] NOT NULL
                                                   IDENTITY(1, 1),
                                [documentFile] [nvarchar](MAX) NULL,
                                [documentPath] [nvarchar](1000)
                                    NOT NULL
                                    CONSTRAINT [DF_document_documentPath]
                                    DEFAULT ( '' ),
                                [documentName] [nvarchar](255)
                                    NOT NULL
                                    CONSTRAINT [DF_document_documentName]
                                    DEFAULT ( '' ),
                                [documentDate] [datetime] NULL,
                                [documentSize] [int] NULL,
                                [documentTypeID] [int] NULL,
                                [documentIsActive] [bit] NULL,
                                [createdBy] [int] NOT NULL,
                                [createDate] [datetime] NULL,
                                [documentTitle] [nvarchar](100) NULL,
                                [documentGroup] [nvarchar](32) NULL,
                                [publicFlag] [bit]
                                    NOT NULL
                                    CONSTRAINT [DF_document_publicFlag]
                                    DEFAULT ( (0) ),
                                [ActivityTypeID] [int] NULL,
                                [isActive] [bit]
                                    NOT NULL
                                    CONSTRAINT [DF_document_isActive]
                                    DEFAULT ( (1) ),
                                [documentFileExt] [nvarchar](10) NULL )
GO

PRINT N'Creating primary key [PK_document] on [doc].[document]'
GO
ALTER TABLE [doc].[document] ADD CONSTRAINT [PK_document] PRIMARY KEY CLUSTERED  ([documentID])
GO

PRINT N'Creating [doc].[PR_Set_Document]'
GO


CREATE PROCEDURE [doc].[PR_Set_Document] ( @documentID INT OUTPUT,
                                           @documentFile NVARCHAR(MAX),
                                           @documentPath NVARCHAR(1000),
                                           @documentName NVARCHAR(255),
                                           @documentDate DATETIME,
                                           @documentSize INT,
                                           @documentTypeID INT,
                                           @documentIsActive BIT,
                                           @createdBy INT,
                                           @createDate DATETIME,
                                           @documentTitle NVARCHAR(100),
                                           @documentGroup NVARCHAR(32),
                                           @publicFlag BIT,
                                           @ActivityTypeID INT,
                                           @isActive BIT )
-- =============================================
-- Author:  Robert Francis
-- Create date:	03.28.2011
-- Description:	Insert/Update Document record
-- Notes:  
-- Usage:  exec doc.document @documentID, @documentFile, @documentPath, @documentName, @documentDate, @documentSize, 
--									@documentTypeID, @documentIsActive, @createdBy, @createDate, @documentTitle, @documentGroup, 
--									@publicFlag, @ActivityTypeID, @isActive
-- Changelog: 03.28.2011, Created
--			  05.06.2011, TF-308 Changed to MERGE and added @pulbicFlag 
--			  08.23.2011, Added isActive and @ActivityTypeID to MERGE
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    DECLARE @MyCnt INT ;
	        
    DECLARE @retTable TABLE ( documentID INT )
	        
    MERGE doc.document AS target
        USING 
            ( SELECT    @documentID,
                        COALESCE(@documentFile, ''),
                        COALESCE(@documentPath, ''),
                        COALESCE(@documentName, ''),
                        @documentDate,
                        @documentSize,
                        @documentTypeID,
                        COALESCE(@isActive, 1),
                        @createdBy,
                        @createDate,
                        COALESCE(@documentTitle, ''),
                        COALESCE(@documentGroup, ''),
                        COALESCE(@publicFlag, 0),
                        @ActivityTypeID,
                        COALESCE(@isActive, 1) ) AS source ( documentID,
                                                             documentFile,
                                                             documentPath,
                                                             documentName,
                                                             documentDate,
                                                             documentSize,
                                                             documentTypeID,
                                                             documentIsActive,
                                                             createdBy,
                                                             createDate,
                                                             documentTitle,
                                                             documentGroup,
                                                             publicFlag,
                                                             ActivityTypeID,
                                                             isActive )
        ON ( target.documentID = source.documentID )
        WHEN MATCHED 
            THEN UPDATE
               SET      documentFile = source.documentFile,
                        documentPath = source.documentPath,
                        documentName = source.documentName,
                        documentDate = source.documentDate,
                        documentSize = source.documentSize,
                        documentTypeID = source.documentTypeID,
                        documentIsActive = source.documentIsActive,
                        createdBy = source.createdBy,
                        createDate = source.createDate,
                        documentTitle = source.documentTitle,
                        documentGroup = source.documentGroup,
                        publicFlag = source.publicFlag,
                        ActivityTypeID = source.ActivityTypeID,
                        isActive = source.isActive
        WHEN NOT MATCHED 
            THEN INSERT ( documentFile,
                          documentPath,
                          documentName,
                          documentDate,
                          documentSize,
                          documentTypeID,
                          documentIsActive,
                          createdBy,
                          createDate,
                          documentTitle,
                          documentGroup,
                          publicFlag,
                          ActivityTypeID,
                          isActive )
               VALUES   ( source.documentFile,
                          source.documentPath,
                          source.documentName,
                          source.documentDate,
                          source.documentSize,
                          source.documentTypeID,
                          source.documentIsActive,
                          source.createdBy,
                          source.createDate,
                          source.documentTitle,
                          source.documentGroup,
                          source.publicFlag,
                          source.ActivityTypeID,
                          source.isActive )
        OUTPUT
            INSERTED.documentID
            INTO @retTable ( documentID ) ; 
	
    SELECT  @MyCnt = @@ROWCOUNT ;
	
    IF @MyCnt = 1 
        BEGIN
            IF @documentID = 0 
                BEGIN
                    SELECT  @documentID = documentID
                    FROM    @retTable
                END					
        END
    ELSE 
        BEGIN
            SET @documentID = -1 			
        END 
	
END


GO

PRINT N'Creating [doc].[PR_Delete_Document]'
GO

CREATE PROCEDURE doc.PR_Delete_Document ( @documentID INT,
                                          @ReturnValue INT OUTPUT )
-- =============================================
-- Author:  Robert Francis
-- Create date:	03/28/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Delete_Document @documentID, @ReturnValue OUTPUT
-- Changelog: 03/28/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    
    UPDATE  doc.Document
    SET     IsActive = 0,
            documentIsActive = 0
    WHERE   documentID = @documentID ;
		 
    SELECT  @ReturnValue = @@ROWCOUNT ;
END

GO

PRINT N'Creating [doc].[MsgBroker]'
GO
CREATE TABLE [doc].[MsgBroker] ( [MsgBrokerID] [uniqueidentifier] NOT NULL
                                                              ROWGUIDCOL,
                                 [ReturnURL] [nvarchar](1000) NULL,
                                 [EntityList] [nvarchar](1000) NULL,
                                 [EntityID] [int] NULL,
                                 [EntityTypeID] [int] NULL,
                                 [UserID] [int] NOT NULL,
                                 [FeatureID] [int] NULL,
                                 [GroupID] [nvarchar](32) NULL,
                                 [CreateDateTime] [datetime] NOT NULL,
                                 [ActivityType] [int] NULL,
                                 [LogActivity] [bit] NULL,
                                 [isActive] [bit]
                                    NOT NULL
                                    CONSTRAINT [DF_MsgBroker_isActive]
                                    DEFAULT ( (1) ) )
GO

PRINT N'Creating primary key [PK_MsgBroker] on [doc].[MsgBroker]'
GO
ALTER TABLE [doc].[MsgBroker] ADD CONSTRAINT [PK_MsgBroker] PRIMARY KEY CLUSTERED  ([MsgBrokerID])
GO

PRINT N'Creating [doc].[PR_Set_MsgBroker]'
GO


CREATE PROCEDURE doc.PR_Set_MsgBroker ( @MsgBrokerID UNIQUEIDENTIFIER,
                                        @ReturnURL NVARCHAR(1000),
                                        @EntityList NVARCHAR(1000),
                                        @EntityID INT,
                                        @EntityTypeID INT,
                                        @UserID INT,
                                        @FeatureID INT,
                                        @GroupID NVARCHAR(32),
                                        @CreateDateTime DATETIME,
                                        @ActivityType INT,
                                        @LogActivity BIT )
-- =============================================
-- Author:  Robert Francis
-- Create date:	05/04/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Set_MsgBroker @MsgBrokerID, @ReturnURL, @EntityList, @EntityID, @EntityTypeID, @UserID,
--												@FeatureID, @GroupID, @CreateDateTime, @ActivityType, @LogActivity
-- Changelog: 05/04/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
	
    MERGE doc.MsgBroker AS target
        USING 
            ( SELECT    @MsgBrokerID,
                        COALESCE(@ReturnURL, ''),
                        COALESCE(@EntityList, ''),
                        @EntityID,
                        @EntityTypeID,
                        @UserID,
                        @FeatureID,
                        COALESCE(@GroupID, ''),
                        @CreateDateTime,
                        @ActivityType,
                        @LogActivity ) AS source ( MsgBrokerID, ReturnURL,
                                                   EntityList, EntityID,
                                                   EntityTypeID, UserID,
                                                   FeatureID, GroupID,
                                                   CreateDateTime,
                                                   ActivityType, LogActivity )
        ON SOURCE.MsgBrokerID = TARGET.MsgBrokerID
        WHEN MATCHED 
            THEN UPDATE
               SET      ReturnURL = source.ReturnURL,
                        EntityList = source.EntityList,
                        EntityID = source.EntityID,
                        EntityTypeID = source.EntityTypeID,
                        UserID = source.UserID,
                        FeatureID = source.FeatureID,
                        GroupID = source.GroupID,
                        CreateDateTime = source.CreateDateTime,
                        ActivityType = source.ActivityType,
                        LogActivity = source.LogActivity
        WHEN NOT MATCHED 
            THEN INSERT ( MsgBrokerID,
                          ReturnURL,
                          EntityList,
                          EntityID,
                          EntityTypeID,
                          UserID,
                          FeatureID,
                          GroupID,
                          CreateDateTime,
                          ActivityType,
                          LogActivity )
               VALUES   ( source.MsgBrokerID,
                          source.ReturnURL,
                          source.EntityList,
                          source.EntityID,
                          source.EntityTypeID,
                          source.UserID,
                          source.FeatureID,
                          source.GroupID,
                          source.CreateDateTime,
                          source.ActivityType,
                          source.LogActivity ) ; 
		 
    IF @@ROWCOUNT <> 1 
        SELECT  1 ;
    ELSE 
        SELECT  0 ;
END

GO

PRINT N'Creating [doc].[documentAttrValue]'
GO
CREATE TABLE [doc].[documentAttrValue] ( [documentAttrValueID] [int] NOT NULL
                                                              IDENTITY(1, 1),
                                         [documentID] [int] NOT NULL,
                                         [documentAttrID] [int] NOT NULL,
                                         [documentAttrValue] [nvarchar](MAX)
                                            COLLATE
                                            SQL_Latin1_General_CP1_CI_AS
                                            NULL,
                                         [isActive] [bit]
                                            NOT NULL
                                            CONSTRAINT [DF_documentAttrValue_isActive]
                                            DEFAULT ( (1) ) )
GO

PRINT N'Creating primary key [PK_documentAttrValue] on [doc].[documentAttrValue]'
GO
ALTER TABLE [doc].[documentAttrValue] ADD CONSTRAINT [PK_documentAttrValue] PRIMARY KEY CLUSTERED  ([documentAttrValueID])
GO

PRINT N'Creating [doc].[PR_Get_documentAttrValue_By_docID_AttrID]'
GO

CREATE PROCEDURE doc.PR_Get_documentAttrValue_By_docID_AttrID ( 
                                                              @documentID INT,
                                                              @documentAttrID INT )
-- =============================================
-- Author:  Robert Francis
-- Create date:	03/28/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Get_documentAttrValue_By_docID_AttrID @documentID, @documentAttrID
-- Changelog: 03/28/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    
    SELECT  documentAttrValueID,
            documentID,
            documentAttrID,
            documentAttrValue
    FROM    doc.documentAttrValue
    WHERE   documentID = @documentID
            AND documentAttrID = @documentAttrID ;
END

GO

PRINT N'Creating [doc].[PR_Set_documentAttrValue]'
GO


CREATE PROCEDURE doc.PR_Set_documentAttrValue ( @documentAttrValueID INT OUTPUT,
                                                @documentID INT,
                                                @documentAttrID INT,
                                                @documentAttrValue NVARCHAR(MAX) )
  -- =============================================
-- Author:  Robert Francis
-- Create date:	03/28/2011
-- Description:	Inserts or updates data in the documentAttrValue table.
-- Notes:  
-- Usage:  exec doc.PR_Set_documentAttrValue @documentAttrValueID output, @documentID, @documentAttrID, @documentAttrValue
-- Changelog: 03/28/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    DECLARE @MyCnt INT,
        @KeyVal INT ;
			
    MERGE doc.documentAttrValue AS target
        USING 
            ( SELECT    @documentAttrValueID,
                        @documentID,
                        @documentAttrID,
                        COALESCE(@documentAttrValue, '') ) AS source ( documentAttrValueID,
                                                              documentID,
                                                              documentAttrID,
                                                              documentAttrValue )
        ON SOURCE.documentAttrValueID = TARGET.documentAttrValueID
        WHEN MATCHED 
            THEN UPDATE
               SET      documentID = source.documentID,
                        documentAttrID = source.documentAttrID,
                        documentAttrValue = source.documentAttrValue
        WHEN NOT MATCHED 
            THEN INSERT ( documentID,
                          documentAttrID,
                          documentAttrValue )
               VALUES   ( source.documentID,
                          source.documentAttrID,
                          source.documentAttrValue ) ;  		
	
    SELECT  @MyCnt = @@ROWCOUNT,
            @KeyVal = SCOPE_IDENTITY() ;
	
    IF @MyCnt = 1 
        BEGIN
            IF @documentAttrValueID = 0 
                BEGIN
                    SET @documentAttrValueID = @KeyVal
                END					
        END
    ELSE 
        BEGIN
            SET @documentAttrValueID = -1 			
        END 
END

GO

PRINT N'Creating [doc].[PR_Get_document]'
GO

CREATE PROCEDURE doc.PR_Get_document
-- =============================================
-- Author:  Robert Francis
-- Create date:	03/28/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Get_document 
-- Changelog: 03/28/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    SELECT  documentID,
            documentFile,
            documentPath,
            documentName,
            documentDate,
            documentSize,
            documentTypeID,
            documentIsActive,
            createdBy,
            createDate,
            documentTitle,
            documentGroup
    FROM    doc.document ;
END

GO

PRINT N'Creating [doc].[PR_Get_document_By_DocumentID]'
GO

CREATE PROCEDURE doc.PR_Get_document_By_DocumentID ( @DocumentID INT )
-- =============================================
-- Author:  Robert Francis
-- Create date:	03/28/2011
-- Description:	Retrieves table document's details
-- Notes:  
-- Usage:  exec doc.PR_Get_document_By_DocumentID @DocumentID
-- Changelog: 03/28/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    SELECT  documentID,
            documentFile,
            documentPath,
            documentName,
            documentDate,
            documentSize,
            documentTypeID,
            documentIsActive,
            createdBy,
            createDate,
            documentTitle,
            documentGroup
    FROM    doc.document
    WHERE   documentID = @documentID ;
END

GO

PRINT N'Creating [doc].[documentAttr]'
GO
CREATE TABLE [doc].[documentAttr] ( [documentAttrID] [int] NOT NULL
                                                           IDENTITY(1, 1),
                                    [documentAttribute] [nvarchar](250)
                                        NOT NULL,
                                    [isActive] [bit]
                                        NOT NULL
                                        CONSTRAINT [DF_documentAttr_isActive]
                                        DEFAULT ( (1) ) )
GO

PRINT N'Creating primary key [PK_documentAttr] on [doc].[documentAttr]'
GO
ALTER TABLE [doc].[documentAttr] ADD CONSTRAINT [PK_documentAttr] PRIMARY KEY CLUSTERED  ([documentAttrID])
GO

PRINT N'Creating [doc].[PR_Get_documentAttr_By_Name]'
GO

CREATE PROCEDURE doc.PR_Get_documentAttr_By_Name ( @documentAttribute NVARCHAR(250) )
-- =============================================
-- Author:  Robert Francis
-- Create date:	04/18/2011
-- Description:	Retrieves table document's details
-- Notes:  
-- Usage:  exec doc.PR_Get_documentAttr_By_Name @documentAttribute
-- Changelog: 04/18/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    
    SELECT  documentAttrID,
            documentAttribute
    FROM    doc.documentAttr
    WHERE   documentAttribute = @documentAttribute ;
END

GO

PRINT N'Creating [doc].[PR_Get_MsgBroker_By_MsgBrokerID]'
GO

CREATE PROCEDURE [doc].[PR_Get_MsgBroker_By_MsgBrokerID] ( @MsgBrokerID UNIQUEIDENTIFIER )
-- =============================================
-- Author:  Robert Francis
-- Create date:	04/19/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Get_MsgBroker_By_MsgBrokerID @MsgBrokerID
-- Changelog: 04/19/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    
    SELECT  MsgBrokerID,
            EntityList,
            EntityID,
            EntityTypeID,
            UserID,
            FeatureID,
            GroupID,
            CreateDateTime,
            ActivityType,
            LogActivity,
            ReturnURL
    FROM    doc.MsgBroker
    WHERE   MsgBrokerID = @MsgBrokerID ;
END

GO

PRINT N'Creating [doc].[PR_Get_DocumentID_By_ActivityType]'
GO

CREATE PROCEDURE doc.PR_Get_DocumentID_By_ActivityType ( @ActivityType INT )
-- =============================================
-- Author:  Robert Francis
-- Create date:	03/28/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Get_Bookmark_By_BookmarkID
-- Changelog: 03/28/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    
    SELECT  documentID
    FROM    doc.document AS doc
    WHERE   doc.activityTypeID = @ActivityType ;
END

GO

PRINT N'Creating [doc].[documentCheckOut]'
GO
CREATE TABLE [doc].[documentCheckOut] ( [documentCheckOutID] [int] NOT NULL
                                                              IDENTITY(1, 1),
                                        [documentID] [int] NOT NULL,
                                        [checkedOut] [bit] NULL,
                                        [checkedOutBy] [int] NOT NULL,
                                        [checkedOutDate] [datetime] NULL,
                                        [checkedInDate] [datetime] NULL,
                                        [isActive] [bit]
                                            NOT NULL
                                            CONSTRAINT [DF_documentCheckOut_isActive]
                                            DEFAULT ( (1) ) )
GO

PRINT N'Creating primary key [PK_documentCheckOut] on [doc].[documentCheckOut]'
GO
ALTER TABLE [doc].[documentCheckOut] ADD CONSTRAINT [PK_documentCheckOut] PRIMARY KEY CLUSTERED  ([documentCheckOutID])
GO

PRINT N'Creating [doc].[PR_IS_Document_Checked_Out]'
GO

CREATE PROCEDURE doc.PR_IS_Document_Checked_Out
    @documentID INT,
    @UserID INT
-- =============================================
-- Author:  Robert Francis
-- Create date:	07/08/2011
-- Description:	Retrieves table documentCheckOut's details
-- Notes:  
-- Usage:  exec doc.PR_Is_Document_Checked_Out @documentID, @UserID
-- Changelog: 07/08/2011, Created 
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    IF @UserID > 0 
        BEGIN
            SELECT  COUNT(*)
            FROM    doc.documentCheckOut
            WHERE   documentID = @documentID
                    AND checkedOutBy = @UserID
                    AND isActive = 1 ;
        END
    ELSE 
        BEGIN
            SELECT  COUNT(*)
            FROM    doc.documentCheckOut
            WHERE   documentID = @documentID
                    AND isActive = 1 ;
        END
END

GO

PRINT N'Creating [doc].[PR_Set_DocumentCheckOut]'
GO

CREATE PROCEDURE doc.PR_Set_DocumentCheckOut ( @documentCheckOutID INT,
                                               @documentID INT,
                                               @checkedOut BIT,
                                               @checkedOutBy INT,
                                               @checkedOutDate DATETIME,
                                               @checkedInDate DATETIME,
                                               @isActive BIT )
-- =============================================
-- Author:  Robert Francis
-- Create date:	07/08/2011
-- Description:	Inserts or updates data in the documentCheckOut table.
-- Notes:  
-- Usage:  exec doc.PR_Set_DocumentCheckOut @documentCheckOutID, @documentID, @checkedOut, @checkedOutBy, @checkedOutDate, @checkedInDate, @isActive
-- Changelog: 07/08/2011, Created 
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
	
    MERGE doc.documentCheckOut AS target
        USING 
            ( SELECT    @documentCheckOutID,
                        @documentID,
                        @checkedOut,
                        @checkedOutBy,
                        @checkedOutDate,
                        @checkedInDate,
                        COALESCE(@isActive, 1) ) AS source ( documentCheckOutID,
                                                             documentID,
                                                             checkedOut,
                                                             checkedOutBy,
                                                             checkedOutDate,
                                                             checkedInDate,
                                                             isActive )
        ON SOURCE.documentCheckOutID = TARGET.documentCheckOutID
        WHEN MATCHED 
            THEN UPDATE
               SET      documentID = source.documentID,
                        checkedOut = source.checkedOut,
                        checkedOutBy = source.checkedOutBy,
                        checkedOutDate = source.checkedOutDate,
                        checkedInDate = source.checkedInDate,
                        isActive = source.isActive
        WHEN NOT MATCHED 
            THEN INSERT ( documentID,
                          checkedOut,
                          checkedOutBy,
                          checkedOutDate,
                          checkedInDate,
                          isActive )
               VALUES   ( source.documentID,
                          source.checkedOut,
                          source.checkedOutBy,
                          source.checkedOutDate,
                          source.checkedInDate,
                          source.isActive ) ;  	
	
    IF @@ROWCOUNT = 1 
        SELECT  1 ;
    ELSE 
        SELECT  0 ;
END


GO

PRINT N'Creating [doc].[PR_Undo_DocumentCheckOut]'
GO

CREATE PROCEDURE doc.PR_Undo_DocumentCheckOut ( @documentID INT,
                                                @checkedOutBy INT )
-- =============================================
-- Author:  Robert Francis
-- Create date:	07/08/2011
-- Description:	Inserts or updates data in the documentCheckOut table.
-- Notes:  
-- Usage:  exec doc.PR_Undo_DocumentCheckOut @documentID, @checkedOutBy
-- Changelog: 07/08/2011, Created 
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    UPDATE  doc.documentCheckOut
    SET     isActive = 0
    WHERE   documentID = @documentID
            AND checkedOutBy = @checkedOutBy
            AND isActive = 1 ;
		 
    IF @@ROWCOUNT = 1 
        SELECT  1 ;
    ELSE 
        SELECT  0 ;
END

GO

PRINT N'Creating [doc].[PR_Version_Document]'
GO


CREATE PROCEDURE doc.PR_Version_Document ( @documentID INT )
-- =============================================
-- Author:  Robert Francis
-- Create date:	07/12/2011
-- Description:	Creates a new version of the given document and associates the versions.
-- Notes:  
-- Usage:  exec doc.PR_Version_Document @documentID
-- Changelog: 07/12/2011, Created 
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
	
    DECLARE @VersionID INT ;
	
    INSERT  INTO doc.document
            ( documentFile,
              documentPath,
              documentName,
              documentDate,
              documentSize,
              documentTypeID,
              documentIsActive,
              createdBy,
              createDate,
              documentTitle,
              documentGroup,
              publicFlag,
              ActivityTypeID,
              isActive )
            SELECT  documentFile,
                    documentPath,
                    documentName,
                    documentDate,
                    documentSize,
                    documentTypeID,
                    documentIsActive,
                    createdBy,
                    createDate,
                    documentTitle,
                    documentGroup,
                    publicFlag,
                    ActivityTypeID,
                    0
            FROM    doc.document
            WHERE   documentID = @documentID ;
    
    SET @VersionID = SCOPE_IDENTITY() ;
    
    INSERT  INTO doc.documentVersion
            ( documentID, versionedDocID )
    VALUES  ( @documentID, @VersionID ) ;
				 
    IF @@ROWCOUNT = 1 
        RETURN 1 ;
    ELSE 
        RETURN 0 ;		
END

GO

PRINT N'Creating [doc].[PR_Get_documentAttrValue_By_AttrID_AttrValue]'
GO

CREATE PROCEDURE doc.PR_Get_documentAttrValue_By_AttrID_AttrValue ( 
                                                              @documentAttribute NVARCHAR(250),
                                                              @documentAttrValue NVARCHAR(MAX) )
-- =============================================
-- Author:  Robert Francis
-- Create date:	07/18/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Get_documentAttrValue_By_AttrID_AttrValue @documentAttribute, @documentAttrValue
-- Changelog: 07/18/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    
    SELECT  documentID
    FROM    doc.documentAttrValue AS dav
    INNER JOIN doc.documentAttr AS da
            ON da.documentAttrID = dav.documentAttrID
    WHERE   da.documentAttribute = @documentAttribute
            AND dav.documentAttrValue LIKE @documentAttrValue
            AND dav.isActive = 1 ;
END


GO

PRINT N'Creating [doc].[PR_Get_documentAttrValue_By_documentID]'
GO

CREATE PROCEDURE [doc].[PR_Get_documentAttrValue_By_documentID] ( @documentID INT )
-- =============================================
-- Author:  Robert Francis
-- Create date:	03/28/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Get_documentAttrValue_By_documentID @documentID
-- Changelog: 03/28/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    
    SELECT  dav.documentAttrValueID,
            dav.documentID,
            dav.documentAttrID,
            dav.documentAttrValue,
            da.documentAttribute,
            doc.documentPath
    FROM    doc.documentAttrValue AS dav
    INNER JOIN doc.documentAttr AS da
            ON dav.documentAttrID = da.documentAttrID
    INNER JOIN doc.document AS doc
            ON doc.documentID = dav.documentID
    WHERE   dav.documentID = @documentID
            AND dav.isActive = 1
            AND doc.isActive = 1
            AND da.isActive = 1 ;
END

GO

PRINT N'Creating [doc].[PR_Delete_documentAttrValue_By_DocumentID]'
GO

CREATE PROCEDURE [doc].[PR_Delete_documentAttrValue_By_DocumentID] @documentID INT
-- =============================================
-- Author:  Robert Francis
-- Create date:	07/18/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Delete_documentAttrValue_By_DocumentID @documentID, @ReturnValue OUTPUT
-- Changelog: 07/18/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    
    DELETE  doc.documentAttrValue
    WHERE   documentID = @documentID ;
END

GO

PRINT N'Creating [doc].[PR_Delete_documentAttrValue_By_docID_AttrID]'
GO

CREATE PROCEDURE doc.PR_Delete_documentAttrValue_By_docID_AttrID ( 
                                                              @documentID INT,
                                                              @documentAttrID INT )
-- =============================================
-- Author:  Robert Francis
-- Create date:	03/28/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Delete_documentAttrValue_By_docID_AttrID @documentID, @documentAttrID
-- Changelog: 03/28/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    DELETE  doc.documentAttrValue
    WHERE   documentID = @documentID
            AND documentAttrID = @documentAttrID ;    
END

GO

PRINT N'Creating [doc].[PR_Get_Documents_For_Entity]'
GO

CREATE PROCEDURE [doc].[PR_Get_Documents_For_Entity] ( @Entity_Type VARCHAR(25),
                                                       @Entity_ID INTEGER,
                                                       @DateSort VARCHAR(4) = NULL )
-- =============================================
-- Author:  Robert Francis
-- Create date:	11.03.2010
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Get_Documents_For_Entity @Entity_Type, @Entity_ID, @DateSort
-- Changelog: 10.29.2010, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    DECLARE @DocID INTEGER,
        @Attributes VARCHAR(MAX),
        @SingAttr NVARCHAR(MAX) ;
            
    DECLARE @DocTable TABLE ( documentID INT,
                              Title NVARCHAR(512),
                              [FileName] NVARCHAR(255),
                              [SFileName] NVARCHAR(255),
                              [Type] NVARCHAR(512),
                              Keywords NVARCHAR(3000),
                              [Date] DATE,
                              [DocEntityID] INT )
    INSERT  INTO @DocTable
            ( documentID,
              [Title],
              [FileName],
              [SFileName],
              [Type],
              [Keywords],
              [Date],
              [DocEntityID] )
            SELECT  Doc.documentID,
                    doc.documentName,
                    Doc.documentFile,
                    doc.documentFile,
                    Doc.documentTypeID,
                    ' ',
                    Doc.createDate,
                    DocEntityID
            FROM    doc.DocEntity AS AHL
            JOIN    doc.document AS Doc
                    ON AHL.documentID = Doc.documentID
            WHERE   ahl.EntityType = @Entity_Type
                    AND ahl.EntityID = @Entity_ID
                    AND doc.documentIsActive = 1 ;
    
    DECLARE DocCursor CURSOR LOCAL FAST_FORWARD
    FOR
    SELECT  documentID
    FROM    @DocTable ;           
        
    OPEN DocCursor
        
    FETCH NEXT FROM DocCursor INTO @DocID

    WHILE @@FETCH_STATUS = 0 
        BEGIN			
            DECLARE ResultsCursor CURSOR LOCAL FAST_FORWARD
            FOR
            SELECT  LTRIM(RTRIM(documentAttrValue))
            FROM    MSSLMS.doc.documentAttrValue AS dav
            LEFT JOIN MSSLMS.doc.documentAttr AS da
                    ON dav.documentAttrID = da.documentAttrID
            WHERE   dav.documentID = @DocID
                    AND da.documentAttribute = 'KEYWORD'
		        
            OPEN ResultsCursor
		        
            FETCH NEXT FROM ResultsCursor INTO @SingAttr

            WHILE @@FETCH_STATUS = 0 
                BEGIN
                    IF @Attributes IS NULL 
                        SELECT  @Attributes = @SingAttr
                    ELSE 
                        SELECT  @Attributes = COALESCE(@Attributes, '') + ', '
                                + @SingAttr
				
                    FETCH NEXT FROM ResultsCursor INTO @SingAttr
                END
		        
            CLOSE ResultsCursor ;
            DEALLOCATE ResultsCursor ;

            UPDATE  @DocTable
            SET     [Keywords] = @Attributes
            WHERE   documentID = @DocID ;			
            SET @Attributes = NULL
            FETCH NEXT FROM DocCursor INTO @DocID
        END
        
    CLOSE DocCursor ;
    DEALLOCATE DocCursor ;   
      
    IF UPPER(@DateSort) = 'ASC' 
        BEGIN	
            SELECT  *
            FROM    @DocTable
            ORDER BY Date ASC ;	 
        END
    ELSE 
        BEGIN
            SELECT  *
            FROM    @DocTable
            ORDER BY Date DESC ;
        END
END
GO

PRINT N'Creating [doc].[Search]'
GO
CREATE TABLE [doc].[Search] ( [SearchID] [int] NOT NULL
                                               IDENTITY(1, 1),
                              [tblName] [nvarchar](50) NOT NULL,
                              [colName] [nvarchar](50) NOT NULL,
                              [colAlias] [nvarchar](50) NOT NULL,
                              [isActive] [bit]
                                NOT NULL
                                CONSTRAINT [DF_Search_isActive]
                                DEFAULT ( (1) ) )
GO

PRINT N'Creating primary key [PK_Search] on [doc].[Search]'
GO
ALTER TABLE [doc].[Search] ADD CONSTRAINT [PK_Search] PRIMARY KEY CLUSTERED  ([SearchID])
GO

PRINT N'Creating [doc].[PR_Get_Search]'
GO

CREATE PROCEDURE [doc].[PR_Get_Search]
-- =============================================
-- Author:  Robert Francis
-- Create date:	06/29/2011
-- Description:	Retrieves table Search's details
-- Notes:  
-- Usage:  exec doc.PR_Version_Document @documentID
-- Changelog: 06/29/2011, Created 
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    SELECT  SearchID,
            tblName,
            colName,
            colAlias
    FROM    doc.Search
    WHERE   isActive = 1 ;
END

GO

PRINT N'Creating [doc].[packet]'
GO
CREATE TABLE [doc].[packet] ( [packetID] [int] NOT NULL
                                               IDENTITY(1, 1),
                              [packetTypeID] [int] NULL,
                              [packetIsActive] [bit] NULL,
                              [createdBy] [int] NOT NULL,
                              [createDate] [datetime] NULL,
                              [packetTitle] [nvarchar](100) NULL,
                              [isActive] [bit]
                                NULL
                                CONSTRAINT [DF_packet_isActive]
                                DEFAULT ( (1) ) )
GO

PRINT N'Creating primary key [PK_packet] on [doc].[packet]'
GO
ALTER TABLE [doc].[packet] ADD CONSTRAINT [PK_packet] PRIMARY KEY CLUSTERED  ([packetID])
GO

PRINT N'Creating [doc].[PR_Set_packet]'
GO

CREATE PROCEDURE doc.PR_Set_packet ( @packetID INT OUTPUT,
                                     @packetTypeID INT,
                                     @packetIsActive BIT,
                                     @createdBy INT,
                                     @createDate DATETIME,
                                     @packetTitle NVARCHAR(100),
                                     @isActive BIT )
-- =============================================
-- Author:  Robert Francis
-- Create date:	07/21/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Set_packet @packetID INT OUTPUT, @packetTypeID, @packetIsActive, @createdBy, @createDate,
--											@packetTitle, @isActive 
-- Changelog: 07/21/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    
    DECLARE @MyCnt INT ;
    
    DECLARE @retTable TABLE ( packetID INT )
    
    MERGE doc.packet AS target
        USING 
            ( SELECT    @packetID,
                        @packetTypeID,
                        @packetIsActive,
                        @createdBy,
                        @createDate,
                        COALESCE(@packetTitle, ''),
                        COALESCE(@isActive, 1) ) AS source ( packetID,
                                                             packetTypeID,
                                                             packetIsActive,
                                                             createdBy,
                                                             createDate,
                                                             packetTitle,
                                                             isActive )
        ON SOURCE.packetID = TARGET.packetID
        WHEN MATCHED 
            THEN UPDATE
               SET      packetTypeID = source.packetTypeID,
                        packetIsActive = source.packetIsActive,
                        createdBy = source.createdBy,
                        createDate = source.createDate,
                        packetTitle = source.packetTitle,
                        isActive = source.isActive
        WHEN NOT MATCHED 
            THEN INSERT ( packetTypeID,
                          packetIsActive,
                          createdBy,
                          createDate,
                          packetTitle,
                          isActive )
               VALUES   ( source.packetTypeID,
                          source.packetIsActive,
                          source.createdBy,
                          source.createDate,
                          source.packetTitle,
                          source.isActive )
        OUTPUT
            INSERTED.packetID
            INTO @retTable ( packetID ) ; ;  
	
    SELECT  @MyCnt = @@ROWCOUNT,
            @packetID = packetID
    FROM    @retTable
	
    RETURN @MyCnt ;	 
END


GO

PRINT N'Creating [doc].[PR_Get_packet_By_packetID]'
GO

CREATE PROCEDURE doc.PR_Get_packet_By_packetID ( @packetID INT )
-- =============================================
-- Author:  Robert Francis
-- Create date:	07/21/2011
-- Description:	Retrieves table document's details
-- Notes:  
-- Usage:  exec doc.PR_Get_packet_By_packetID @packetID
-- Changelog: 07/21/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    
    SELECT  packetID,
            packetTypeID,
            packetIsActive,
            createdBy,
            createDate,
            packetTitle,
            isActive
    FROM    doc.packet
    WHERE   packetID = @packetID ;
END


GO

PRINT N'Creating [doc].[packetAttrValue]'
GO
CREATE TABLE [doc].[packetAttrValue] ( [packetAttrValueID] [int] NOT NULL
                                                              IDENTITY(1, 1),
                                       [packetID] [int] NOT NULL,
                                       [packetAttrID] [int] NOT NULL,
                                       [packetAttrValue] [nvarchar](MAX) NULL,
                                       [isActive] [bit]
                                        NOT NULL
                                        CONSTRAINT [DF_packetAttrValue_isActive]
                                        DEFAULT ( (1) ) )
GO

PRINT N'Creating primary key [PK_packetAttrValue] on [doc].[packetAttrValue]'
GO
ALTER TABLE [doc].[packetAttrValue] ADD CONSTRAINT [PK_packetAttrValue] PRIMARY KEY CLUSTERED  ([packetAttrValueID])
GO

PRINT N'Creating [doc].[PR_Delete_packetAttrValue_By_packetID]'
GO

CREATE PROCEDURE [doc].[PR_Delete_packetAttrValue_By_packetID] @packetID INT
-- =============================================
-- Author:  Robert Francis
-- Create date:	03/28/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Delete_packetAttrValue_By_packetID @packetID
-- Changelog: 03/28/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    
    DELETE  doc.packetAttrValue
    WHERE   packetID = @packetID ;
END

GO

PRINT N'Creating [doc].[PR_Delete_packetAttrValue_By_docID_AttrID]'
GO

CREATE PROCEDURE [doc].[PR_Delete_packetAttrValue_By_docID_AttrID] ( 
                                                              @packetID INT,
                                                              @packetAttrID INT )
-- =============================================
-- Author:  Robert Francis
-- Create date:	03/28/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Delete_packetAttrValue_By_docID_AttrID @packetID, @packetAttrID
-- Changelog: 03/28/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    
    DELETE  doc.packetAttrValue
    WHERE   packetID = @packetID
            AND packetAttrID = @packetAttrID ;    
END

GO

PRINT N'Creating [doc].[packetAttr]'
GO
CREATE TABLE [doc].[packetAttr] ( [packetAttrID] [int] NOT NULL
                                                       IDENTITY(1, 1),
                                  [packetAttribute] [nvarchar](250) NOT NULL,
                                  [isActive] [bit]
                                    NOT NULL
                                    CONSTRAINT [DF_packetAttr_isActive]
                                    DEFAULT ( (1) ) )
GO

PRINT N'Creating primary key [PK_packetAttr] on [doc].[packetAttr]'
GO
ALTER TABLE [doc].[packetAttr] ADD CONSTRAINT [PK_packetAttr] PRIMARY KEY CLUSTERED  ([packetAttrID])
GO

PRINT N'Creating [doc].[PR_Set_packetAttrValue]'
GO


CREATE PROCEDURE doc.PR_Set_packetAttrValue ( @packetAttrValueID INT OUTPUT,
                                              @packetID INT,
                                              @packetAttrID INT,
                                              @packetAttrValue NVARCHAR(MAX) )
  -- =============================================
-- Author:  Robert Francis
-- Create date:	03/28/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Set_packetAttrValue @packetAttrValueID output, @packetID, @packetAttrID, @packetAttrValue
-- Changelog: 03/28/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    DECLARE @MyCnt INT,
        @KeyVal INT ;
			
    MERGE doc.packetAttrValue AS target
        USING 
            ( SELECT    @packetAttrValueID,
                        @packetID,
                        @packetAttrID,
                        COALESCE(@packetAttrValue, '') ) AS source ( packetAttrValueID,
                                                              packetID,
                                                              packetAttrID,
                                                              packetAttrValue )
        ON SOURCE.packetAttrValueID = TARGET.packetAttrValueID
        WHEN MATCHED 
            THEN UPDATE
               SET      packetID = source.packetID,
                        packetAttrID = source.packetAttrID,
                        packetAttrValue = source.packetAttrValue
        WHEN NOT MATCHED 
            THEN INSERT ( packetID,
                          packetAttrID,
                          packetAttrValue )
               VALUES   ( source.packetID,
                          source.packetAttrID,
                          source.packetAttrValue ) ;  		
	
    SELECT  @MyCnt = @@ROWCOUNT,
            @KeyVal = SCOPE_IDENTITY() ;
	
    IF @MyCnt = 1 
        BEGIN
            IF @packetAttrValueID = 0 
                BEGIN
                    SET @packetAttrValueID = @KeyVal
                END					
        END
    ELSE 
        BEGIN
            SET @packetAttrValueID = -1 			
        END 
END

GO

PRINT N'Creating [doc].[PR_Get_documents_By_packetID]'
GO

CREATE PROCEDURE doc.PR_Get_documents_By_packetID ( @packetID INT )
-- =============================================
-- Author:  Robert Francis
-- Create date:	03/28/2011
-- Description:	Retrieves table document's details
-- Notes:  
-- Usage:  exec doc.PR_Get_documents_By_packetID @packetID
-- Changelog: 03/28/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    SELECT  d.documentID,
            0 AS rowCnt,
            d.documentFile,
            d.documentPath,
            d.documentName,
            CONVERT(VARCHAR, d.documentDate, 110) AS documentDate,
            d.documentSize,
            d.documentTypeID,
            rtyp.documentType,
            d.documentIsActive,
            d.createdBy,
            d.createDate,
            d.documentTitle,
            d.documentGroup,
            ' ' AS keywords
    FROM    doc.document AS d
    INNER JOIN doc.packetDocument AS pd
            ON pd.documentID = d.documentid
    LEFT JOIN doc.refDocumentType AS rtyp
            ON rtyp.documentTypeID = d.documentTypeID
    WHERE   pd.packetID = @packetID
            AND d.isActive = 1
            AND pd.isActive = 1 ;
END

GO

PRINT N'Creating [doc].[PR_Get_document_By_DocList]'
GO

CREATE PROCEDURE [doc].[PR_Get_document_By_DocList] ( @DocList VARCHAR(250) )
-- =============================================
-- Author:  Robert Francis
-- Create date:	03/28/2011
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Get_document_By_DocList @DocList
-- Changelog: 03/28/2011, Created
--			  08/10/2011, SLMS-905, Reworked SP to consolidated logic to remove comma error
--			  08/10/2011, SLMS-899, Sorted results by docCreateDate (most recent first)
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON 
    
    SELECT  DISTINCT
            d.documentID docID,
            d.documentFile docFile,
            d.documentPath docFilePath,
            d.documentName docName,
            d.documentTitle docTitle,
            d.documentTypeID docTypeID,
            dtyp.documentType docType,
            d.documentDate docDate,
            d.documentSize docSize,
            d.createdBy docCreatedBy,
            d.createDate docCreateDate,
            msslms.doc.FN_documentKeywords(d.documentID) docKeywords,
            d.documentGroup docGroup,
            atr.documentAttrID docAttrID,
            atr.documentAttribute docAttr
    FROM    doc.fnParseList(',', @DocList) fpl
    INNER JOIN doc.document AS d
            ON d.documentID = CAST(fpl.Data AS INT)
    LEFT JOIN doc.refDocumentType AS dtyp
            ON d.documentTypeID = dtyp.documentTypeID
    LEFT JOIN doc.documentAttrValue AS atrVal
            ON atrVal.documentID = d.documentid
    LEFT JOIN doc.documentAttr AS atr
            ON atr.documentAttrID = atrVal.documentAttrID
               AND atr.documentAttribute LIKE '%Template-%'
    WHERE   d.IsActive = 1
    ORDER BY docCreateDate DESC 

END

GO

PRINT N'Creating [doc].[PR_Get_Documents_By_EntityList]'
GO

CREATE PROCEDURE [doc].[PR_Get_Documents_By_EntityList] ( @Entity_Type VARCHAR(25),
                                                          @EntityList VARCHAR(400),
                                                          @DateSort VARCHAR(4) = NULL )
-- =============================================
-- Author:  Robert Francis
-- Create date:	11.03.2010
-- Description:	
-- Notes:  
-- Usage:  exec doc.PR_Get_Documents_By_EntityList @Entity_Type, @Entity_ID, @DateSort
-- Changelog: 10.29.2010, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    DECLARE @DocID INTEGER,
        @Attributes VARCHAR(MAX),
        @SingAttr NVARCHAR(MAX),
        @Pos INT,
        @tmpEntityID INT ;            
            
    DECLARE @DocTable TABLE ( documentID INT,
                              Title NVARCHAR(512),
                              [FileName] NVARCHAR(255),
                              [SFileName] NVARCHAR(255),
                              [Type] NVARCHAR(512),
                              Keywords NVARCHAR(3000),
                              [Date] DATE,
                              [DocEntityID] INT )
                             
    DECLARE @TempList TABLE ( tmpEntityID INT ) ;                          
      
    SET @EntityList = LTRIM(RTRIM(@EntityList)) + ',' ;
    SET @Pos = CHARINDEX(',', @EntityList, 1) ;
	
    IF REPLACE(@EntityList, ',', '') <> '' 
        BEGIN
            WHILE @Pos > 0 
                BEGIN
                    SET @tmpEntityID = LTRIM(RTRIM(LEFT(@EntityList, @Pos - 1)))
                    IF @tmpEntityID <> '' 
                        BEGIN
                            INSERT  INTO @TempList
                                    ( tmpEntityID )
                            VALUES  ( CAST(@tmpEntityID AS INT) ) --Use Appropriate conversion
                        END
                    SET @EntityList = RIGHT(@EntityList,
                                            LEN(@EntityList) - @Pos)
                    SET @Pos = CHARINDEX(',', @EntityList, 1)

                END
        END                      
                             
    INSERT  INTO @DocTable
            ( documentID,
              [Title],
              [FileName],
              [SFileName],
              [Type],
              [Keywords],
              [Date],
              [DocEntityID] )
            SELECT  Doc.documentID,
                    doc.documentTitle,
                    Doc.documentName,
                    doc.documentName,
                    Doc.documentTypeID,
                    ' ',
                    Doc.createDate,
                    DocEntityID
            FROM    doc.DocEntity AS AHL
            JOIN    doc.document AS Doc
                    ON AHL.documentID = Doc.documentID
            WHERE   ahl.EntityType = @Entity_Type
                    AND doc.documentIsActive = 1
                    AND ahl.EntityID IN ( SELECT    tmpEntityID
                                          FROM      @TempList )
                    
    
    DECLARE DocCursor CURSOR LOCAL FAST_FORWARD
    FOR
    SELECT  documentID
    FROM    @DocTable ;           
        
    OPEN DocCursor
        
    FETCH NEXT FROM DocCursor INTO @DocID

    WHILE @@FETCH_STATUS = 0 
        BEGIN			
            DECLARE ResultsCursor CURSOR LOCAL FAST_FORWARD
            FOR
            SELECT  LTRIM(RTRIM(documentAttrValue))
            FROM    doc.documentAttrValue AS dav
            LEFT JOIN doc.documentAttr AS da
                    ON dav.documentAttrID = da.documentAttrID
            WHERE   dav.documentID = @DocID
                    AND da.documentAttribute = 'KEYWORD'
		        
            OPEN ResultsCursor
		        
            FETCH NEXT FROM ResultsCursor INTO @SingAttr

            WHILE @@FETCH_STATUS = 0 
                BEGIN
                    IF @Attributes IS NULL 
                        SELECT  @Attributes = @SingAttr
                    ELSE 
                        SELECT  @Attributes = COALESCE(@Attributes, '') + ', '
                                + @SingAttr
				
                    FETCH NEXT FROM ResultsCursor INTO @SingAttr
                END
		        
            CLOSE ResultsCursor ;
            DEALLOCATE ResultsCursor ;

            UPDATE  @DocTable
            SET     [Keywords] = @Attributes
            WHERE   documentID = @DocID ;			
            SET @Attributes = NULL
            FETCH NEXT FROM DocCursor INTO @DocID
        END
        
    CLOSE DocCursor ;
    DEALLOCATE DocCursor ;   
      
    IF UPPER(@DateSort) = 'ASC' 
        BEGIN	
            SELECT  *
            FROM    @DocTable
            ORDER BY Date ASC ;	 
        END
    ELSE 
        BEGIN
            SELECT  *
            FROM    @DocTable
            ORDER BY Date DESC ;
        END
END
GO

PRINT N'Creating [doc].[PR_Get_packetAttrValue_By_packetID]'
GO

CREATE PROCEDURE doc.PR_Get_packetAttrValue_By_packetID ( @packetID INT )
-- =============================================
-- Author:  Robert Francis
-- Create date:	07/21/2011
-- Description:	Retrieves table document's details
-- Notes:  
-- Usage:  exec doc.PR_Get_packetAttrValue_By_packetID @packetID
-- Changelog: 07/21/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    
    SELECT  pav.packetAttrValueID,
            pav.packetID,
            pav.packetAttrID,
            pav.packetAttrValue,
            pav.isActive,
            pa.packetAttribute
    FROM    doc.packetAttrValue AS pav
    INNER JOIN doc.packetAttr AS pa
            ON pa.packetAttrID = pav.packetAttrID
    WHERE   packetID = @packetID ;
END


GO

PRINT N'Creating [doc].[PR_Get_packetAttrValue_By_AttrID_AttrValue]'
GO

CREATE PROCEDURE doc.PR_Get_packetAttrValue_By_AttrID_AttrValue ( 
                                                              @packetAttribute NVARCHAR(250),
                                                              @packetAttrValue NVARCHAR(MAX) )
-- =============================================
-- Author:  Robert Francis
-- Create date:	07/18/2011
-- Description:	Retrieves table document's details
-- Notes:  
-- Usage:  exec doc.PR_Get_packetAttrValue_By_AttrID_AttrValue @packetAttribute, @packetAttrValue
-- Changelog: 07/18/2011, Created
-- =============================================
AS 
BEGIN
    SET NOCOUNT ON ;
    
    SELECT  pav.packetID,
            pav.packetAttrValue,
            pa.packetAttribute
    FROM    doc.packetAttrValue AS pav
    INNER JOIN doc.packetAttr AS pa
            ON pa.packetAttrID = pav.packetAttrID
    WHERE   pa.packetAttribute = @packetAttribute
            AND pav.packetAttrValue LIKE @packetAttrValue
            AND pav.isActive = 1 ;
END


GO

PRINT N'Creating [doc].[FN_documentKeywords]'
GO

CREATE  FUNCTION doc.FN_documentKeywords ( @documentID INT )
RETURNS NVARCHAR(MAX)

-- =============================================
-- Author: Jeff Kring
-- Create date:	05.17.2010
-- Description:	Pull keywords from MSSLMS table and concantenate into a single field
-- Usage:  select msslsm.doc.FN_documentKeywords(21)
-- Changelog: 05.17.2010, Created
-- =============================================
AS 
BEGIN

    DECLARE @Attributes VARCHAR(MAX),
        @SingAttr NVARCHAR(MAX)

    DECLARE ResultsCursor CURSOR LOCAL FAST_FORWARD
    FOR
    SELECT  LTRIM(RTRIM(documentAttrValue))
    FROM    MSSLMS.doc.documentAttrValue dav
    LEFT JOIN MSSLMS.doc.documentAttr da
            ON dav.documentAttrID = da.documentAttrID
    WHERE   dav.documentID = @documentID
            AND da.documentAttribute = 'KEYWORD'
        
    OPEN ResultsCursor
        
    FETCH NEXT FROM ResultsCursor INTO @SingAttr

    WHILE @@FETCH_STATUS = 0 
        BEGIN
            IF @Attributes IS NULL 
                SELECT  @Attributes = @SingAttr
            ELSE 
                SELECT  @Attributes = COALESCE(@Attributes, '') + ', '
                        + @SingAttr
		
            FETCH NEXT FROM ResultsCursor INTO @SingAttr
        END
        
    CLOSE ResultsCursor ;
    DEALLOCATE ResultsCursor ;

    RETURN @Attributes

       
END

GO

PRINT N'Adding foreign keys to [doc].[DocEntity]'
GO
ALTER TABLE [doc].[DocEntity] ADD
CONSTRAINT [FK_DocEntity_document_documentID] FOREIGN KEY ([documentID]) REFERENCES [doc].[document] ([documentID])
GO

PRINT N'Adding foreign keys to [doc].[documentVersion]'
GO
ALTER TABLE [doc].[documentVersion] ADD
CONSTRAINT [FK_document_version_document_documentID] FOREIGN KEY ([documentID]) REFERENCES [doc].[document] ([documentID]),
CONSTRAINT [FK_documentVersion_document_versionedDocID] FOREIGN KEY ([versionedDocID]) REFERENCES [doc].[document] ([documentID])
GO

PRINT N'Adding foreign keys to [doc].[documentAttrValue]'
GO
ALTER TABLE [doc].[documentAttrValue] ADD
CONSTRAINT [FK_documentAttrValue_doc] FOREIGN KEY ([documentID]) REFERENCES [doc].[document] ([documentID]),
CONSTRAINT [FK_documentAttrValue_documentAttr] FOREIGN KEY ([documentAttrID]) REFERENCES [doc].[documentAttr] ([documentAttrID]) ON DELETE CASCADE
GO

PRINT N'Adding foreign keys to [doc].[documentCheckOut]'
GO
ALTER TABLE [doc].[documentCheckOut] ADD
CONSTRAINT [FK_documentCheckOut_document] FOREIGN KEY ([documentID]) REFERENCES [doc].[document] ([documentID])
GO

PRINT N'Adding foreign keys to [doc].[packetDocument]'
GO
ALTER TABLE [doc].[packetDocument] ADD
CONSTRAINT [FK_packetDocument_document_documentID] FOREIGN KEY ([documentID]) REFERENCES [doc].[document] ([documentID]),
CONSTRAINT [FK_packetDocument_packet_packetID] FOREIGN KEY ([packetID]) REFERENCES [doc].[packet] ([packetID])
GO

PRINT N'Adding foreign keys to [doc].[packetAttrValue]'
GO
ALTER TABLE [doc].[packetAttrValue] ADD
CONSTRAINT [FK_packetAttrValue_doc] FOREIGN KEY ([packetID]) REFERENCES [doc].[packet] ([packetID]),
CONSTRAINT [FK_packetAttrValue_packetAttr] FOREIGN KEY ([packetAttrID]) REFERENCES [doc].[packetAttr] ([packetAttrID]) ON DELETE CASCADE
GO

PRINT N'Adding foreign keys to [doc].[packet]'
GO
ALTER TABLE [doc].[packet] ADD
CONSTRAINT [FK_packet_refDocumentType_packetTypeID] FOREIGN KEY ([packetTypeID]) REFERENCES [doc].[refDocumentType] ([documentTypeID])
GO

