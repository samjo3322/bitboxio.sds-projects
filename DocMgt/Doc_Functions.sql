﻿
-- =============================================
-- PG FUNCTION TO READ TABLES
-- =============================================
DROP FUNCTION doc."PR_Get_BkMrkData"();
CREATE FUNCTION doc."PR_Get_BkMrkData"()
RETURNS TABLE(	BkMrkDataID integer,
		dbName character varying(50),
		viewPrefix character varying(50),
		dataSource character varying(50),
		BookmarkName character varying(50),
		query text,
		UseBy character varying(150),
		createdBy integer,
		createDate timestamp without time zone[],
		modifiedBy integer,
		modifiedDate timestamp without time zone,
		Note text
	)
 AS
$BODY$
	SELECT 	"BkMrkData"."BkMrkDataID",
		"BkMrkData"."dbName",
		"BkMrkData"."viewPrefix",
		"BkMrkData"."dataSource",
		"BkMrkData"."BookmarkName",
		"BkMrkData"."query",
		"BkMrkData"."UseBy",
		"BkMrkData"."createdBy",
		"BkMrkData"."createDate",
		"BkMrkData"."modifiedBy",
		"BkMrkData"."modifiedDate",
		"BkMrkData"."Note"
		FROM doc."BkMrkData";
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_BkMrkData"() OWNER TO "SDSDoc";




DROP FUNCTION doc."PR_Get_refDocumentType"();
CREATE FUNCTION doc."PR_Get_refDocumentType"()
RETURNS TABLE(	documentTypeID integer,
		documentType character varying(50),
		isActive bit
	)
 AS
$BODY$
	SELECT  "refDocumentType"."documentTypeID",
		"refDocumentType"."documentType",
		"refDocumentType"."isActive"
    FROM    doc."refDocumentType"
    ORDER BY "refDocumentType"."documentType" ;
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_refDocumentType"() OWNER TO "SDSDoc";



DROP FUNCTION doc."PR_Get_MapBkMrkData"();
CREATE FUNCTION doc."PR_Get_MapBkMrkData"()
RETURNS TABLE(	MapBkMrkDataID integer,
		DBName character varying(200),
		ViewPrefix character varying(30),
		DataSource character varying(60),
		BookMarkName character varying(200),
		BkMrkQry character varying(500),
		BkMrkNote character varying(500),
		CreatedBy integer,
		Created timestamp without time zone,
		ModifiedBy integer,
		Modified timestamp without time zone,
		IsActive bit
	)
 AS
$BODY$
	SELECT  "MapBkMrkData"."MapBkMrkDataID",
		"MapBkMrkData"."DBName",
		"MapBkMrkData"."ViewPrefix",
		"MapBkMrkData"."DataSource",
		"MapBkMrkData"."BookMarkName",
		"MapBkMrkData"."BkMrkQry",
		"MapBkMrkData"."BkMrkNote",
		"MapBkMrkData"."CreatedBy",
		"MapBkMrkData"."Created",
		"MapBkMrkData"."ModifiedBy",
		"MapBkMrkData"."Modified",
		"MapBkMrkData"."IsActive"
		FROM    doc."MapBkMrkData" ;
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_MapBkMrkData"() OWNER TO "SDSDoc";




DROP FUNCTION doc."PR_Get_Template_refDocumentType"();
CREATE FUNCTION doc."PR_Get_Template_refDocumentType" ()
RETURNS TABLE(	documentTypeID integer,
		documentType character varying(50),
		isActive bit
	)
 AS
$BODY$
	SELECT 	"refDocumentType"."documentTypeID",
		"refDocumentType"."documentType",
		"refDocumentType"."isActive"
		FROM    doc."refDocumentType"
	WHERE   "refDocumentType"."documentType" = 'Template' ;
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_Template_refDocumentType"() OWNER TO "SDSDoc";




DROP FUNCTION doc."PR_Get_Bookmark_By_BookmarkID"(integer);
CREATE FUNCTION doc."PR_Get_Bookmark_By_BookmarkID" (IN "BookmarkID" integer)
RETURNS TABLE(	BkMrkDataID integer,
		dbName character varying(50),
		viewPrefix character varying(50),
		dataSource character varying(50),
		BookmarkName character varying(50),
		query text,
		UseBy character varying(150),
		createdBy integer,
		createDate timestamp without time zone[],
		modifiedBy integer,
		modifiedDate timestamp without time zone,
		Note text
	)
 AS
$BODY$
	SELECT  "BkMrkData"."BkMrkDataID" AS  BookmarkID,
		"BkMrkData"."dbName",
		"BkMrkData"."viewPrefix",
		"BkMrkData"."dataSource",
		"BkMrkData"."BookmarkName",
		"BkMrkData"."query",
		"BkMrkData"."UseBy",
		"BkMrkData"."createdBy",
		"BkMrkData"."createDate",
		"BkMrkData"."modifiedBy",
		"BkMrkData"."modifiedDate",
		"BkMrkData"."Note"
		FROM    doc."BkMrkData"
		WHERE   "BkMrkData"."BkMrkDataID" = $1 ;
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_Bookmark_By_BookmarkID"(integer) OWNER TO "SDSDoc";




DROP FUNCTION doc."PR_Get_Default_refDocumentType"();
CREATE FUNCTION doc."PR_Get_Default_refDocumentType" ()
RETURNS TABLE(	documentTypeID integer,
		documentType character varying(50),
		isActive bit
	)
 AS
$BODY$
	SELECT  "refDocumentType"."documentTypeID",
		"refDocumentType"."documentType",
		"refDocumentType"."isActive"
		FROM    doc."refDocumentType"
		WHERE   "refDocumentType"."documentType" = 'Default';
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_Default_refDocumentType"() OWNER TO "SDSDoc";




DROP FUNCTION doc."PR_Get_documentAttrValue_By_docID_AttrID"(integer,integer);
CREATE FUNCTION doc."PR_Get_documentAttrValue_By_docID_AttrID" (IN "documentID" integer, "documentAttrID" integer)
RETURNS TABLE(	documentAttrValueID integer,
		documentID integer,
		documentAttrID integer,
		documentAttrValue text
	)
 AS
$BODY$
	SELECT  "documentAttrValue"."documentAttrValueID",
		"documentAttrValue"."documentID",
		"documentAttrValue"."documentAttrID",
		"documentAttrValue"."documentAttrValue"
		FROM    doc."documentAttrValue"
		WHERE   "documentAttrValue"."documentID" = $1
		AND "documentAttrValue"."documentAttrID" = $2 ;
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_documentAttrValue_By_docID_AttrID"(integer,integer) OWNER TO "SDSDoc";





DROP FUNCTION doc."PR_Get_document"();
CREATE FUNCTION doc."PR_Get_document" ()
RETURNS TABLE(	documentID integer,
		documentFile text,
		documentPath character varying(1000),
		documentName character varying(255),
		documentDate timestamp without time zone,
		documentSize integer,
		documentTypeID integer,
		documentIsActive bit,
		createdBy integer,
		createDate timestamp without time zone,
		documentTitle character varying(100),
		documentGroup character varying(32)
	)
 AS
$BODY$
	SELECT  "document"."documentID",
		"document"."documentFile",
		"document"."documentPath",
		"document"."documentName",
		"document"."documentDate",
		"document"."documentSize",
		"document"."documentTypeID",
		"document"."documentIsActive",
		"document"."createdBy",
		"document"."createDate",
		"document"."documentTitle",
		"document"."documentGroup"
		FROM    doc."document" ;
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_document"() OWNER TO "SDSDoc";





DROP FUNCTION doc."PR_Get_document_By_DocumentID"(integer);
CREATE FUNCTION doc."PR_Get_document_By_DocumentID" (IN "DocumentID" integer)
RETURNS TABLE(	documentID integer,
		documentFile text,
		documentPath character varying(1000),
		documentName character varying(255),
		documentDate timestamp without time zone,
		documentSize integer,
		documentTypeID integer,
		documentIsActive bit,
		createdBy integer,
		createDate timestamp without time zone,
		documentTitle character varying(100),
		documentGroup character varying(32)
	)
 AS
$BODY$
	SELECT  "document"."documentID",
		"document"."documentFile",
		"document"."documentPath",
		"document"."documentName",
		"document"."documentDate",
		"document"."documentSize",
		"document"."documentTypeID",
		"document"."documentIsActive",
		"document"."createdBy",
		"document"."createDate",
		"document"."documentTitle",
		"document"."documentGroup"
		FROM    doc."document"
		WHERE   "document"."documentID" = $1;
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_document_By_DocumentID"(integer) OWNER TO "SDSDoc";





DROP FUNCTION doc."PR_Get_documentAttr_By_Name"(character varying);
CREATE FUNCTION doc."PR_Get_documentAttr_By_Name" (IN "documentAttribute" character varying(250))
RETURNS TABLE(	documentAttrID integer,
		documentAttribute character varying(250)
	)
 AS
$BODY$
	SELECT  "documentAttr"."documentAttrID",
		"documentAttr"."documentAttribute"
		FROM    doc."documentAttr"
		WHERE   "documentAttr"."documentAttribute" = $1 ;
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_documentAttr_By_Name"(character varying) OWNER TO "SDSDoc";




DROP FUNCTION doc."PR_Get_MsgBroker_By_MsgBrokerID"(uuid);
CREATE FUNCTION doc."PR_Get_MsgBroker_By_MsgBrokerID" (IN "MsgBrokerID" uuid)
RETURNS TABLE(	MsgBrokerID uuid,
		EntityList character varying(1000),
		EntityID integer,
		EntityTypeID integer,
		UserID integer,
		FeatureID integer,
		GroupID character varying(32),
		CreateDateTime timestamp without time zone,
		ActivityType integer,
		LogActivity bit(1),
		ReturnURL character varying(1000)
	)
 AS
$BODY$
	SELECT  "MsgBroker"."MsgBrokerID",
		"MsgBroker"."EntityList",
		"MsgBroker"."EntityID",
		"MsgBroker"."EntityTypeID",
		"MsgBroker"."UserID",
		"MsgBroker"."FeatureID",
		"MsgBroker"."GroupID",
		"MsgBroker"."CreateDateTime",
		"MsgBroker"."ActivityType",
		"MsgBroker"."LogActivity",
		"MsgBroker"."ReturnURL"
		FROM    doc."MsgBroker"
		WHERE   "MsgBroker"."MsgBrokerID" = $1 ;
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_MsgBroker_By_MsgBrokerID"(uuid) OWNER TO "SDSDoc";





DROP FUNCTION doc."PR_Get_DocumentID_By_ActivityType"(integer);
CREATE FUNCTION doc."PR_Get_DocumentID_By_ActivityType" (IN "ActivityType" integer)
RETURNS TABLE(	documentID integer )
 AS
$BODY$
	SELECT  "document"."documentID"
		FROM    doc."document"
		WHERE   "document"."ActivityTypeID" = $1;
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_DocumentID_By_ActivityType"(integer) OWNER TO "SDSDoc";





DROP FUNCTION doc."PR_Get_documentAttrValue_By_AttrID_AttrValue"(character varying(250), text);
CREATE FUNCTION doc."PR_Get_documentAttrValue_By_AttrID_AttrValue" (IN "documentAttribute" character varying(250), "documentAttrValue" text)
RETURNS TABLE(	documentID integer )
 AS
$BODY$
	 SELECT  "documentAttrValue"."documentID"
		FROM    doc."documentAttrValue"
		INNER JOIN doc."documentAttr"
		ON "documentAttr"."documentAttrID" = "documentAttrValue"."documentAttrID"
		WHERE   "documentAttr"."documentAttribute" = $1
		AND "documentAttrValue"."documentAttrValue" LIKE $2
		AND "documentAttrValue"."isActive" = '1' ;
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_documentAttrValue_By_AttrID_AttrValue"(character varying(250), text) OWNER TO "SDSDoc";





DROP FUNCTION doc."PR_Get_documentAttrValue_By_documentID"(integer);
CREATE FUNCTION doc."PR_Get_documentAttrValue_By_documentID" (IN "documentID" integer)
RETURNS SETOF RECORD
 AS
$BODY$
	 SELECT "documentAttrValue"."documentAttrValueID",
		"documentAttrValue"."documentID",
		"documentAttrValue"."documentAttrID",
		"documentAttrValue"."documentAttrValue",
		"documentAttr"."documentAttribute",
		"document"."documentPath"
		FROM    doc."documentAttrValue"
		INNER JOIN doc."documentAttr"
			ON "documentAttrValue"."documentAttrID" = "documentAttr"."documentAttrID"
		INNER JOIN doc."document"
			ON "document"."documentID" = "documentAttrValue"."documentID"
		WHERE "documentAttrValue"."documentID" = $1
		AND "documentAttrValue"."isActive" = '1'
		AND "document"."isActive" = '1'
		AND "documentAttr"."isActive" = '1' ;
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_documentAttrValue_By_documentID"(integer) OWNER TO "SDSDoc";



/*
PUT ON HOLD UNTIL FIGURE OUT HOW

!!! MAY WANT TO DROP INTO MIDDLEWARE !!!

DROP FUNCTION doc."PR_Get_Documents_For_Entity"(character varying, integer, character varying);
CREATE TYPE DocTable AS    ( documentID integer,
			      Title character varying(512),
			      FileName character varying(255),
			      SFileName character varying(255),
			      Type character varying(512),
			      Keywords character varying(3000),
			      Date timestamp without time zone,
			      DocEntityID integer);
CREATE FUNCTION doc."PR_Get_Documents_For_Entity" (IN "Entity_Type" character varying(25), "Entity_ID" integer, "DateSort" character varying(4) Default 'NULL')

RETURNS SETOF DocTable
 AS
$BODY$
	--DECLARE
	--DocID integer,
	--Attributes text,
	--SingAttr text;	
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_Documents_For_Entity"(character varying, integer, character varying) OWNER TO "SDSDoc";
*/





DROP FUNCTION doc."PR_Get_document"();
CREATE FUNCTION doc."PR_Get_document" ()
RETURNS TABLE(	documentID integer,
		documentFile text,
		documentPath character varying(1000),
		documentName character varying(255),
		documentDate timestamp without time zone,
		documentSize integer,
		documentTypeID integer,
		documentIsActive bit,
		createdBy integer,
		createDate timestamp without time zone,
		documentTitle character varying(100),
		documentGroup character varying(32)
	)
 AS
$BODY$
	SELECT  "document"."documentID",
		"document"."documentFile",
		"document"."documentPath",
		"document"."documentName",
		"document"."documentDate",
		"document"."documentSize",
		"document"."documentTypeID",
		"document"."documentIsActive",
		"document"."createdBy",
		"document"."createDate",
		"document"."documentTitle",
		"document"."documentGroup"
		FROM    doc."document" ;
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_document"() OWNER TO "SDSDoc";





DROP FUNCTION doc."PR_Get_Search"();
CREATE FUNCTION doc."PR_Get_Search" ()
RETURNS TABLE(	SearchID integer,
		tblName character varying(50),
		colName character varying(50),
		colAlias character varying(50)
	)
 AS
$BODY$
	SELECT  "Search"."SearchID",
		"Search"."tblName",
		"Search"."colName",
		"Search"."colAlias"
		FROM    doc."Search"
		WHERE   "Search"."isActive" = '1' ;
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_Search"() OWNER TO "SDSDoc";





DROP FUNCTION doc."PR_Get_packet_By_packetID"(integer);
CREATE FUNCTION doc."PR_Get_packet_By_packetID" (IN "packetID" integer)
RETURNS SETOF RECORD
 AS
$BODY$
	 SELECT "packet"."packetID",
		"packet"."packetTypeID",
		"packet"."packetIsActive",
		"packet"."createdBy",
		"packet"."createDate",
		"packet"."packetTitle",
		"packet"."isActive"
		FROM    doc."packet"
		WHERE   "packet"."packetID" = $1;
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_packet_By_packetID"(integer) OWNER TO "SDSDoc";





DROP FUNCTION doc."PR_Get_documents_By_packetID"(integer);
CREATE FUNCTION doc."PR_Get_documents_By_packetID" (IN "packetID" integer)
RETURNS SETOF RECORD
 AS
$BODY$
	 SELECT  "document"."documentID",
		'0' AS rowCnt,
		"document"."documentFile",
		"document"."documentPath",
		"document"."documentName",
		CAST ("document"."documentDate" AS char(110)),
		"document"."documentSize",
		"document"."documentTypeID",
		"refDocumentType"."documentType",
		"document"."documentIsActive",
		"document"."createdBy",
		"document"."createDate",
		"document"."documentTitle",
		"document"."documentGroup",
		' ' AS keywords
		FROM    doc."document"
		INNER JOIN doc."packetDocument"
			ON "packetDocument"."documentID" = "document"."documentID"
		LEFT JOIN doc."refDocumentType"
			ON "refDocumentType"."documentTypeID" = "document"."documentTypeID"
		WHERE   "packetDocument"."packetID" = $1
			AND "document"."isActive" = '1'
			AND"packetDocument"."isActive" = '1' ;
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_documents_By_packetID"(integer) OWNER TO "SDSDoc";



/*
PUT ON HOLD UNTIL FIGURE OUT HOW

!!! MAY WANT TO DROP INTO MIDDLEWARE !!!

DROP FUNCTION doc."PR_Get_document_By_DocList"(character varying);
CREATE FUNCTION doc."PR_Get_document_By_DocList" (IN "DocList" character varying(250))
RETURNS SETOF RECORD
 AS
$BODY$
	 SELECT  DISTINCT
		"document"."documentID" AS docID,
		"document"."documentFile" AS docFile,
		"document"."documentPath" AS docFilePath,
		"document"."documentName" AS docName,
		"document"."documentTitle" AS docTitle,
		"document"."documentTypeID" AS docTypeID,
		"refDocumentType"."documentType" AS docType,
		"document"."documentDate" AS docDate,
		"document"."documentSize" AS docSize,
		"document"."createdBy" AS docCreatedBy,
		"document"."createDate" AS docCreateDate,
		msslms.doc.FN_documentKeywords(d.documentID) docKeywords,
		"document"."documentGroup" AS docGroup,
		atr.documentAttrID docAttrID,
		atr.documentAttribute docAttr
		FROM    doc."fnParseList"(',', $1) fpl
		INNER JOIN doc."document"
			ON "document"."documentID" = CAST(fpl.Data AS INT)
		LEFT JOIN doc."refDocumentType"
			ON "document"."documentTypeID" = "refDocumentType".documentTypeID
		LEFT JOIN doc.documentAttrValue AS atrVal
			ON atrVal.documentID = "document"."documentid"
		LEFT JOIN doc.documentAttr AS atr
			ON atr.documentAttrID = atrVal.documentAttrID
                AND atr.documentAttribute LIKE '%Template-%'
		WHERE   "document"."IsActive" = '1'
		ORDER BY docCreateDate DESC
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_document_By_DocList"(character varying) OWNER TO "SDSDoc";
*/




/*
PUT ON HOLD UNTIL FIGURE OUT HOW

!!! MAY WANT TO DROP INTO MIDDLEWARE !!!

PR_Get_Documents_By_EntityList

*/







DROP FUNCTION doc."PR_Get_packetAttrValue_By_packetID"(integer);
CREATE FUNCTION doc."PR_Get_packetAttrValue_By_packetID" (IN "packetID" integer)
RETURNS SETOF RECORD
 AS
$BODY$
	 SELECT "packetAttrValue"."packetAttrValueID",
		"packetAttrValue"."packetID",
		"packetAttrValue"."packetAttrID",
		"packetAttrValue"."packetAttrValue",
		"packetAttrValue"."isActive",
		"packetAttr"."packetAttribute"
		FROM    doc."packetAttrValue" 
		INNER JOIN doc."packetAttr" 
			ON "packetAttr"."packetAttrID" = "packetAttrValue"."packetAttrID"
		WHERE   "packetAttrValue"."packetID" = $1 ;
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_packetAttrValue_By_packetID"(integer) OWNER TO "SDSDoc";




DROP FUNCTION doc."PR_Get_packetAttrValue_By_AttrID_AttrValue"(character varying, text);
CREATE FUNCTION doc."PR_Get_packetAttrValue_By_AttrID_AttrValue" (IN "packetAttribute" character varying(250), "packetAttrValue" text)
RETURNS SETOF RECORD
 AS
$BODY$
	 SELECT "packetAttrValue"."packetID",
		"packetAttrValue"."packetAttrValue",
		"packetAttr"."packetAttribute"
		FROM    doc."packetAttrValue" 
		INNER JOIN doc."packetAttr" 
		ON "packetAttr"."packetAttrID" = "packetAttrValue"."packetAttrID"
		WHERE   "packetAttr"."packetAttribute" = $1
		AND "packetAttrValue"."packetAttrValue" LIKE $2
		AND "packetAttrValue"."isActive" = '1' ;
$BODY$
LANGUAGE sql STABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION doc."PR_Get_packetAttrValue_By_AttrID_AttrValue"(character varying, text) OWNER TO "SDSDoc";





DROP FUNCTION doc."PR_Delete_MapBkMrkData"(integer);
CREATE FUNCTION doc."PR_Delete_MapBkMrkData" (IN "MapBkMrkDataID" integer)
RETURNS integer
 AS
$BODY$
	UPDATE  doc."MapBkMrkData"
		SET     "IsActive" = '0' 
		WHERE   "MapBkMrkDataID" = $1;
	SELECT 1;
$BODY$
LANGUAGE sql;
ALTER FUNCTION doc."PR_Delete_MapBkMrkData"(integer) OWNER TO "SDSDoc";





DROP FUNCTION doc."PR_Delete_Document"(integer);
CREATE FUNCTION doc."PR_Delete_Document" (IN "documentID" integer)
RETURNS bigint
 AS
$BODY$
	UPDATE  doc."document"
		SET "isActive" = '0',
		"documentIsActive" = '0'
		WHERE   "documentID" = $1;
	SELECT COUNT("document"."isActive")
		FROM doc."document" 
		WHERE   "documentID" = $1;
$BODY$
LANGUAGE sql;
ALTER FUNCTION doc."PR_Delete_Document"(integer) OWNER TO "SDSDoc";





DROP FUNCTION doc."PR_Delete_documentAttrValue_By_DocumentID"(integer);
CREATE FUNCTION doc."PR_Delete_documentAttrValue_By_DocumentID" (IN "documentID" integer)
RETURNS VOID
 AS
$BODY$
	DELETE  FROM doc."documentAttrValue"
		WHERE "documentAttrValue"."documentID" = $1;
$BODY$
LANGUAGE sql;
ALTER FUNCTION doc."PR_Delete_documentAttrValue_By_DocumentID"(integer) OWNER TO "SDSDoc";





DROP FUNCTION doc."PR_Delete_documentAttrValue_By_docID_AttrID"(integer, integer);
CREATE FUNCTION doc."PR_Delete_documentAttrValue_By_docID_AttrID" (IN "documentID" integer, "documentAttrID" integer)
RETURNS VOID
 AS
$BODY$
	DELETE  FROM doc."documentAttrValue"
		WHERE "documentAttrValue"."documentID" = $1
		AND "documentAttrValue"."documentAttrID" = $2;
$BODY$
LANGUAGE sql;
ALTER FUNCTION doc."PR_Delete_documentAttrValue_By_docID_AttrID"(integer, integer) OWNER TO "SDSDoc";





DROP FUNCTION doc."PR_Delete_packetAttrValue_By_packetID"(integer);
CREATE FUNCTION doc."PR_Delete_packetAttrValue_By_packetID" (IN "packetID" integer)
RETURNS VOID
 AS
$BODY$
	DELETE  FROM doc."packetAttrValue"
		WHERE   "packetAttrValue"."packetID" = $1;
$BODY$
LANGUAGE sql;
ALTER FUNCTION doc."PR_Delete_packetAttrValue_By_packetID"(integer) OWNER TO "SDSDoc";





DROP FUNCTION doc."PR_Delete_packetAttrValue_By_docID_AttrID"(integer, integer);
CREATE FUNCTION doc."PR_Delete_packetAttrValue_By_docID_AttrID" (IN "packetID" integer, "packetAttrID" integer)
RETURNS VOID
 AS
$BODY$
	DELETE  FROM doc."packetAttrValue"
		WHERE "packetAttrValue"."packetID" = $1
		AND "packetAttrValue"."packetAttrID" = $2;
$BODY$
LANGUAGE sql;
ALTER FUNCTION doc."PR_Delete_packetAttrValue_By_docID_AttrID"(integer, integer) OWNER TO "SDSDoc";




DROP FUNCTION doc."PR_Set_packetDocument"(integer, integer, bit);
CREATE FUNCTION doc."PR_Set_packetDocument" (IN "packetID" integer, "documentID" integer, "isActive" bit)
RETURNS integer
 AS
$BODY$
	INSERT INTO doc."packetDocument" 
			( "packetID", "documentID", "isActive" )
		VALUES  ( $1, $2, $3 );
		
	SELECT "packetDocument"."packetID"
		FROM doc."packetDocument" 
		WHERE "packetID" = $1 
		AND "documentID" = $2 
		AND "isActive" = $3
$BODY$
LANGUAGE sql;
ALTER FUNCTION doc."PR_Set_packetDocument"(integer, integer, bit) OWNER TO "SDSDoc";







---------------------------------------- FK UPDATES ON CASCADE ----------------------------------------

ALTER TABLE doc."DocEntity" DROP CONSTRAINT "documentID_FK2";
ALTER TABLE doc."DocEntity"
  ADD CONSTRAINT "documentID_FK2" FOREIGN KEY ("documentID")
      REFERENCES doc."document" ("documentID") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;


ALTER TABLE doc."document" DROP CONSTRAINT "documentTypeID_FK";
ALTER TABLE doc."document"
  ADD CONSTRAINT "documentTypeID_FK" FOREIGN KEY ("documentTypeID")
      REFERENCES doc."refDocumentType" ("documentTypeID") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;


ALTER TABLE doc."documentAttrValue" DROP CONSTRAINT "documentAttrID_FK";
ALTER TABLE doc."documentAttrValue"
  ADD CONSTRAINT "documentAttrID_FK" FOREIGN KEY ("documentAttrID")
      REFERENCES doc."documentAttr" ("documentAttrID") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE;


ALTER TABLE doc."documentAttrValue" DROP CONSTRAINT "documentID_FK3";
ALTER TABLE doc."documentAttrValue"
  ADD CONSTRAINT "documentID_FK3" FOREIGN KEY ("documentID")
      REFERENCES doc."document" ("documentID") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;


ALTER TABLE doc."documentCheckOut" DROP CONSTRAINT "documentID_FK4";
ALTER TABLE doc."documentCheckOut"
  ADD CONSTRAINT "documentID_FK4" FOREIGN KEY ("documentID")
      REFERENCES doc."document" ("documentID") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;


ALTER TABLE doc."documentVersion" DROP CONSTRAINT "documentID_FK1";
ALTER TABLE doc."documentVersion"
  ADD CONSTRAINT "documentID_FK1" FOREIGN KEY ("documentID")
      REFERENCES doc."document" ("documentID") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;


ALTER TABLE doc."packetAttrValue" DROP CONSTRAINT "packetAttrID_FK";
ALTER TABLE doc."packetAttrValue"
  ADD CONSTRAINT "packetAttrID_FK" FOREIGN KEY ("packetAttrID")
      REFERENCES doc."packetAttr" ("packetAttrID") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE;


ALTER TABLE doc."packetAttrValue" DROP CONSTRAINT "packetID_PK1";
ALTER TABLE doc."packetAttrValue"
  ADD CONSTRAINT "packetID_PK1" FOREIGN KEY ("packetID")
      REFERENCES doc.packet ("packetID") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;


ALTER TABLE doc."packetDocument" DROP CONSTRAINT "documentID_FK";
ALTER TABLE doc."packetDocument"
  ADD CONSTRAINT "documentID_FK" FOREIGN KEY ("documentID")
      REFERENCES doc."document" ("documentID") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE doc."packetDocument" DROP CONSTRAINT "packetID_FK";
ALTER TABLE doc."packetDocument"
  ADD CONSTRAINT "packetID_FK" FOREIGN KEY ("packetID")
      REFERENCES doc.packet ("packetID") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
      
-------------------------------------------------------------------------------------------------------